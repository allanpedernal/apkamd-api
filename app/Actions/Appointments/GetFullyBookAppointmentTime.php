<?php

namespace App\Actions\Appointments;

use Carbon\Carbon;
use PDO;
use DB;

use Illuminate\Support\Arr;

class GetFullyBookAppointmentTime
{
    /**
     * Database Connection
     * @var [type]
     */
    protected $connection;

    /**
     * [__construct description]
     */
    public function __construct()
    {
        $this->connection = app('sql_srv_connection')->getPdo();
    }

    /**
     * Execute Action
     * @return [type] [description]
     */
    public function execute($patient_id, $date, $state, $provider_id)
    {
        $availableProviders = [];
        $unavailableProviders = [];
        $timeslots = $this->getTimeSlots($date);
        $list_providers = $this->getProvidersByState($state);

        $providers = [];

        foreach($list_providers as $check_provider)
        {
            if($check_provider['ProviderID'] == $provider_id) {
                $providers[] = $check_provider;
                break;
            }
        }

        $noAvailableProviders = [];

        foreach ($timeslots as $key => $time) {

            $datetime = now()->change($date . ' ' . $time)->format('m/d/Y H:i:s');
            $time = now()->change($date . ' ' . $time)->format('h:i A');
            $appointments = $this->getProviderAppointments($datetime);
            $unavailableProviders = Arr::pluck($appointments, 'provider_id');
            $schedules = $this->getProviderWithSchedule($providers, $date);
            $patient_appointments = $this->getPatientAppointments($patient_id, $date);

            foreach ($schedules as $provider) {
                if (!in_array($provider['ProviderID'], $unavailableProviders)) {
                    if (in_array($time, $provider['schedules'])) {
                        $availableProviders[] = $provider;
                    }
                }
            }

            if(empty($availableProviders) && !in_array($time, $patient_appointments)) {
                $noAvailableProviders[] = [
                    'start' => Carbon::parse($datetime)->format('Y-m-d H:i'),
                    'end' => Carbon::parse($datetime)->addMinutes(30)->format('Y-m-d H:i'),
                    'title' => 'No available provider',
                    'title_2' => 'No available provider',
                    'visit_type' => 'no_provider',
                ];
            }
        }

        return $noAvailableProviders;
    }

    protected function getTimeSlots($date) {

        $dayOfWeek = Carbon::Parse($date)->dayOfWeek;
        $timeslots = [];
        if($dayOfWeek > 0) {
             /* @TODO: fetch the official schedule of the facility from the DB */
            switch ($dayOfWeek) {
                case 1: // monday
                    $current = now()->change('06:00:00');
                    $end = now()->change('14:00:00');
                    break;
                case 2: // tuesday
                    $current = now()->change('06:00:00');
                    $end = now()->change('18:00:00');
                    break;
                case 3: // wednesday
                    $current = now()->change('06:00:00');
                    $end = now()->change('15:00:00');
                    break;
                case 4: // thursday
                    $current = now()->change('06:00:00');
                    $end = now()->change('18:00:00');
                    break;
                case 5: // friday
                    $current = now()->change('06:00:00');
                    $end = now()->change('16:00:00');
                    break;
                case 6: // saturday
                    $current = now()->change('06:00:00');
                    $end = now()->change('12:00:00');
                break;
            }

            $current = Carbon::parse($current)->format('H:i:s');
            $end = Carbon::parse($end)->addMinutes(30)->format('H:i:s');


            while ($current < $end) {
                $timeslots[] = $current;
                $current = Carbon::parse($current)->addMinutes(30)->format('H:i:s');
            }

            return $timeslots;
        }


    return $timeslots;
}

    /**
     * Get providers by state
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    protected function getProvidersByState($state)
    {
        $practice_id = env('PRACTICE_ID');

        $statement = $this->connection->prepare(
            "EXEC sp_Schedule_ProviderList
                @PracticeID = :practiceID,
                @State = :state"
        );

        $statement->bindParam(':practiceID', $practice_id);
        $statement->bindParam(':state', $state);
        $statement->execute();
        $providers = $statement->fetchAll(PDO::FETCH_ASSOC);

        return $providers;
    }

    /**
     * Get Provider and their schedules
     * @return [type] [description]
     */
    public function getProviderWithSchedule($providers, $date)
    {
        $result = [];

        foreach ($providers as $provider) {
            $schedule = $provider;
            $schedule['schedules'] = $this->getProviderSchedule($provider['ProviderID'], $date);
            $result[] = $schedule;
        }

        return $result;
    }

    /**
     * Get all appointments and the assigned providers
     * @return [type] [description]
     */
    public function getProviderAppointments($datetime)
    {
        $result = [];

        $statement = $this->connection->prepare(
            "EXEC dbo.sp_appointment_by_datetime
                @appointment_datetime  = :appointment_datetime"
        );

        $statement->bindParam(':appointment_datetime', $datetime);
        $statement->execute();
        $result = $statement->fetchAll(PDO::FETCH_ASSOC);

        return $result;
    }

    /**
     * Get all patient appointments
     * @param  [type] $patient_id [description]
     * @return [type]             [description]
     */
    public function getPatientAppointments($patient_id, $date)
    {
        $result = [];
        $practice_id = env('PRACTICE_ID');
        $provider_id = null;

        $statement = $this->connection->prepare("
            EXEC sp_ScheduleList
                @PracticeId = :practice_id
            ,   @PatientID = :PatientID
            ,   @ProviderID = :ProviderID
        ");

        $statement->bindParam(':practice_id',$practice_id);
        $statement->bindParam(':PatientID', $patient_id);
        $statement->bindParam(':ProviderID', $provider_id);
        $statement->execute();
        $appointments = $statement->fetchAll(PDO::FETCH_ASSOC);

        if (is_array($appointments) && count($appointments) > 0) {
            $result = Arr::where($appointments, function($item) use ($date) {
                return now()->change($item['start'])->toDateString() == now()->change($date)->toDateString();
            });

            $result = array_map(function($item) {
                return now()->change($item['start'])->format('h:i A');
            }, $result);
        }

        return $result;
    }

    /**
     * Get provider's schedule
     * @param  [type] $provider_id [description]
     * @return [type]              [description]
     */
    protected function getProviderSchedule($provider_id, $date)
    {
        $result = [];
        $practice_id = env('PRACTICE_ID');

        $statement = $this->connection->prepare(
            "EXEC sp_provider_schedule
                @practice_id = :practice_id,
                @provider_id = :provider_id"
        );

        $statement->bindParam(':practice_id', $practice_id);
        $statement->bindParam(':provider_id', $provider_id);
        $statement->execute();
        $schedules = $statement->fetchAll(PDO::FETCH_ASSOC);

        $dayOfWeek = strtolower(now()->change($date)->format('l'));

        $schedules = Arr::where($schedules, function($value, $key) use ($dayOfWeek) {
            return $value['days'] == $dayOfWeek;
        });

        foreach ($schedules as $schedule) {
            $scheduleArr = [];

            if($schedule['time_open'] && $schedule['time_close']) {
                $current = $schedule['time_open'] ? now()->change($schedule['time_open']) :  now()->change('06:00');
                $end = $schedule['time_close'] ? now()->change($schedule['time_close']) :  now()->change('18:00');
                $scheduleArr = $this->getScheduleList($date, $current, $end);

                $result = array_merge($result, $scheduleArr);
            }

        }

        return $result;
    }

    /**
     * Generate schedule based on date, start and end time
     * @param  [type] $date    [description]
     * @param  [type] $current [description]
     * @param  [type] $end     [description]
     * @return [type]          [description]
     */
    public function getScheduleList($date, $current, $end)
    {
        $result = [];

        if ($current && $end) {
            $count = 0;
            while ($end->gt($current)) {
                if ($count > 0) {
                    $current = now()
                        ->change($current)
                        ->addMinutes(30);
                }
                if (now()->change($date)->isToday()) {
                    if (now()->change($date)->lte($current)) {
                        $result[] = $current->format('h:i A');
                    }
                } else {
                    $result[] = $current->format('h:i A');
                }

                $count++;
            }
        }

        return $result;
    }
}
