<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;

class TestCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'test:test';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $carrier_data = [
            'ppmdmsg' => [
                '@action' => 'lookupcarrier',
                '@class' => 'lookup',
                '@msgtime' => Carbon::now()->format('m/d/Y h:i:s A'),
                '@exactmatch' => '0',
                '@code' => 'AAP',
                '@orderby' => 'code',
                '@page' => '1',
                'subtype' => [
                    "@filter-activeonly" => "true"
                ]
            ]
        ];
        dd(\AMD::init($carrier_data)->process());
        return 0;
    }
}
