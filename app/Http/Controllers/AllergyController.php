<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

/**
 * @group  Allergy
 *
 * APIs for Allergies
 */
class AllergyController extends Controller
{
    /**
     * @OA\Get(
     *     path="/api/v1/allergy/patient/{patient_id}",
     *     tags={"Allergy"},
     *     description="API to get allergies by patient id",
     *     summary="Get Allergies By Patient ID",
     *     security={
     *       {"passport": {}},
     *     },
     *     @OA\Parameter(
     *         name="patient_id",
     *         in="path",
     *         required=true,
     *         description="Patient ID of the Patient to retrieve Allergies.",
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean",
     *                 example="false"
     *             ),
     *             @OA\Property(
     *                 property="allergies",
     *                 type="array",
     *                 @OA\Items(
     *                      type="object",
     *                      @OA\Property(property="id", type="string", example="B7D26C7F-9CB1-EB11-AAE9-02E7A59D591E"),
     *                      @OA\Property(property="patient_id", type="string", example="468C3D28-7EB1-EB11-AAE9-02E7A59D591E"),
     *                      @OA\Property(property="allergy_type", type="string", example="Drug"),
     *                      @OA\Property(property="allergy", type="string", example="Shrimp"),
     *                      @OA\Property(property="reaction", type="string", example="Swelling"),
     *                      @OA\Property(property="created_at", type="string", example="2021-05-25 07:33:04.290"),
     *                      @OA\Property(property="updated_at", type="string", example="2021-05-25 07:33:04.290"),
     *                 ),
     *             ),
     *         )
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": true, "message": "Error Message"}
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": true, "message": "Error Message"}
     *         )
     *     )
     * )
     */
    public function getAllergiesByPatientID(Request $request, $patient_id)
    {
        try
        {
            // set connection
            $connection = \DB::connection('sqlsrv')->getPdo();

            // execute appointment by date mobile
            $statement = $connection->prepare("
                EXEC dbo.sp_allergy_list
                    @patient_id=:patient_id
            ");
            $statement->bindParam(':patient_id',$patient_id);
            $statement->execute();
            $allergies = $statement->fetchAll(\PDO::FETCH_ASSOC);

            // return response
            return \Response::json([
                'error'=>false,
                'allergies'=>$allergies
            ],200);
        }
        catch(\Exception $e)
        {
            return \Response::json([
                'error'=>true,
                'message'=>$e->getMessage()
            ],500);
        }
    }

    /**
     * @OA\Post(
     *     path="/api/v1/allergy",
     *     tags={"Allergy"},
     *     description="Description",
     *     summary="Store Allergy",
     *     security={
     *       {"passport": {}},
     *     },
     *     @OA\RequestBody(
     *       required=true,
     *       description="Description",
     *       @OA\JsonContent(
     *           required={"patient_id","allergy_type","allergy","reaction"},
     *           @OA\Property(property="patient_id", type="uuid", example="944550AC-7EB1-EB11-AAE9-02E7A59D591E"),
     *           @OA\Property(property="allergy_type", type="string", example="Drug"),
     *           @OA\Property(property="allergy", type="string", example="Shrimp"),
     *           @OA\Property(property="reaction", type="string", example="Swelling"),
     *       ),
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": false, "message": "Allergy is successfully added!"}
     *         )
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": true, "message": "Error Message"}
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": true, "message": "Error Message"}
     *         )
     *     )
     * )
     */
    public function store(Request $request)
    {
        try
        {
            // get error
            $error = static::validateRequest(
                \Validator::make($request->all(), [
                    'patient_id' => 'required',
                    'allergy_type' => 'required',
                    'allergy' => 'required',
                    'reaction' => 'required',
                ])
            );

            // count error
            if (count($error) > 0)
            {
                return \Response::json([
                    'error' => true,
                    'message' => $error
                ], 400);
            }

            // start transaction
            \DB::connection('sqlsrv')->beginTransaction();

            # set connection
            $connection = \DB::connection('sqlsrv')->getPdo();

            // extract all
            extract($request->all());

            // execute insurance mobile
            $statement = $connection->prepare("
                EXEC dbo.spm_allergy
                    @flag=0,
                    @patient_id=:patient_id,
                    @allergy_type=:allergy_type,
                    @allergy=:allergy,
                    @reaction=:reaction,
                    @id=NULL
            ");
            $statement->bindParam(':patient_id',$patient_id);
            $statement->bindParam(':allergy_type',$allergy_type);
            $statement->bindParam(':allergy',$allergy);
            $statement->bindParam(':reaction',$reaction);
            $statement->execute();

            // commit
            \DB::connection('sqlsrv')->commit();

            // return response
            return \Response::json([
                'error'=>false,
                'message'=>'Allergy is successfully added!'
            ],200);
        }
        catch(\Exception $e)
        {
            return \Response::json([
                'error'=>true,
                'message'=>$e->getMessage()
            ],500);
        }
    }

    /**
     * @OA\Put(
     *     path="/api/v1/allergy/{id}",
     *     tags={"Allergy"},
     *     description="Description",
     *     summary="Update Allergy",
     *     security={
     *       {"passport": {}},
     *     },
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         required=true,
     *         description="ID of the Allergy.",
     *     ),
     *     @OA\RequestBody(
     *       required=true,
     *       description="Description",
     *       @OA\JsonContent(
     *           required={"patient_id","allergy_type","allergy","reaction"},
     *           @OA\Property(property="patient_id", type="uuid", example="944550AC-7EB1-EB11-AAE9-02E7A59D591E"),
     *           @OA\Property(property="allergy_type", type="string", example="Drug"),
     *           @OA\Property(property="allergy", type="string", example="Shrimp"),
     *           @OA\Property(property="reaction", type="string", example="Swelling"),
     *       ),
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": false, "message": "Allergy successfully updated!"}
     *         )
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": true, "message": "Error Message"}
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": true, "message": "Error Message"}
     *         )
     *     )
     * )
     */
    public function update(Request $request, $id)
    {
        try
        {
            # set connection
            $connection = \DB::connection('sqlsrv')->getPdo();

            // extract all
            extract($request->all());

            $patient_id = (isset($patient_id)&&!empty($patient_id) ? $patient_id : '');
            $allergy_type = (isset($allergy_type)&&!empty($allergy_type) ? $allergy_type : '');
            $allergy = (isset($allergy)&&!empty($allergy) ? $allergy : '');
            $reaction = (isset($reaction)&&!empty($reaction) ? $reaction : '');

            // start transaction
            \DB::connection('sqlsrv')->beginTransaction();

            // execute insurance mobile
            $statement = $connection->prepare("
                EXEC dbo.spm_allergy
                    @flag=1,
                    @patient_id=:patient_id,
                    @allergy_type=:allergy_type,
                    @allergy=:allergy,
                    @reaction=:reaction,
                    @id=:id
            ");
            $statement->bindParam(':patient_id',$patient_id);
            $statement->bindParam(':allergy_type',$allergy_type);
            $statement->bindParam(':allergy',$allergy);
            $statement->bindParam(':reaction',$reaction);
            $statement->bindParam(':id',$id);
            $statement->execute();

            // commit
            \DB::connection('sqlsrv')->commit();

            // return response
            return \Response::json([
                'error'=>false,
                'message'=>'Allergy successfully updated!'
            ],200);
        }
        catch(\Exception $e)
        {
            return \Response::json([
                'error'=>true,
                'message'=>$e->getMessage()
            ],500);
        }
    }

    /**
     * @OA\Delete(
     *     path="/api/v1/allergy/{id}",
     *     tags={"Allergy"},
     *     description="Description",
     *     summary="Delete Allergy",
     *     security={
     *       {"passport": {}},
     *     },
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         required=true,
     *         description="ID of the allergy.",
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": false, "message": "Allergy successfully deleted!"}
     *         )
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": true, "message": "Error Message"}
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": true, "message": "Error Message"}
     *         )
     *     )
     * )
     */
    public function destroy($id)
    {
        try
        {
            # set connection
            $connection = \DB::connection('sqlsrv')->getPdo();

            // start transaction
            \DB::connection('sqlsrv')->beginTransaction();

            // execute insurance mobile
            $statement = $connection->prepare("
                EXEC dbo.spm_allergy
                    @flag=2,
                    @patient_id=null,
                    @allergy_type='',
                    @allergy='',
                    @reaction='',
                    @id=:id
            ");
            $statement->bindParam(':id',$id);
            $statement->execute();

            // commit
            \DB::connection('sqlsrv')->commit();

            // return response
            return \Response::json([
                'error'=>false,
                'message'=>'Allergy successfully deleted!'
            ],200);
        }
        catch(\Exception $e)
        {
            return \Response::json([
                'error'=>true,
                'message'=>$e->getMessage()
            ],500);
        }
    }
}
