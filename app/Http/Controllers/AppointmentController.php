<?php

namespace App\Http\Controllers;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Actions\Appointments\GetFullyBookAppointmentTime;
use PDO;
use Carbon\Carbon;
use Arr;

class AppointmentController extends Controller
{
    protected $connection;

    public function __construct()
    {
        $this->connection = app('sql_srv_connection')->getPdo();
    }
    /**
     * @OA\Get(
     *     path="/api/v1/appointments",
     *     tags={"Appointment Management"},
     *     description="API to get all appointments",
     *     summary="Get Appointments",
     *     security={
     *       {"passport": {}},
     *     },
     *     @OA\Parameter(
     *         name="offset",
     *         in="query",
     *         required=true,
     *         description="Offset of the record (ex. 0).",
     *     ),
     *     @OA\Parameter(
     *         name="limit",
     *         in="query",
     *         required=true,
     *         description="Limit of the record (ex. 10).",
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean",
     *                 example="false"
     *             ),
     *             @OA\Property(
     *                 property="appointments",
     *                 type="array",
     *                 @OA\Items(
     *                      type="object",
     *                      @OA\Property(property="ID", type="uuid", example="9F4B897E-D7A9-EB11-AAE9-02E7A59D591E"),
     *                      @OA\Property(property="PatientID", type="uuid", example="0F7A3351-9708-4A6E-A862-F8ED22C2B9C7"),
     *                      @OA\Property(property="PracticeID", type="integer", example="1"),
     *                      @OA\Property(property="AppointmentStartDate", type="datetime", example="2020-05-06 08:00:00.000"),
     *                      @OA\Property(property="AppointmentEndDate", type="datetime", example="2020-05-06 09:00:00.000"),
     *                      @OA\Property(property="AppointmentStatus", type="string", example="Confirmed"),
     *                      @OA\Property(property="provider_id", type="uuid", example="A8655881-5839-EB11-AAAA-02892B467D0E"),
     *                      @OA\Property(property="provider_name", type="string", example="Doe, John"),
     *                      @OA\Property(property="SMS_Flag", type="boolean", example="1"),
     *                      @OA\Property(property="Email_Flag", type="boolean", example="1"),
     *                      @OA\Property(property="rating", type="string", example="null"),
     *                      @OA\Property(property="review", type="string", example="null"),
     *                      @OA\Property(property="Notes", type="string", example="sample notes..."),
     *                      @OA\Property(property="duration", type="string", example="null"),
     *                      @OA\Property(property="location_id", type="string", example="null"),
     *                      @OA\Property(property="location_name", type="string", example="null"),
     *                      @OA\Property(property="reason_for_visit", type="string", example="ok"),
     *                      @OA\Property(property="cnt", type="integer", example="1104"),
     *                 ),
     *             ),
     *         )
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": true, "message": "Error Message"}
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": true, "message": "Error Message"}
     *         )
     *     )
     * )
     */
    public function getAppointments(Request $request)
    {
        try
        {
            // get error
            $error = static::validateRequest(
                \Validator::make($request->all(), [
                    'offset' => 'required',
                    'limit' => 'required',
                ])
            );

            // count error
            if (count($error) > 0)
            {
                // return response
                return \Response::json([
                    'error' => true,
                    'message' => $error
                ], 400);
            }

            // set connection
            $connection = \DB::connection('sqlsrv')->getPdo();

            // extract
            extract($request->all());

            // execute patient list mobile
            $statement = $connection->prepare("
                EXEC dbo.sp_appointment_list_mobile
                    @offset=:offset,
                    @limit=:limit
            ");
            $statement->bindParam(':offset',$offset);
            $statement->bindParam(':limit',$limit);
            $statement->execute();
            $appointments = $statement->fetchAll(PDO::FETCH_ASSOC);

            // return response
            return \Response::json([
                'error'=>false,
                'appointments'=>$appointments
            ],200);
        }
        catch(\Exception $e)
        {
            return \Response::json([
                'error'=>true,
                'message'=>$e->getMessage()
            ],500);
        }
    }

    /**
     * @OA\Get(
     *     path="/api/v1/appointment/date/{datetime}",
     *     tags={"Appointment Management"},
     *     description="API to get appointments by datetime",
     *     summary="Get Appointments By Datetime",
     *     security={
     *       {"passport": {}},
     *     },
     *     @OA\Parameter(
     *         name="datetime",
     *         in="path",
     *         required=true,
     *         description="Datetime of the appointment.",
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean",
     *                 example="false"
     *             ),
     *             @OA\Property(
     *                 property="appointments",
     *                 type="array",
     *                 @OA\Items(
     *                      type="object",
     *                      @OA\Property(property="ID", type="uuid", example="9F4B897E-D7A9-EB11-AAE9-02E7A59D591E"),
     *                      @OA\Property(property="PatientID", type="uuid", example="0F7A3351-9708-4A6E-A862-F8ED22C2B9C7"),
     *                      @OA\Property(property="PracticeID", type="integer", example="1"),
     *                      @OA\Property(property="AppointmentStartDate", type="datetime", example="2020-05-06 08:00:00.000"),
     *                      @OA\Property(property="AppointmentEndDate", type="datetime", example="2020-05-06 09:00:00.000"),
     *                      @OA\Property(property="AppointmentStatus", type="string", example="Confirmed"),
     *                      @OA\Property(property="provider_id", type="uuid", example="A8655881-5839-EB11-AAAA-02892B467D0E"),
     *                      @OA\Property(property="provider_name", type="string", example="Doe, John"),
     *                      @OA\Property(property="SMS_Flag", type="boolean", example="1"),
     *                      @OA\Property(property="Email_Flag", type="boolean", example="1"),
     *                      @OA\Property(property="rating", type="string", example="null"),
     *                      @OA\Property(property="review", type="string", example="null"),
     *                      @OA\Property(property="Notes", type="string", example="sample notes..."),
     *                      @OA\Property(property="duration", type="string", example="null"),
     *                      @OA\Property(property="location_id", type="string", example="null"),
     *                      @OA\Property(property="location_name", type="string", example="null"),
     *                      @OA\Property(property="reason_for_visit", type="string", example="ok")
     *                 ),
     *             ),
     *         )
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": true, "message": "Error Message"}
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": true, "message": "Error Message"}
     *         )
     *     )
     * )
     */
    public function getAppointmentsByDate(Request $request, $datetime)
    {
        try
        {
            // set connection
            $connection = \DB::connection('sqlsrv')->getPdo();

            // extract
            extract($request->all());

            // execute appointment by datetime mobile
            $statement = $connection->prepare("
                EXEC dbo.sp_appointment_by_datetime_mobile
                    @appointment_datetime=:appointment_datetime
            ");
            $statement->bindParam(':appointment_datetime',$datetime);
            $statement->execute();
            $appointments = $statement->fetchAll(PDO::FETCH_ASSOC);

            // return response
            return \Response::json([
                'error'=>false,
                'appointments'=>$appointments
            ],200);
        }
        catch(\Exception $e)
        {
            return \Response::json([
                'error'=>true,
                'message'=>$e->getMessage()
            ],500);
        }
    }

    /**
     * @OA\Get(
     *     path="/api/v1/appointment/patient/{patient_id}",
     *     tags={"Appointment Management"},
     *     description="API to get appointments by patient id",
     *     summary="Get Appointments By Patient ID",
     *     security={
     *       {"passport": {}},
     *     },
     *     @OA\Parameter(
     *         name="patient_id",
     *         in="path",
     *         required=true,
     *         description="Patient ID of the appointment.",
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean",
     *                 example="false"
     *             ),
     *             @OA\Property(
     *                 property="appointments",
     *                 type="array",
     *                 @OA\Items(
     *                      type="object",
     *                      @OA\Property(property="ID", type="uuid", example="9F4B897E-D7A9-EB11-AAE9-02E7A59D591E"),
     *                      @OA\Property(property="PatientID", type="uuid", example="0F7A3351-9708-4A6E-A862-F8ED22C2B9C7"),
     *                      @OA\Property(property="PracticeID", type="integer", example="1"),
     *                      @OA\Property(property="AppointmentStartDate", type="datetime", example="2020-05-06 08:00:00.000"),
     *                      @OA\Property(property="AppointmentEndDate", type="datetime", example="2020-05-06 09:00:00.000"),
     *                      @OA\Property(property="AppointmentStatus", type="string", example="Confirmed"),
     *                      @OA\Property(property="provider_id", type="uuid", example="A8655881-5839-EB11-AAAA-02892B467D0E"),
     *                      @OA\Property(property="provider_name", type="string", example="Doe, John"),
     *                      @OA\Property(property="SMS_Flag", type="boolean", example="1"),
     *                      @OA\Property(property="Email_Flag", type="boolean", example="1"),
     *                      @OA\Property(property="rating", type="string", example="null"),
     *                      @OA\Property(property="review", type="string", example="null"),
     *                      @OA\Property(property="Notes", type="string", example="sample notes..."),
     *                      @OA\Property(property="duration", type="string", example="null"),
     *                      @OA\Property(property="location_id", type="string", example="null"),
     *                      @OA\Property(property="location_name", type="string", example="null"),
     *                      @OA\Property(property="reason_for_visit", type="string", example="ok")
     *                 ),
     *             ),
     *         )
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": true, "message": "Error Message"}
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": true, "message": "Error Message"}
     *         )
     *     )
     * )
     */
    public function getAppointmentsByPatientID(Request $request, $patient_id)
    {
        try
        {
            // set connection
            $connection = \DB::connection('sqlsrv')->getPdo();

            // execute appointment by date mobile
            $statement = $connection->prepare("
                EXEC dbo.sp_appointment_by_patient_mobile
                    @patient_id=:patient_id
            ");
            $statement->bindParam(':patient_id',$patient_id);
            $statement->execute();
            $appointments = $statement->fetchAll(PDO::FETCH_ASSOC);

            // return response
            return \Response::json([
                'error'=>false,
                'appointments'=>$appointments
            ],200);
        }
        catch(\Exception $e)
        {
            return \Response::json([
                'error'=>true,
                'message'=>$e->getMessage()
            ],500);
        }
    }

    /**
     * @OA\Get(
     *     path="/api/v1/appointment/provider/{provider_id}",
     *     tags={"Appointment Management"},
     *     description="API to get appointments by provider id",
     *     summary="Get Appointments By Provider ID",
     *     security={
     *       {"passport": {}},
     *     },
     *     @OA\Parameter(
     *         name="provider_id",
     *         in="path",
     *         required=true,
     *         description="Provider ID of the appointment.",
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean",
     *                 example="false"
     *             ),
     *             @OA\Property(
     *                 property="appointments",
     *                 type="array",
     *                 @OA\Items(
     *                      type="object",
     *                      @OA\Property(property="ID", type="uuid", example="9F4B897E-D7A9-EB11-AAE9-02E7A59D591E"),
     *                      @OA\Property(property="PatientID", type="uuid", example="0F7A3351-9708-4A6E-A862-F8ED22C2B9C7"),
     *                      @OA\Property(property="PracticeID", type="integer", example="1"),
     *                      @OA\Property(property="AppointmentStartDate", type="datetime", example="2020-05-06 08:00:00.000"),
     *                      @OA\Property(property="AppointmentEndDate", type="datetime", example="2020-05-06 09:00:00.000"),
     *                      @OA\Property(property="AppointmentStatus", type="string", example="Confirmed"),
     *                      @OA\Property(property="provider_id", type="uuid", example="A8655881-5839-EB11-AAAA-02892B467D0E"),
     *                      @OA\Property(property="provider_name", type="string", example="Doe, John"),
     *                      @OA\Property(property="SMS_Flag", type="boolean", example="1"),
     *                      @OA\Property(property="Email_Flag", type="boolean", example="1"),
     *                      @OA\Property(property="rating", type="string", example="null"),
     *                      @OA\Property(property="review", type="string", example="null"),
     *                      @OA\Property(property="Notes", type="string", example="sample notes..."),
     *                      @OA\Property(property="duration", type="string", example="null"),
     *                      @OA\Property(property="location_id", type="string", example="null"),
     *                      @OA\Property(property="location_name", type="string", example="null"),
     *                      @OA\Property(property="reason_for_visit", type="string", example="ok")
     *                 ),
     *             ),
     *         )
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": true, "message": "Error Message"}
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": true, "message": "Error Message"}
     *         )
     *     )
     * )
     */
    public function getAppointmentsByProviderID(Request $request, $provider_id)
    {
        try
        {
            // set connection
            $connection = \DB::connection('sqlsrv')->getPdo();

            // execute appointment by date mobile
            $statement = $connection->prepare("
                EXEC dbo.sp_appointment_by_provider_mobile
                    @provider_id=:provider_id
            ");
            $statement->bindParam(':provider_id',$provider_id);
            $statement->execute();
            $appointments = $statement->fetchAll(PDO::FETCH_ASSOC);

            // return response
            return \Response::json([
                'error'=>false,
                'appointments'=>$appointments
            ],200);
        }
        catch(\Exception $e)
        {
            return \Response::json([
                'error'=>true,
                'message'=>$e->getMessage()
            ],500);
        }
    }

    /**
     * @OA\Post(
     *     path="/api/v1/appointment",
     *     tags={"Appointment Management"},
     *     description="Store appointment",
     *     summary="Store Appointment",
     *     security={
     *       {"passport": {}},
     *     },
     *     @OA\RequestBody(
     *       required=true,
     *       description="Request body for adding appointment",
     *       @OA\JsonContent(
     *           required={},
     *           @OA\Property(property="patient_id", type="uuid", example="0F7A3351-9708-4A6E-A862-F8ED22C2B9C7"),
     *           @OA\Property(property="appointment_startdate", type="datetime", example="2020-05-06 08:00:00"),
     *           @OA\Property(property="appointment_enddate", type="datetime", example="2020-05-06 09:00:00"),
     *           @OA\Property(property="appointment_status", type="string", example="Confirmed"),
     *           @OA\Property(property="provider_id", type="uuid", example="A8655881-5839-EB11-AAAA-02892B467D0E"),
     *           @OA\Property(property="remind_email", type="string", example="1"),
     *           @OA\Property(property="remind_sms", type="boolean", example="1"),
     *           @OA\Property(property="rating", type="string", example="null"),
     *           @OA\Property(property="review", type="string", example="null"),
     *           @OA\Property(property="notes", type="string", example="sample notes..."),
     *           @OA\Property(property="duration", type="string", example="null"),
     *           @OA\Property(property="created_by", type="uuid", example="0F7A3351-9708-4A6E-A862-F8ED22C2B9C7"),
     *       ),
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": false, "message": "Appointment is successfully added!"}
     *         )
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": true, "message": "Error Message"}
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": true, "message": "Error Message"}
     *         )
     *     )
     * )
     */
    public function store(Request $request)
    {
        try
        {
            // get error
            $error = static::validateRequest(
                \Validator::make($request->all(), [
                    'patient_id' => 'required',
                    'appointment_startdate' => 'required',
                    'appointment_enddate' => 'required',
                    'appointment_status' => 'required',
                    'provider_id' => 'required',
                    'remind_email' => 'required',
                    'remind_sms' => 'required',
                    'rating' => 'required',
                    'review' => 'required',
                    'duration' => 'required',
                    'created_by' => 'required'
                ])
            );

            // count error
            if (count($error) > 0)
            {
                return \Response::json([
                    'error' => true,
                    'message' => $error
                ], 400);
            }

            # set connection
            $connection = \DB::connection('sqlsrv')->getPdo();

            // extract all
            extract($request->all());

            // set notes
            $notes = (isset($notes)&&!empty($notes) ? $notes : '');

            //dd($request->all());

            // start transaction
            \DB::connection('sqlsrv')->beginTransaction();

            // execute patient mobile
            $statement = $connection->prepare("
                EXEC dbo.spm_appointment_mobile
                    @flag=0,
                    @patient_id=:patient_id,
                    @practice_id=1,
                    @appointment_startdate=:appointment_startdate,
                    @appointment_enddate=:appointment_enddate,
                    @appointment_status=:appointment_status,
                    @provider_id=:provider_id,
                    @remind_email=:remind_email,
                    @remind_sms=:remind_sms,
                    @rating=:rating,
                    @review=:review,
                    @duration=:duration,
                    @location_id=NULL,
                    @notes=:notes,
                    @created_by=:created_by,
                    @id=NULL
            ");
            $statement->bindParam(':patient_id',$patient_id);
            $statement->bindParam(':appointment_startdate',$appointment_startdate);
            $statement->bindParam(':appointment_enddate',$appointment_enddate);
            $statement->bindParam(':appointment_status',$appointment_status);
            $statement->bindParam(':provider_id',$provider_id);
            $statement->bindParam(':remind_email',$remind_email);
            $statement->bindParam(':remind_sms',$remind_sms);
            $statement->bindParam(':rating',$rating);
            $statement->bindParam(':review',$review);
            $statement->bindParam(':duration',$duration);
            $statement->bindParam(':notes',$notes);
            $statement->bindParam(':created_by',$created_by);
            $statement->execute();

            // commit
            \DB::connection('sqlsrv')->commit();

            // return response
            return \Response::json([
                'error'=>false,
                'message'=>'Appointment is successfully added!'
            ],200);
        }
        catch(\Exception $e)
        {
            return \Response::json([
                'error'=>true,
                'message'=>$e->getMessage()
            ],500);
        }
    }

    /**
     * @OA\Get(
     *     path="/api/v1/appointment/{id}",
     *     tags={"Appointment Management"},
     *     description="API to get appointment",
     *     summary="Show Appointment",
     *     security={
     *       {"passport": {}},
     *     },
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         required=true,
     *         description="ID of the appointment.",
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean",
     *                 example="false"
     *             ),
     *             @OA\Property(
     *                 property="appointment",
     *                      @OA\Property(property="ID", type="uuid", example="9F4B897E-D7A9-EB11-AAE9-02E7A59D591E"),
     *                      @OA\Property(property="PatientID", type="uuid", example="0F7A3351-9708-4A6E-A862-F8ED22C2B9C7"),
     *                      @OA\Property(property="PracticeID", type="integer", example="1"),
     *                      @OA\Property(property="AppointmentStartDate", type="datetime", example="2020-05-06 08:00:00.000"),
     *                      @OA\Property(property="AppointmentEndDate", type="datetime", example="2020-05-06 09:00:00.000"),
     *                      @OA\Property(property="AppointmentStatus", type="string", example="Confirmed"),
     *                      @OA\Property(property="provider_id", type="uuid", example="A8655881-5839-EB11-AAAA-02892B467D0E"),
     *                      @OA\Property(property="provider_name", type="string", example="Doe, John"),
     *                      @OA\Property(property="SMS_Flag", type="boolean", example="1"),
     *                      @OA\Property(property="Email_Flag", type="boolean", example="1"),
     *                      @OA\Property(property="rating", type="string", example="null"),
     *                      @OA\Property(property="review", type="string", example="null"),
     *                      @OA\Property(property="notes", type="string", example="sample notes..."),
     *                      @OA\Property(property="duration", type="string", example="null"),
     *                      @OA\Property(property="location_name", type="string", example="null"),

     *             ),
     *         )
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": true, "message": "Error Message"}
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": true, "message": "Error Message"}
     *         )
     *     )
     * )
     */
    public function show($id): JsonResponse
    {
        try
        {
            // set connection
            $connection = app('sql_srv_connection')->getPdo();

            // execute appointment by id mobile
            $statement = $connection->prepare("EXEC dbo.sp_appointment_by_id_mobile @appointment_id = :appointment_id");
            $statement->execute([':appointment_id' => $id]);
            $appointment = $statement->fetch(PDO::FETCH_ASSOC);

            // return response
            return response()->json([
                'error'=>false,
                'appointment'=>$appointment
            ]);
        }
        catch(\Exception $e)
        {
            return response()->json([
                'error'=>true,
                'message'=>$e->getMessage()
            ],500);
        }
    }

    /**
     * @OA\Put(
     *     path="/api/v1/appointment/{id}",
     *     tags={"Appointment Management"},
     *     description="API to update appointment",
     *     summary="Update Appointment",
     *     security={
     *       {"passport": {}},
     *     },
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         required=true,
     *         description="ID of the appointment.",
     *     ),
     *     @OA\RequestBody(
     *       required=true,
     *       description="Request body to update appointment",
     *       @OA\JsonContent(
     *           required={},
     *           @OA\Property(property="patient_id", type="uuid", example="0F7A3351-9708-4A6E-A862-F8ED22C2B9C7"),
     *           @OA\Property(property="appointment_startdate", type="datetime", example="2020-05-06 08:00:00"),
     *           @OA\Property(property="appointment_enddate", type="datetime", example="2020-05-06 09:00:00"),
     *           @OA\Property(property="appointment_status", type="string", example="Confirmed"),
     *           @OA\Property(property="provider_id", type="uuid", example="A8655881-5839-EB11-AAAA-02892B467D0E"),
     *           @OA\Property(property="remind_email", type="string", example="1"),
     *           @OA\Property(property="remind_sms", type="boolean", example="1"),
     *           @OA\Property(property="rating", type="string", example="null"),
     *           @OA\Property(property="review", type="string", example="null"),
     *           @OA\Property(property="notes", type="string", example="sample notes..."),
     *           @OA\Property(property="duration", type="string", example="null"),
     *           @OA\Property(property="created_by", type="uuid", example="0F7A3351-9708-4A6E-A862-F8ED22C2B9C7"),
     *       ),
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": false, "message": "Appointment is successfully updated!"}
     *         )
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": true, "message": "Error Message"}
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": true, "message": "Error Message"}
     *         )
     *     )
     * )
     */
    public function update(Request $request, $id)
    {
        try
        {
            # set connection
            $connection = \DB::connection('sqlsrv')->getPdo();

            // extract all
            extract($request->all());

            // optional
            $patient_id = (isset($patient_id)&&!empty($patient_id) ? $patient_id : '');
            $appointment_startdate = (isset($appointment_startdate)&&!empty($appointment_startdate) ? $appointment_startdate : '');
            $appointment_enddate = (isset($appointment_enddate)&&!empty($appointment_enddate) ? $appointment_enddate : '');
            $appointment_status = (isset($appointment_status)&&!empty($appointment_status) ? $appointment_status : '');
            $provider_id = (isset($provider_id)&&!empty($provider_id) ? $provider_id : NULL);
            $remind_email = (isset($remind_email)&&!empty($remind_email) ? $remind_email : '');
            $remind_sms = (isset($remind_sms)&&!empty($remind_sms) ? $remind_sms : '');
            $rating = (isset($rating)&&!empty($rating) ? $rating : '');
            $review = (isset($review)&&!empty($review) ? $review : '');
            $duration = (isset($duration)&&!empty($duration) ? $duration : '');
            $notes = (isset($notes)&&!empty($notes) ? $notes : '');
            $created_by = (isset($created_by)&&!empty($created_by) ? $created_by : NULL);

            // start transaction
            \DB::connection('sqlsrv')->beginTransaction();

            // execute patient mobile
            $statement = $connection->prepare("
                EXEC dbo.spm_appointment_mobile
                    @flag=1,
                    @patient_id=:patient_id,
                    @practice_id=1,
                    @appointment_startdate=:appointment_startdate,
                    @appointment_enddate=:appointment_enddate,
                    @appointment_status=:appointment_status,
                    @provider_id=:provider_id,
                    @remind_email=:remind_email,
                    @remind_sms=:remind_sms,
                    @rating=:rating,
                    @review=:review,
                    @duration=:duration,
                    @location_id=NULL,
                    @notes=:notes,
                    @created_by=:created_by,
                    @id=:id
            ");
            $statement->bindParam(':patient_id',$patient_id);
            $statement->bindParam(':appointment_startdate',$appointment_startdate);
            $statement->bindParam(':appointment_enddate',$appointment_enddate);
            $statement->bindParam(':appointment_status',$appointment_status);
            $statement->bindParam(':provider_id',$provider_id);
            $statement->bindParam(':remind_email',$remind_email);
            $statement->bindParam(':remind_sms',$remind_sms);
            $statement->bindParam(':rating',$rating);
            $statement->bindParam(':review',$review);
            $statement->bindParam(':duration',$duration);
            $statement->bindParam(':notes',$notes);
            $statement->bindParam(':created_by',$created_by);
            $statement->bindParam(':id',$id);
            $statement->execute();

            // commit
            \DB::connection('sqlsrv')->commit();

            // return response
            return \Response::json([
                'error'=>false,
                'message'=>'Appointment is successfully updated!'
            ],200);
        }
        catch(\Exception $e)
        {
            return \Response::json([
                'error'=>true,
                'message'=>$e->getMessage()
            ],500);
        }
    }

    /**
     * @OA\Delete(
     *     path="/api/v1/appointment/{id}",
     *     tags={"Appointment Management"},
     *     description="API to delete appointment",
     *     summary="Delete Appointment",
     *     security={
     *       {"passport": {}},
     *     },
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         required=true,
     *         description="ID of the appointment.",
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": false, "message": "Appointment is successfully deleted!"}
     *         )
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": true, "message": "Error Message"}
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": true, "message": "Error Message"}
     *         )
     *     )
     * )
     */
    public function destroy($id)
    {
        try
        {
            # set connection
            $connection = \DB::connection('sqlsrv')->getPdo();

            // start transaction
            \DB::connection('sqlsrv')->beginTransaction();

            // execute patient mobile
            $statement = $connection->prepare("
                EXEC dbo.spm_appointment_mobile
                    @flag=2,
                    @patient_id=NULL,
                    @practice_id=NULL,
                    @appointment_startdate='',
                    @appointment_enddate='',
                    @appointment_status='',
                    @provider_id=NULL,
                    @remind_email='',
                    @remind_sms='',
                    @rating='',
                    @review='',
                    @duration='',
                    @location_id=NULL,
                    @notes='',
                    @created_by=NULL,
                    @id=:id
            ");
            $statement->bindParam(':id',$id);
            $statement->execute();

            // commit
            \DB::connection('sqlsrv')->commit();

            // return response
            return \Response::json([
                'error'=>false,
                'message'=>'Appointment is successfully deleted!'
            ],200);
        }
        catch(\Exception $e)
        {
            return \Response::json([
                'error'=>true,
                'message'=>$e->getMessage()
            ],500);
        }
    }

    /**
     * @OA\Post(
     *     path="/api/v1/appointment/dates/get-provider-schedule-dates",
     *     tags={"Appointment Management"},
     *     description="Get the schedule dates of a specific provider.",
     *     summary="Provider Available Dates",
     *     security={
     *       {"passport": {}},
     *     },
     *     @OA\RequestBody(
     *       required=true,
     *       description="Description",
     *       @OA\JsonContent(
     *           @OA\Property(property="start_date", type="date", example="6/1/2021", description="First day of current month displayed in calendar date picker. (M/D/YYYY format)"),
     *           @OA\Property(property="end_date", type="date", example="6/30/2021", description="Last day of current month displayed in calendar date picker. (M/D/YYYY format)"),
     *           @OA\Property(property="provider_id", type="uuid", example="9F9B86B8-71A3-EB11-AAE9-02E7A59D591E", description="Provider ID"),
     *       ),
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *         @OA\JsonContent(
     *             @OA\Property(property="disable_dates", type="string", example={"06/03/2021","06/04/2021","06/06/2021"}),
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean",
     *                 example=false
     *             ),
     *             @OA\Property(
     *                 property="schedule_timeslot",
     *                 type="array",
     *                 @OA\Items(
     *                      type="object",
     *                      @OA\Property(property="06/03/2021", type="string", example={"06:00 AM","06:30 AM","07:00 AM"}),
     *                 ),
     *             ),
     *         )
     *     ),
     *         )
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": true, "message": "Error Message"}
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": true, "message": "Error Message"}
     *         )
     *     )
     * )
     */
    public function getProviderUnavailableDates(Request $request)
    {
        $practice_id = env('PRACTICE_ID');
        $params = $request->all();

        $provider_id = $params['provider_id'];

        $start = @$params['start_date'];
        $end = @$params['end_date'];

        $period = new \DatePeriod(
             new \DateTime($start),
             new \DateInterval('P1D'),
             new \DateTime($end.' +1 day')
        );

        foreach($period as $date) {
            $dates[] = $date->format('m/d/Y');
        }

        try
        {
            $schedules = [];
            $disable_dates = [];

            foreach($dates as $date) {

                $timeslots = $this->getVisitTimesByProvider($date, $provider_id);

                if(count($timeslots) > 0) {
                    $schedules[$date] = $timeslots;
                } else {
                    $disable_dates[] = $date;
                }
            }

            // return response
            return \Response::json([
                'error' => false,
                'schedule_timeslot' => $schedules,
                'disable_dates' => $disable_dates,
            ],200);
        }
        catch(\Exception $e)
        {
            return \Response::json([
                'error'=>true,
                'message'=>$e->getMessage()
            ],500);
        }
    }


    private function getVisitTimesByProvider($date, $provider_id)
    {
        $practice_id = env('PRACTICE_ID');
        $provider_id = $provider_id;
        $visit_times = [];

        if (! $date) {
            $date = now()->toDateString();
        }

        $date = now()->change($date)->format('m/d/Y');

        $schedules = $this->getProviderSchedule($provider_id, $date);
        $appointments = $this->getProviderAppointments($provider_id, $date);
        $appointments = Arr::pluck($appointments, 'appointment_dates');
        $appointments = array_map(function($appointment) {
            return now()->change($appointment)->format('h:i A');
        }, $appointments);

        foreach ($schedules as $schedule) {
            if (! in_array($schedule, $appointments)) {
                $visit_times[] = $schedule;
            }
        }

        return $visit_times;
    }

    private function getProviderSchedule($provider_id, $date)
    {
        $result = [];
        $practice_id = env('PRACTICE_ID');

        $statement = $this->connection->prepare(
            "EXEC sp_provider_schedule
                @practice_id = :practice_id,
                @provider_id = :provider_id"
        );

        $statement->bindParam(':practice_id', $practice_id);
        $statement->bindParam(':provider_id', $provider_id);
        $statement->execute();
        $schedules = $statement->fetchAll(PDO::FETCH_ASSOC);

        $dayOfWeek = strtolower(now()->change($date)->format('l'));

        $schedules = Arr::where($schedules, function($value, $key) use ($dayOfWeek) {
            return $value['days'] == $dayOfWeek;
        });

        foreach ($schedules as $schedule) {
            $scheduleArr = [];

            if($schedule['time_open'] != null && $schedule['time_close'] != null) {
                $current = @now()->change($schedule['time_open']);
                $carbon_end = Carbon::parse($schedule['time_close'])->subMinutes(30);
                $end = @now()->change($carbon_end);
                $scheduleArr = $this->getScheduleList($date, $current, $end);
            }

            $result = array_merge($result, $scheduleArr);
        }

        return $result;
    }

    private function getProviderAppointments($provider_id, $date)
    {
        $statement = $this->connection->prepare(
            "EXEC sp_provider_appointment_by_date
                @provider_id = :provider_id,
                @appointment_date = :appointment_date"
        );

        $statement->bindParam(':provider_id', $provider_id);
        $statement->bindParam(':appointment_date', $date);
        $statement->execute();
        return $statement->fetchAll(PDO::FETCH_ASSOC);
    }

    private function getScheduleList($date, $current, $end)
    {
        $result = [];

        if ($current && $end) {
            $count = 0;
            while ($end->gt($current)) {
                if ($count > 0) {
                    $current = now()
                        ->change($current)
                        ->addMinutes(30);
                }
                if (now()->change($date)->isToday()) {
                    if (now()->change($date)->lte($current)) {
                        $result[] = $current->format('h:i A');
                    }
                } else {
                    $result[] = $current->format('h:i A');
                }

                $count++;
            }
        }

        return $result;
    }
}
