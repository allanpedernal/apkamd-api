<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;

use App\Services\SmsService;
use App\Mail\Register\SendEmailVerification;

use App\User;
use App\Person;

use App\Http\Requests\Register\RegisterEmailVerificationRequest;
use App\Http\Requests\Register\RegisterMobileVerificationRequest;
use Illuminate\Support\Str;
use PDO;

/**
 * @group  Authentication
 *
 * APIs for authentication
 */
class AuthenticateController extends Controller
{
    /**
     * @OA\Post(
     *     path="/api/v1/auth/token",
     *     tags={"Token"},
     *     description="Description",
     *     summary="Generate Token",
     *     @OA\RequestBody(
     *         description="Client Credential",
     *         required=true,
     *         @OA\MediaType(
     *              mediaType="multipart/form-data",
     *              @OA\Schema(
     *                  @OA\Property(
     *                      property="client_key",
     *                      type="string"
     *                   ),
     *                  @OA\Property(
     *                      property="client_secret",
     *                      type="string"
     *                   ),
     *               ),
     *           ),
     *         @OA\MediaType(
     *              mediaType="application/json",
     *              @OA\Schema(
     *                  @OA\Property(
     *                      property="client_key",
     *                      type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="client_secret",
     *                     type="string"
     *                 ),
     *                 example={"client_key": "sample client id", "client_secret": "client secret here"}
     *              ),
     *          )
     *      ),
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="token_type",
     *                 type="string"
     *             ),
     *             @OA\Property(
     *                 property="expires_in",
     *                 type="integer"
     *             ),
     *             @OA\Property(
     *                 property="access_token",
     *                 type="string"
     *             ),
     *             example={"token_type": "Bearer", "expires_in": 1234567, "access_token": "some jwt"}
     *        )
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Error"
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Error"
     *     )
     * )
     */
    public function authToken(Request $request)
    {
        try
        {
            // get error
            $error = static::validateRequest(
                \Validator::make($request->all(), [
                    'client_key' => 'required',
                    'client_secret' => 'required'
                ])
            );

            // count error
            if (count($error) > 0)
            {
                // return response
                return \Response::json([
                    'error' => true,
                    'message' => $error
                ], 400);
            }

            // get client credentials
            $client_key = $request->client_key;
            $client_secret = $request->client_secret;

            // get response
            $response = Http::asForm()->post(url('oauth/token'), [
                'grant_type' => 'client_credentials',
                'client_id' => $client_key,
                'client_secret' => $client_secret,
                'scope' => '',
            ]);

            // return token
            return \Response::json(json_decode($response->body(),true),$response->status());
        }
        catch(Exception $e)
        {
            // return response
            return \Response::json([
                'error'=>true,
                'message'=>$e->getMessage()
            ],500);
        }
    }

    /**
     * @OA\Get(
     *     path="/api/v1/email-verification",
     *     tags={"Authentication Management"},
     *     description="API to send email verification code",
     *     summary="Send Email Verification",
     *     security={
     *       {"passport": {}},
     *     },
     *     @OA\Parameter(
     *         name="email",
     *         in="query",
     *         required=true,
     *         description="Email address of the user (ex. jane.doe@example.com).",
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": false, "message": "Email verification code is successfully sent!"}
     *         )
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": true, "message": "Error Message"}
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": true, "message": "Error Message"}
     *         )
     *     )
     * )
     */
    public function sendEmailVerification(RegisterEmailVerificationRequest $request)
    {
        try
        {
            // set connection
            $connection = \DB::connection('sqlsrv')->getPdo();

            $email = $request->input('email');
            $code = rand(100000, 999999);

            // execute spm email
            $statement = $connection->prepare("
                EXEC spm_email
                    @email = :email,
                    @code = :code
            ");
            $statement->bindParam(':email', $email);
            $statement->bindParam(':code', $code);
            $statement->execute();

            // send email
            \Mail::to($email)->send(new SendEmailVerification($code));

            // return response
            return \Response::json([
                'error'=>false,
                'message'=>'Email verification code is successfully sent!'
            ],200);
        }
        catch (Exception $e)
        {
            return \Response::json([
                'error'=>true,
                'message'=>$e->getMessage()
            ],500);
        }
    }

    /**
     * @OA\Post(
     *     path="/api/v1/email-verification",
     *     tags={"Auth"},
     *     description="API to verify email verification code",
     *     summary="Verify Email Verfification Code",
     *     security={
     *       {"passport": {}},
     *     },
     *     @OA\RequestBody(
     *         description="Verification Code",
     *         required=true,
     *         @OA\MediaType(
     *              mediaType="application/json",
     *              @OA\Schema(
     *                  @OA\Property(
     *                      property="email",
     *                      type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="code",
     *                     type="integer"
     *                 ),
     *                 @OA\Property(
     *                     property="counter",
     *                     type="integer"
     *                 ),
     *                 example={"email": "", "code": "", "counter": ""}
     *              ),
     *          )
     *      ),
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": false, "message": "Email is successfully verified!"}
     *         )
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": true, "message": "Error Message"}
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": true, "message": "Error Message"}
     *         )
     *     )
     * )
     */
    public function verifyEmail(Request $request)
    {
        try
        {
            // (new RegisterVerification)->getByEmail();
            $codeMatch = false;
            $codeValid = false;

            // set connection
            $connection = \DB::connection('sqlsrv')->getPdo();

            // get params
            $email = $request->input('email');

            // execute sp email list
            $statement = $connection->prepare(
                "EXEC sp_email_list
                    @email = :email"
            );
            $statement->bindParam(':email', $email);
            $statement->execute();
            $result = $statement->fetch(PDO::FETCH_ASSOC);

            // Check if email is found
            if (! $result)
            {
                // return response
                return \Response::json([
                    'error'=>true,
                    'message'=>'The verification code is incorrect.'
                ],400);
            }

            // Check if code matches
            if ($result['code'] == $request->input('code'))
            {
                $codeMatch = true;
            }

            // Check if verification is expired
            if ($request->input('counter') > 0)
            {
                $codeValid = true;
            }

            // Show error message if code mismatch
            if (! $codeMatch)
            {
                // return response
                return \Response::json([
                    'error'=>true,
                    'message'=>'The verification code is incorrect.'
                ],400);
            }

            // Show error message if expired
            if (! $codeValid)
            {
                // return response
                return \Response::json([
                    'error'=>true,
                    'message'=>'The verification code is expired.'
                ],400);
            }

            // return response
            return \Response::json([
                'error'=>false,
                'message'=>'Email is successfully verified!'
            ],200);
        }
        catch (Exception $e)
        {
            return \Response::json([
                'error'=>true,
                'message'=>$e->getMessage()
            ],500);
        }
    }

    /**
     * @OA\Get(
     *     path="/api/v1/sms-verification",
     *     tags={"Authentication Management"},
     *     description="API to send sms verification code",
     *     summary="Send SMS Verification",
     *     security={
     *       {"passport": {}},
     *     },
     *     @OA\Parameter(
     *         name="mobile",
     *         in="query",
     *         required=true,
     *         description="Mobile number of the user (ex. 01234567890).",
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": false, "message": "SMS verification code is successfully sent!"}
     *         )
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": true, "message": "Error Message"}
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": true, "message": "Error Message"}
     *         )
     *     )
     * )
     */
    public function sendSmsVerification(RegisterMobileVerificationRequest $request)
    {
        try
        {
            // set connection
            $connection = \DB::connection('sqlsrv')->getPdo();

            // get params
            $mobile = \Str::slug($request->input('mobile'), '');
            $code = rand(100000, 999999);

            // execute spm_mobile_list
            $statement = $connection->prepare(
                "EXEC dbo.spm_mobile_list
                    @mobile = :mobile,
                    @code = :code"
            );
            $statement->bindParam(':mobile', $mobile);
            $statement->bindParam(':code', $code);
            $statement->execute();

            // set message
            $message = '[' . env('APP_NAME') . '] To verify your mobile number, please enter this verification code: ' . $code;
            SmsService::send($mobile, $message);

            // return response
            return \Response::json([
                'error'=>false,
                'message'=>'SMS verification code is successfully sent!'
            ],200);
        }
        catch(Exception $e)
        {
            return \Response::json([
                'error'=>true,
                'message'=>$e->getMessage()
            ],500);
        }
    }

    /**
     * @OA\Post(
     *     path="/api/v1/sms-verification",
     *     tags={"Auth"},
     *     description="API to verify sms verification code",
     *     summary="Verify Email Verfification Code",
     *     security={
     *       {"passport": {}},
     *     },
     *     @OA\RequestBody(
     *         description="Verification Code",
     *         required=true,
     *         @OA\MediaType(
     *              mediaType="application/json",
     *              @OA\Schema(
     *                  @OA\Property(
     *                      property="mobile",
     *                      type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="code",
     *                     type="integer"
     *                 ),
     *                 @OA\Property(
     *                     property="counter",
     *                     type="integer"
     *                 ),
     *                 example={"mobile": "", "code": "", "counter": ""}
     *              ),
     *          )
     *      ),
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": false, "message": "SMS is successfully verified!"}
     *         )
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": true, "message": "Error Message"}
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": true, "message": "Error Message"}
     *         )
     *     )
     * )
     */
    public function verifySms(Request $request)
    {
        try
        {
            $codeMatch = false;
            $codeValid = false;

            $mobile = \Str::slug($request->input('mobile'), '');

            // set connection
            $connection = \DB::connection('sqlsrv')->getPdo();

            // execute sp mobile list
            $statement = $connection->prepare(
                "EXEC dbo.sp_mobile_list
                    @mobile = :mobile"
            );
            $statement->bindParam(':mobile', $mobile);
            $statement->execute();
            $result = $statement->fetch(PDO::FETCH_ASSOC);

            // Check if email is found
            if (! $result)
            {
                // return response
                return \Response::json([
                    'error'=>true,
                    'message'=>'The verification code is incorrect.'
                ],400);
            }

            // Check if code matches
            if ($result['code'] == $request->input('code')) {
                $codeMatch = true;
            }

            // Check if verification is expired
            if ($request->input('counter') > 0) {
                $codeValid = true;
            }

            // Show error message if code mismatch
            if (! $codeMatch)
            {
                // return response
                return \Response::json([
                    'error'=>true,
                    'message'=>'The verification code is incorrect.'
                ],400);
            }

            // Show error message if expired
            if (! $codeValid)
            {
                // return response
                return \Response::json([
                    'error'=>true,
                    'message'=>'The verification code is expired.'
                ],400);
            }

            // return response
            return \Response::json([
                'error'=>false,
                'message'=>'SMS is successfully verified!'
            ],200);
        }
        catch(Exception $e)
        {
            return \Response::json([
                'error'=>true,
                'message'=>$e->getMessage()
            ],500);
        }
    }

    /**
     * @OA\Post(
     *     path="/api/v1/auth/login",
     *     tags={"Auth"},
     *     description="Description",
     *     summary="Patient Login",
     *     security={
     *       {"passport": {}},
     *     },
     *     @OA\RequestBody(
     *         description="Patient Credential",
     *         required=true,
     *         @OA\MediaType(
     *              mediaType="application/json",
     *              @OA\Schema(
     *                  @OA\Property(
     *                      property="username",
     *                      type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="password",
     *                     type="string"
     *                 ),
     *                 example={"username": "", "password": ""}
     *              ),
     *          )
     *      ),
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="message",
     *                 type="string",
     *                 example="Successfully logged in."
     *             ),
     *             @OA\Property(
     *                 property="person_id",
     *                 type="string",
     *                 example="8EF31034-589C-EB11-AAE9-02E7A59D591E"
     *             ),
     *             @OA\Property(
     *                 property="user",
     *                 @OA\Property(property="users_id", type="string", example="8FF31034-589C-EB11-AAE9-02E7A59D591E"),
     *                 @OA\Property(property="patient_id", type="string", example="05893B9A-599C-EB11-AAE9-02E7A59D591E"),
     *                 @OA\Property(property="person_id", type="string", example="8EF31034-589C-EB11-AAE9-02E7A59D591E"),
     *                 @OA\Property(property="lastname", type="string", example="John"),
     *                 @OA\Property(property="firstname", type="string", example="Doe"),
     *                 @OA\Property(property="dob", type="string", example="1960-06-01 00:00:00.000"),
     *                 @OA\Property(property="gender", type="string", example="M"),
     *                 @OA\Property(property="amd_chart", type="string", example="2691"),
     *                 @OA\Property(property="amd_responsible_party", type="[string, NULL]", example="test"),
     *                 @OA\Property(property="advancedmd_patient_id", type="string", example="pat5983048"),
     *             ),
     *        )
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Error"
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Error"
     *     )
     * )
     */
    public function authLogin(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'username' => 'required|string|min:5|max:255',
            'password' => 'required|string|min:5',
        ]);

        if ($validator->fails())
        {
            return response(['errors'=>$validator->errors()->all()], 400);
        }

        $user = User::where('username', $request->username)->first();

        if ($user) {
            if (\Hash::check($request->password, $user->password)) {

                $person = Person::where('ID', $user->personid)->first();

                if((!$person->completed_registration || $person->completed_registration == 'false') && $person->current_page !== null) {

                    $response = [
                        'message' => 'Incomplete Registration',
                        'person_id' => $user->personid,
                        'completed_registration' => false,
                    ];
                    return response($response, 422);

                }

                $connection = \DB::connection('sqlsrv')->getPdo();

                $user_id = $user->id;

                $username = $request->username;
                $password = $user->password;

                $statement = $connection->prepare("
                    EXEC dbo.sp_login_patient_mobile
                        @username=:username,
                        @password=:password
                ");
                $statement->bindParam(':username',$username);
                $statement->bindParam(':password',$password);
                $statement->execute();
                $query_user = $statement->fetch(PDO::FETCH_ASSOC);

                // $statement = $connection->prepare("
                //     EXEC dbo.sp_user_by_id_mobile
                //         @user_id=:user_id
                // ");
                // $statement->bindParam(':user_id',$user_id);
                // $statement->execute();
                // $query_user = $statement->fetch(\PDO::FETCH_ASSOC);

                $response = [
                    'user' => $query_user,
                    'person_id' => $user->personid,
                    'message' => 'Successfully logged in.',
                ];

                return response($response, 200);
            } else {
                $response = ["message" => "Invalid username and password combination"];
                return response($response, 422);
            }
        } else {
            $response = ["message" =>'User does not exist'];
            return response($response, 422);
        }
    }

    /**
     * @OA\Post(
     *     path="/api/v1/auth/register",
     *     tags={"Auth"},
     *     description="Description",
     *     summary="Patient Registration",
     *     security={
     *       {"passport": {}},
     *     },
     *     @OA\RequestBody(
     *       required=true,
     *       description="Description",
     *       @OA\JsonContent(
     *           required={},
     *           @OA\Property(property="lastname", type="string", example="david"),
     *           @OA\Property(property="firstname", type="string", example="pomeranian"),
     *           @OA\Property(property="dob", type="string", example="12/25/2020"),
     *           @OA\Property(property="preferred_language", type="string", example="English"),
     *           @OA\Property(property="gender", type="string", example="M"),
     *           @OA\Property(property="email", type="string", example="testpatient@gmail.com"),
     *           @OA\Property(property="mobile", type="string", example="0357778995"),
     *           @OA\Property(property="username", type="string", example="testpatient"),
     *           @OA\Property(property="password", type="string", example="password1"),
     *           @OA\Property(property="street_address", type="string", example="#25 Hallmark St."),
     *           @OA\Property(property="street_address_2", type="string", example=""),
     *           @OA\Property(property="city", type="string", example="Brooklyn"),
     *           @OA\Property(property="state", type="string", example="CA"),
     *           @OA\Property(property="country", type="string", example="US"),
     *           @OA\Property(property="zip_code", type="string", example="2374"),
     *           @OA\Property(property="timezone", type="string", example="Eastern standard time"),
     *           @OA\Property(property="person_id", type="uuid", example=""),
     *       ),
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": false, "message": "Successfully registered.", "patient_id": "4A94D2CE-0BBF-EB11-AAE9-02E7A59D591E", "person_id": "4994D2CE-0BBF-EB11-AAE9-02E7A59D591E"}
     *         )
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": true, "message": "Error Message"}
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": true, "message": "Error Message"}
     *         )
     *     )
     * )
     */
    public function authRegister(Request $request)
    {
        try
        {
            // set connection
            $connection = app('db_connection', ['sqlsrv'])->getPdo();

            app('db_connection', ['sqlsrv'])->beginTransaction();
            // extract
            extract($request->all());

            // optional
            $patient_source = 'self-register';
            $password = bcrypt($password);
            $practice_id = env('PRACTICE_ID');
            $mobile = Str::slug($mobile, '');
            $dob = now()->change($dob)->format('m/d/Y');

            $flag = 0;
            if($person_id == '' || $person_id == null) {
                $flag = 0;
                $person_id = null;
            } else {
                $flag = 1;
            }

            // execute patient list mobile
            $statement = $connection->prepare("
                EXEC spm_basic_demographics_account_create
                    @flag = :flag,
                    @patient_source = :patient_source,
                    @lastname = :lastname,
                    @firstname  = :firstname,
                    @dob = :dob,
                    @preferred_language = :preferred_language,
                    @gender = :gender,
                    @email = :email,
                    @mobile = :mobile,
                    @practice_id = :practice_id,
                    @username = :username,
                    @password = :password,
                    @id = :id
            ");
            $statement->bindParam(':flag',$flag);
            $statement->bindParam(':patient_source',$patient_source);
            $statement->bindParam(':lastname',$lastname);
            $statement->bindParam(':firstname',$firstname);
            $statement->bindParam(':dob',$dob);
            $statement->bindParam(':preferred_language',$preferred_language);
            $statement->bindParam(':gender',$gender);
            $statement->bindParam(':email',$email);
            $statement->bindParam(':mobile',$mobile);
            $statement->bindParam(':practice_id',$practice_id);
            $statement->bindParam(':username',$username);
            $statement->bindParam(':password',$password);
            $statement->bindParam(':id',$person_id);
            $statement->execute();
            $patient = $statement->fetch(PDO::FETCH_ASSOC);

            $result = [
                'person_id' => '',
                'patient_id' => '',
            ];

            if (is_array($patient)) {

                // on some instance computed is the key used in the db response
                if (isset($patient['computed'])) {
                    $patient = $patient['computed'];
                } else {
                    $patient = $patient[''];
                }

                $patient = explode('|', $patient);

                foreach ($patient as $property) {
                    $properties = explode(': ', $property);
                    $result = array_merge($result, [
                        $properties[0] => $properties[1]
                    ]);
                }
            }

            $statement2 = $connection->prepare("
                EXEC spm_address_and_contacts
                    @flag  = :flag,
                    @person_id  = :person_id,
                    @street_address  = :street_address,
                    @street_address_2  = :street_address_2,
                    @city  = :city,
                    @country  =:country,
                    @state = :state,
                    @zip_code  = :zip_code,
                    @timezone = :timezone
            ");
            $statement2->bindParam(':flag',$flag);
            $statement2->bindParam(':person_id',$result['person_id']);
            $statement2->bindParam(':street_address',$street_address);
            $statement2->bindParam(':street_address_2',$street_address_2);
            $statement2->bindParam(':city',$city);
            $statement2->bindParam(':country',$country);
            $statement2->bindParam(':state',$state);
            $statement2->bindParam(':zip_code',$zip_code);
            $statement2->bindParam(':timezone',$timezone);
            $statement2->execute();

            /*
             * amd utility
             * */

            #patient name
            $amd_patient_name = function() use (& $request){
                $firstname = $request->get('firstname');
                $lastname = $request->get('lastname');
                return trim($lastname . ',' . $firstname);
            };

            #marital status
            $amd_marital_status = function() use (& $request) {
                $marital_status = $request->get('marital_status');
                switch (strtoupper($marital_status)) {
                    case 'SINGLE' :
                        return '1';
                    case 'MARRIED' :
                        return '2';
                    case 'DIVORCED' :
                        return  '3';
                    case 'LEGALLY SEPARATED' :
                        return  '4';
                    case 'WIDOWED' :
                        return  '5';
                    case 'UNKNOWN' :
                        return '6';
                    default:
                        return '';
                }
            };

            $resolve_ssn = function () use (& $request) {
                $ssn = $request->get('ssn');
                $default = 'XXX-01-';
                $ssn = str_replace('-', '', $ssn);
                $str = '';
                if (strlen($ssn) === 4 AND $ssn !== '0000') {
                    $str = $default . $ssn;
                } else {
                    if (strlen($ssn) === 9) {
                        $ssn = substr($ssn, 5);
                        $str = $default . $ssn;
                    } elseif ('0' === $ssn OR '0000' === $ssn) {
                        $str =  $default . mt_rand(1000,9999);
                    }
                }
                return $str;
            };

            $amd_dob = Carbon::parse($request->get('dob'))->format('m/d/Y');
            $amd_contact_number = $request->get('mobile');
            $amd_gender = $request->get('gender');

            if($flag == 0) {

                $patient_data = [
                    'ppmdmsg' => [
                        '@action' => 'addpatient',
                        '@class' => 'api',
                        '@msgtime' => Carbon::now()->format('m/d/Y h:i:s A'),
                        'patientlist' => [
                            'patient' => [
                                '@respparty' => 'SELF',
                                '@name' => $amd_patient_name(),
                                '@sex' => strtoupper( $amd_gender ),
                                '@relationship' => '1',
                                '@hipaarelationship' => '18',
                                '@dob' => $amd_dob,
                                '@ssn' => $resolve_ssn(),
                                '@chart' => 'AUTO',
                                '@profile' => '18',
                                '@finclass' => '10',
                                '@maritalstatus' => $amd_marital_status(),
                                '@insorder' => '',
                                'address' => [
                                    '@zip' => $request->get('zip_code'),
                                    '@city' => $request->get('city'),
                                    '@state' => $request->get('state'),
                                    '@address1' => $request->get('street_address'),
                                    '@address2' => $request->get('street_address_2')
                                ],
                                'contactinfo' => [
                                    '@homephone' => $amd_contact_number
                                ]
                            ]
                        ],
                        'resppartylist' => [
                            'respparty' => [
                                '@name' => 'self',
                                '@accttype' => '4'
                            ]
                        ]
                    ]
                ];
                $add_patient_response = \AMD::init($patient_data)->process();
                $amd_result = $add_patient_response['PPMDResults'];
                $message = '';
                if (!$amd_result['Error']) {
                    if ($amd_result['Results']['@success'] === '1') {
                        $person_id = $result['person_id'];
                        $patient_id = $result['patient_id'];
                        $pat_id = $amd_result['Results']['patientlist']['patient']['@id'];
                        $chart = $amd_result['Results']['patientlist']['patient']['@chart'];
                        $responsible_party = $amd_result['Results']['patientlist']['patient']['@respparty'];

                        #update patient record.
                        $sql = "EXEC spm_update_patient_amd_data";
                        $sql .= " @patient_id = :patient_id";
                        $sql .= " ,@advancedmd_patient_id = :advancedmd_patient_id";
                        $sql .= " ,@amd_chart = :amd_chart";
                        $sql .= " ,@amd_responsible_party = :amd_responsible_party";
                        $sql .= " ,@user_id = :user_id";
                        $statement = $connection->prepare($sql);
                        $statement->execute([
                            ':patient_id' => $patient_id,
                            ':advancedmd_patient_id' => $pat_id,
                            ':amd_chart' => $chart,
                            ':amd_responsible_party' => $responsible_party,
                            ':user_id' => $person_id
                        ]);

                        $message = "Successfully registered.";
                    }
                } else {
                    app('db_connection', ['sqlsrv'])->rollback();

                    $code = $amd_result['Error']['Fault']['detail']['code'];
                    $err_detail = $amd_result['Error']['Fault']['detail']['description'];
                    $message = '(' . $code . ')' . ' ' . $err_detail;
                }

            } else {
                $message = "Patient Successfully updated.";
            }

            app('db_connection', ['sqlsrv'])->commit();
            // return response
            return response()->json([
                'error' => false,
                'message' => $message,
                'patient_id' => $result['patient_id'],
                'person_id' => $result['person_id']
            ]);
        }
        catch(Exception $e)
        {
            app('db_connection', ['sqlsrv'])->rollback();
            return response()->json([
                'error' => true,
                'message' => $e->getMessage(),
                'info' => [
                    'line' => $e->getLine(),
                    'file' => $e->getFile(),
                    'trace' => $e->getTraceAsString()
                ]
            ],500);
        }
    }
}
