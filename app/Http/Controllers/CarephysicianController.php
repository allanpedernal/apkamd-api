<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

/**
 * @group  Care Physician
 *
 * APIs for Care Physician
 */
class CarephysicianController extends Controller
{
    /**
     * @OA\Get(
     *     path="/api/v1/care-physician/patient/{patient_id}",
     *     tags={"Care Physician"},
     *     description="API to get Care Physician by patient id",
     *     summary="Get Care Physician By Patient ID",
     *     security={
     *       {"passport": {}},
     *     },
     *     @OA\Parameter(
     *         name="patient_id",
     *         in="path",
     *         required=true,
     *         description="Patient ID of the Patient to retrieve Care Physician.",
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean",
     *                 example="false"
     *             ),
     *             @OA\Property(
     *                 property="carephysician",
     *                 type="array",
     *                 @OA\Items(
     *                      type="object",
     *                      @OA\Property(property="id", type="string", example="B7D26C7F-9CB1-EB11-AAE9-02E7A59D591E"),
     *                      @OA\Property(property="lastname", type="string", example="Doe"),
     *                      @OA\Property(property="firstname", type="string", example="John"),
     *                      @OA\Property(property="fax", type="string", example="1232123231"),
     *                      @OA\Property(property="specialty", type="string", example="Family Care"),
     *                      @OA\Property(property="last_visit_to_doctor", type="string", example="last month"),
     *                      @OA\Property(property="other", type="string", example=" "),
     *                      @OA\Property(property="created_at", type="string", example="2021-05-25 07:33:04.290"),
     *                      @OA\Property(property="updated_at", type="string", example="2021-05-25 07:33:04.290"),
     *                 ),
     *             ),
     *         )
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": true, "message": "Error Message"}
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": true, "message": "Error Message"}
     *         )
     *     )
     * )
     */
    public function getCarephysicianByPatientID(Request $request, $patient_id)
    {
        try
        {
            // set connection
            $connection = \DB::connection('sqlsrv')->getPdo();

            // execute appointment by date mobile
            $statement = $connection->prepare("
                EXEC dbo.sp_care_physician
                    @patient_id=:patient_id
            ");
            $statement->bindParam(':patient_id',$patient_id);
            $statement->execute();
            $carephysician = $statement->fetchAll(\PDO::FETCH_ASSOC);

            // return response
            return \Response::json([
                'error'=>false,
                'carephysician'=>$carephysician
            ],200);
        }
        catch(\Exception $e)
        {
            return \Response::json([
                'error'=>true,
                'message'=>$e->getMessage()
            ],500);
        }
    }

    /**
     * @OA\Post(
     *     path="/api/v1/care-physician",
     *     tags={"Care Physician"},
     *     description="Description",
     *     summary="Store Care Physician",
     *     security={
     *       {"passport": {}},
     *     },
     *     @OA\RequestBody(
     *       required=true,
     *       description="Description",
     *       @OA\JsonContent(
     *           required={"patient_id"},
     *           @OA\Property(property="patient_id", type="uuid", example="944550AC-7EB1-EB11-AAE9-02E7A59D591E"),
     *           @OA\Property(property="lastname", type="string", example="Doe"),
     *           @OA\Property(property="firstname", type="string", example="John"),
     *           @OA\Property(property="phone", type="string", example="1232123231"),
     *           @OA\Property(property="fax", type="string", example="1232123231"),
     *           @OA\Property(property="specialty", type="string", example="Neurologist"),
     *           @OA\Property(property="last_visit_to_doctor", type="string", example="last month"),
     *           @OA\Property(property="other", type="string", example=" "),
     *       ),
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": false, "message": "Care Physician successfully added!"}
     *         )
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": true, "message": "Error Message"}
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": true, "message": "Error Message"}
     *         )
     *     )
     * )
     */
    public function store(Request $request)
    {
        try
        {
            // get error
            $error = static::validateRequest(
                \Validator::make($request->all(), [
                    'patient_id' => 'required',
                ])
            );

            // count error
            if (count($error) > 0)
            {
                return \Response::json([
                    'error' => true,
                    'message' => $error
                ], 400);
            }

            // start transaction
            \DB::connection('sqlsrv')->beginTransaction();

            # set connection
            $connection = \DB::connection('sqlsrv')->getPdo();

            // extract all
            extract($request->all());

            // execute insurance mobile
            $statement = $connection->prepare("
                EXEC dbo.spm_care_physician
                    @flag=0,
                    @patient_id=:patient_id,
                    @lastname =:lastname,
                    @firstname =:firstname,
                    @phone =:phone,
                    @fax =:fax,
                    @specialty =:specialty,
                    @last_visit_to_doctor =:last_visit_to_doctor,
                    @other =:other,
                    @id=NULL
            ");
            $statement->bindParam(':patient_id',$patient_id);
            $statement->bindParam(':lastname',$lastname);
            $statement->bindParam(':firstname',$firstname);
            $statement->bindParam(':phone',$phone);
            $statement->bindParam(':fax',$fax);
            $statement->bindParam(':specialty',$specialty);
            $statement->bindParam(':last_visit_to_doctor',$last_visit_to_doctor);
            $statement->bindParam(':other',$other);
            $statement->execute();

            // commit
            \DB::connection('sqlsrv')->commit();

            // return response
            return \Response::json([
                'error'=>false,
                'message'=>'Care Physician successfully added!'
            ],200);
        }
        catch(\Exception $e)
        {
            return \Response::json([
                'error'=>true,
                'message'=>$e->getMessage()
            ],500);
        }
    }

    /**
     * @OA\Put(
     *     path="/api/v1/care-physician/{id}",
     *     tags={"Care Physician"},
     *     description="Description",
     *     summary="Update Care Physician",
     *     security={
     *       {"passport": {}},
     *     },
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         required=true,
     *         description="ID of the Care Physician.",
     *     ),
     *     @OA\RequestBody(
     *       required=true,
     *       description="Description",
     *       @OA\JsonContent(
     *           required={"patient_id"},
     *           @OA\Property(property="patient_id", type="uuid", example="944550AC-7EB1-EB11-AAE9-02E7A59D591E"),
     *           @OA\Property(property="lastname", type="string", example="Doe"),
     *           @OA\Property(property="firstname", type="string", example="John"),
     *           @OA\Property(property="phone", type="string", example="123456789"),
     *           @OA\Property(property="fax", type="string", example="123456789"),
     *           @OA\Property(property="specialty", type="string", example="Neurologist"),
     *           @OA\Property(property="last_visit_to_doctor", type="string", example="last month"),
     *           @OA\Property(property="other", type="string", example=" "),
     *       ),
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": false, "message": "Care Physician successfully updated!"}
     *         )
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": true, "message": "Error Message"}
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": true, "message": "Error Message"}
     *         )
     *     )
     * )
     */
    public function update(Request $request, $id)
    {
        try
        {
            # set connection
            $connection = \DB::connection('sqlsrv')->getPdo();

            // extract all
            extract($request->all());

            $patient_id = (isset($patient_id)&&!empty($patient_id) ? $patient_id : '');
            $lastname  = (isset($lastname)&&!empty($lastname) ? $lastname : '');
            $firstname  = (isset($firstname)&&!empty($firstname) ? $firstname : '');
            $phone  = (isset($phone)&&!empty($phone) ? $phone : '');
            $fax  = (isset($fax)&&!empty($fax) ? $fax : '');
            $specialty  = (isset($specialty)&&!empty($specialty) ? $specialty : '');
            $last_visit_to_doctor  = (isset($last_visit_to_doctor)&&!empty($last_visit_to_doctor) ? $last_visit_to_doctor : '');
            $other  = (isset($other)&&!empty($other) ? $other : '');

            // start transaction
            \DB::connection('sqlsrv')->beginTransaction();

            // execute insurance mobile
            $statement = $connection->prepare("
                EXEC dbo.spm_care_physician
                    @flag=1,
                    @patient_id=:patient_id,
                    @lastname =:lastname,
                    @firstname =:firstname,
                    @phone =:phone,
                    @fax =:fax,
                    @specialty =:specialty,
                    @last_visit_to_doctor =:last_visit_to_doctor,
                    @other  =:other,
                    @id=:id
            ");
            $statement->bindParam(':patient_id',$patient_id);
            $statement->bindParam(':lastname',$lastname);
            $statement->bindParam(':firstname',$firstname);
            $statement->bindParam(':phone',$phone);
            $statement->bindParam(':fax',$fax);
            $statement->bindParam(':specialty',$specialty);
            $statement->bindParam(':last_visit_to_doctor',$last_visit_to_doctor);
            $statement->bindParam(':other',$other);
            $statement->bindParam(':id',$id);
            $statement->execute();

            // commit
            \DB::connection('sqlsrv')->commit();

            // return response
            return \Response::json([
                'error'=>false,
                'message'=>'Care Physician successfully updated!'
            ],200);
        }
        catch(\Exception $e)
        {
            return \Response::json([
                'error'=>true,
                'message'=>$e->getMessage()
            ],500);
        }
    }

    /**
     * @OA\Delete(
     *     path="/api/v1/care-physician/{id}",
     *     tags={"Care Physician"},
     *     description="Description",
     *     summary="Delete Care Physician",
     *     security={
     *       {"passport": {}},
     *     },
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         required=true,
     *         description="ID of the Care Physician Record.",
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": false, "message": "Care Physician successfully deleted!"}
     *         )
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": true, "message": "Error Message"}
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": true, "message": "Error Message"}
     *         )
     *     )
     * )
     */
    public function destroy($id)
    {
        try
        {
            # set connection
            $connection = \DB::connection('sqlsrv')->getPdo();

            // start transaction
            \DB::connection('sqlsrv')->beginTransaction();

            // execute insurance mobile
            $statement = $connection->prepare("
                EXEC dbo.spm_care_physician
                    @flag=2,
                    @patient_id=null,
                    @lastname ='',
                    @firstname ='',
                    @phone ='',
                    @fax ='',
                    @specialty ='',
                    @last_visit_to_doctor ='',
                    @other ='',
                    @id=:id
            ");
            $statement->bindParam(':id',$id);
            $statement->execute();

            // commit
            \DB::connection('sqlsrv')->commit();

            // return response
            return \Response::json([
                'error'=>false,
                'message'=>'Care Physician successfully deleted!'
            ],200);
        }
        catch(\Exception $e)
        {
            return \Response::json([
                'error'=>true,
                'message'=>$e->getMessage()
            ],500);
        }
    }
}
