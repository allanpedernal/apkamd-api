<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

/**
 * @group  Carrier Management
 *
 * APIs for managing insurances
 */
class CarrierController extends Controller
{
    /**
     * @OA\Get(
     *     path="/api/v1/carriers",
     *     tags={"Carrier Management"},
     *     description="Description",
     *     security={
     *       {"passport": {}},
     *     },
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean",
     *                 example="false"
     *             ),
     *             @OA\Property(
     *                 property="carriers",
     *                 type="array",
     *                 @OA\Items(
     *                      type="object",
     *                      @OA\Property(property="id", type="string", example="724379A4-9AB1-EB11-AAE9-02E7A59D591E"),
     *                      @OA\Property(property="plan_name", type="string", example="ACS BENEFIT SERVICES, INC"),
     *                 ),
     *             ),
     *         )
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": true, "message": "Error Message"}
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": true, "message": "Error Message"}
     *         )
     *     )
     * )
     */
    public function getCarriers(Request $request)
    {
        try
        {
            // set connection
            $connection = \DB::connection('sqlsrv')->getPdo();

            // extract
            extract($request->all());

            // execute carrier list
            $statement = $connection->prepare("EXEC dbo.sp_carrier_list");
            $statement->execute();
            $carriers = $statement->fetchAll(\PDO::FETCH_ASSOC);

            // return response
            return \Response::json([
                'error' => false,
                'carriers' => $carriers,
            ], 200);
        } catch (\Exception $e) {
            return \Response::json([
                'error' => true,
                'message' => $e->getMessage(),
            ], 500);
        }
    }
}
