<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DeviceController extends Controller
{
    /**
     * @OA\Get(
     *     path="/api/v1/device/registration/key/{userid}",
     *     tags={"Device Management"},
     *     description="Description",
     *     security={
     *       {"passport": {}},
     *     },
     *     @OA\Parameter(
     *         name="userid",
     *         in="path",
     *         required=true,
     *         description="User ID of the device.",
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean",
     *                 example="false"
     *             ),
     *             @OA\Property(
     *                 property="key",
     *                 type="array",
     *                 @OA\Items(
     *                      type="object",
     *                      @OA\Property(property="id", type="uuid", example="E011D77C-46D0-EB11-AAE9-02E7A59D591E"),
     *                      @OA\Property(property="userid", type="uuid", example="7020BFEF-3599-EB11-AAE9-02E7A59D591E"),
     *                      @OA\Property(property="fcm_key", type="uuid", example="QQ:EE:54:00"),
     *                      @OA\Property(property="created_at", type="datetime", example="2021-06-18 11:04:33.567"),
     *                      @OA\Property(property="updated_at", type="datetime", example="2021-06-18 11:04:33.567"),
     *                      @OA\Property(property="deleted_at", type="datetime", example="null"),
     *                 ),
     *             ),
     *         )
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": true, "message": "Error Message"}
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": true, "message": "Error Message"}
     *         )
     *     )
     * )
     */
    public function getRegistrationKey(Request $request, $userid)
    {
        try
        {
            // set connection
            $connection = \DB::connection('sqlsrv')->getPdo();

            // extract
            extract($request->all());

            // execute device registration key
            $statement = $connection->prepare("
                EXEC sp_device_registration_key
                    @userid=:userid
            ");
            $statement->bindParam(':userid',$userid);
            $statement->execute();
            $key = $statement->fetchAll(\PDO::FETCH_ASSOC);

            // return response
            return \Response::json([
                'error' => false,
                'key' => $key
            ], 200);
        }
        catch (\Exception $e)
        {
            return \Response::json([
                'error' => true,
                'message' => $e->getMessage(),
            ], 500);
        }
    }

    /**
     * @OA\Post(
     *     path="/api/v1/device/registration/key",
     *     tags={"Device Management"},
     *     description="Description",
     *     summary="Store Device Registration Key",
     *     security={
     *       {"passport": {}},
     *     },
     *     @OA\RequestBody(
     *       required=true,
     *       description="Description",
     *       @OA\JsonContent(
     *           required={"userid","fcm_key"},
     *           @OA\Property(property="userid", type="uuid", example="7020BFEF-3599-EB11-AAE9-02E7A59D591E"),
     *           @OA\Property(property="fcm_key", type="string", example="QQ:E1:54:33"),
     *       ),
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="userid",
     *                 type="uuid"
     *             ),
     *             @OA\Property(
     *                 property="fcm_key",
     *                 type="string"
     *             ),
     *             example={"error": false, "userid": "7020BFEF-3599-EB11-AAE9-02E7A59D591E", "fcm_key": "QQ:E1:54:33"}
     *         )
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": true, "message": "Error Message"}
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": true, "message": "Error Message"}
     *         )
     *     )
     * )
     */
    public function storeRegistrationKey(Request $request)
    {
        try
        {
            // get error
            $error = static::validateRequest(
                \Validator::make($request->all(), [
                    'userid' => 'required',
                    'fcm_key' => 'required',
                ])
            );

            // count error
            if (count($error) > 0)
            {
                // return response
                return \Response::json([
                    'error' => true,
                    'message' => $error
                ], 400);
            }

            // start transaction
            \DB::connection('sqlsrv')->beginTransaction();

            // set connection
            $connection = \DB::connection('sqlsrv')->getPdo();

            // extract
            extract($request->all());

            // execute device registration key
            $statement = $connection->prepare("
                EXEC spm_device_registration_key
                    @flag=0,
                    @userid=:userid,
                    @fcm_key=:fcm_key,
                    @id=NULL
            ");
            $statement->bindParam(':userid',$userid);
            $statement->bindParam(':fcm_key',$fcm_key);
            $statement->execute();

            // commit
            \DB::connection('sqlsrv')->commit();

            // return response
            return \Response::json([
                'error'=>false,
                'userid'=>$userid,
                'fcm_key'=>$fcm_key
            ],200);
        } 
        catch (\Exception $e)
        {
            return \Response::json([
                'error' => true,
                'message' => $e->getMessage(),
            ], 500);
        }
    }
}
