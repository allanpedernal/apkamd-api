<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

/**
 * @group  Doctor
 *
 * APIs for managing insurances
 */
class DoctorController extends Controller
{
    /**
     * @OA\Get(
     *     path="/api/v1/get-doctor-availability",
     *     tags={"Doctor"},
     *     description="Description",
     *     security={
     *       {"passport": {}},
     *     },
     *      @OA\Parameter(
     *         name="provider_id",
     *         in="query",
     *         required=true,
     *         description="Provider ID (ex. 7020BFEF-3599-EB11-AAE9-02E7A59D591E).",
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean",
     *                 example="false"
     *             ),
     *             @OA\Property(
     *                 property="schedules",
     *                 type="array",
     *                 @OA\Items(
     *                      type="object",
     *                      @OA\Property(property="id", type="string", example="EFB8C3DF-8EB3-EB11-AAE9-02E7A59D591E"),
     *                      @OA\Property(property="provider_id", type="string", example="7020BFEF-3599-EB11-AAE9-02E7A59D591E"),
     *                      @OA\Property(property="practice_id", type="string", example="1"),
     *                      @OA\Property(property="practice_name", type="string", example="ApkaMD"),
     *                      @OA\Property(property="seqno", type="string", example="1"),
     *                      @OA\Property(property="days", type="string", example="monday"),
     *                      @OA\Property(property="time_open", type="string", example="06:00"),
     *                      @OA\Property(property="time_close", type="string", example="07:00"),
     *                 ),
     *             ),
     *         )
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": true, "message": "Error Message"}
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": true, "message": "Error Message"}
     *         )
     *     )
     * )
     */
    public function getDoctorAvailability(Request $request)
    {
        try
        {
            // set connection
            $connection = \DB::connection('sqlsrv')->getPdo();

            // extract
            extract($request->all());

            // execute carrier list
            $statement = $connection->prepare("
                EXEC dbo.sp_provider_schedule
                    @practice_id=1,
                    @provider_id=:provider_id
            ");
            $statement->bindParam(':provider_id',$provider_id);
            $statement->execute();
            $schedules = $statement->fetchAll(\PDO::FETCH_ASSOC);

            // return response
            return \Response::json([
                'error' => false,
                'schedules' => $schedules,
            ], 200);
        } catch (\Exception $e) {
            return \Response::json([
                'error' => true,
                'message' => $e->getMessage(),
            ], 500);
        }
    }
}
