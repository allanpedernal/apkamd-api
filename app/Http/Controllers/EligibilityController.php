<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use PDO;

class EligibilityController extends Controller
{
    //
    private $connection;
    function __construct() {
        $this->connection = app('db_connection')->getPdo();
    }

    /**
     * @OA\Post(
     *     path="/api/v1/check-eligibility",
     *     tags={"Eligibility Management"},
     *     description="Check Insurance Eligibility",
     *     summary="Check Insurance Eligibility",
     *     security={
     *       {"passport": {}},
     *     },
     *     @OA\RequestBody(
     *       required=true,
     *       description="Description",
     *       @OA\JsonContent(
     *           required={"patient_id","amd_pat_id","amd_insurance_id","insurance_id"},
     *           @OA\Property(property="patient_id", type="string", format="uuid"),
     *           @OA\Property(property="amd_pat_id", type="string", example="pat1234"),
     *           @OA\Property(property="amd_insurance_id", type="string", example="ins1234"),
     *           @OA\Property(property="insurance_id", type="string", format="uuid"),
     *       ),
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean",
     *                 example="false"
     *             ),
     *             @OA\Property(
     *                 property="data",
     *                     @OA\Property(property="eligible", type="string", example="Eligible"),
     *                     @OA\Property(property="last_checked", type="string", example="07/09/2021 12:00:00")
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="data",
     *                 type="string"
     *             ),
     *             example={"error": true, "data": "Field Required!"}
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": true, "message": "Error Message"}
     *         )
     *     )
     * )
     */
    function checkEligibility(Request $request): JsonResponse
    {
        try {
            $amd_pat_id = $request->get('amd_pat_id');
            $amd_insurance_id = $request->get('amd_insurance_id');
            $patient_id = $request->get('patient_id');
            $insurance_id = $request->get('insurance_id');

            $validation_error = self::validateRequest(Validator::make($request->all(), [
                'patient_id' => [
                    'required',
                    'uuid',
                    function($attribute, $value, $fail) {
                        $sql = "SELECT * FROM [person].[Patient] WHERE ID = :id";
                        $statement = $this->connection->prepare($sql);
                        $statement->execute([':id' => $value]);
                        $result = $statement->fetch(PDO::FETCH_ASSOC);
                        $attribute = 'Entity';
                        if (!$result) {
                            $fail($attribute . ' not exist in database!.');
                        }
                    }
                ],
                'amd_pat_id' => [
                    'required',
                    function($attribute, $value, $fail) {
                        $sql = "SELECT * FROM [person].[Patient] WHERE advancedmd_patient_id = :id";
                        $statement = $this->connection->prepare($sql);
                        $statement->execute([':id' => $value]);
                        $result = $statement->fetch(PDO::FETCH_ASSOC);
                        $attribute = 'Entity';
                        if (!$result) {
                            $fail($attribute . ' not exist in database!.');
                        }
                    }
                ],
                'amd_insurance_id' => [
                    'required',
                    function($attribute, $value, $fail) {
                        $sql = "SELECT * FROM [patient].[Insurance] WHERE amd_insurance_id = :id";
                        $statement = $this->connection->prepare($sql);
                        $statement->execute([':id' => $value]);
                        $result = $statement->fetch(PDO::FETCH_ASSOC);
                        $attribute = 'Entity';
                        if (!$result) {
                            $fail($attribute . ' not exist in database!.');
                        }
                    }
                ],
                'insurance_id' => [
                    'required',
                    'uuid',
                    function($attribute, $value, $fail) {
                        $sql = "SELECT * FROM [patient].[Insurance] WHERE ID = :id";
                        $statement = $this->connection->prepare($sql);
                        $statement->execute([':id' => $value]);
                        $result = $statement->fetch(PDO::FETCH_ASSOC);
                        $attribute = 'Entity';
                        if (!$result) {
                            $fail($attribute . ' not exist in database!.');
                        }
                    }
                ],
            ]));

            if (count($validation_error) > 0)
                return response()->json([
                    'error' => true,
                    'data' => $validation_error
                ], 400);


            #saving eligibility.
            $save_eligibility = function($status, $last_checked) use (& $patient_id, & $insurance_id) {
                $sql = "dbo.spm_patient_eligibility";
                $sql .= " @p_patient_id = :p_patient_id";
                $sql .= " ,@p_insurance_id = :p_insurance_id";
                $sql .= " ,@p_eligibility = :p_eligibility";
                $sql .= " ,@p_date_verified = :p_date_verified";
                $sql .= " ,@p_userid = :p_userid";

                $statement = $this->connection->prepare($sql);
                $statement->execute([
                    ':p_patient_id' => $patient_id,
                    ':p_insurance_id' => $insurance_id,
                    ':p_eligibility' => $status,
                    ':p_date_verified' => $last_checked,
                    ':p_userid' => $patient_id
                ]);
            };


            if (env('APP_ENV') === 'local') {
                $last_checked = Carbon::now()->format('Y-m-d H:i:s');
                $status = 'Eligible';
            } else {
                $status = 'Not Eligible';

                //get eligibility ID
                $eligibility_data = [
                    'ppmdmsg' => [
                        '@action' => 'submitdemandrequest',
                        '@class' => 'atseligibility',
                        '@msgtime' => Carbon::now()->format('m/d/Y h:i:s A'),
                        '@patientid' => str_replace('pat', '', $amd_pat_id),
                        '@insurancecoverageid' => str_replace('ins', '', $amd_insurance_id),
                        '@eligibilitystc' => "30"
                    ]
                ];
                $get_eligibility_id_response = \AMD::init($eligibility_data)->process();
                $result = @$get_eligibility_id_response['PPMDResults'];

                //check eligibility
                $has_result = false;
                $eligibility_response = null;
                $check_eligibility_data = [
                    'ppmdmsg' => [
                        '@action' => 'CheckEligibilityResponse',
                        '@class' => 'eligibility',
                        '@msgtime' => Carbon::now()->format('m/d/Y h:i:s A'),
                        '@eligibilityid' => @$result['Results']['@eligibilityid']
                    ]
                ];

                while (!$has_result) {
                    $check_eligibility_response = \AMD::init($check_eligibility_data)->process();
                    $result = @$check_eligibility_response['PPMDResults'];
                    if ($result['Results']['@eligibilitystatusid'] == 1) {
                        $has_result = true;
                        $eligibility_response = $result;
                    }
                }

                $last_checked = Carbon::parse($eligibility_response['@lst'])->format('m/d/Y H:i:s');
                $name = $eligibility_response['Results']['Edi271']['TransactionSets']['TransactionSet']['SubscriberDetail']['Name'];
                if (!$name['RequestValidations']) {
                    $status = 'Eligible';
                }
            }
            $save_eligibility($status, $last_checked);
            return response()->json([
                'error' => false,
                'data' => [
                    'eligible' => $status,
                    'last_checked' => $last_checked
                ]
            ]);
        } catch (Exception $e) {
            return response()->json([
                'error' => true,
                'message' => $e->getMessage()
            ], 500);
        }
    }
}
