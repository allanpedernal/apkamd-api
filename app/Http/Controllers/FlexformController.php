<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

/**
 * @group  Health Problems & Family History
 *
 * APIs for Health Problems and Family History
 */
class FlexformController extends Controller
{
    /**
     * @OA\Get(
     *     path="/api/v1/flex-form/questions",
     *     tags={"Health Problems & Family History"},
     *     description="API to get Questions and Options for Health problems and Family History",
     *     summary="Get Questions and Options for Health problems and Family History",
     *     security={
     *       {"passport": {}},
     *     },
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean",
     *                 example="false"
     *             ),
     *             @OA\Property(
     *                 property="questions",
     *                 type="array",
     *                 @OA\Items(
     *                      type="object",
     *                      @OA\Property(property="form_id", type="string", example="B7D26C7F-9CB1-EB11-AAE9-02E7A59D591E"),
     *                      @OA\Property(property="title", type="string", example="Health Problems"),
     *                      @OA\Property(property="question_id", type="string", example="E6BB7A41-24AC-EB11-AAE9-02E7A59D591E"),
     *                      @OA\Property(property="question_text", type="string", example="Anxiety"),
     *                      @OA\Property(property="control_type", type="string", example="radio"),
     *                      @OA\Property(property="option_label", type="string", example="Yes"),
     *                      @OA\Property(property="option_value", type="string", example="0"),
     *                      @OA\Property(property="option_id", type="string", example="3F2DD9E9-DF1F-4312-A3F8-D6053BD7E7E3"),
     *                 ),
     *             ),
     *         )
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": true, "message": "Error Message"}
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": true, "message": "Error Message"}
     *         )
     *     )
     * )
     */

    public function getFlexFormQuestions(Request $request)
    {
        try
        {
            // set connection
            $connection = \DB::connection('sqlsrv')->getPdo();

            // execute appointment by date mobile
            $statement = $connection->prepare("
                EXEC dbo.sp_flexform_healthproblems_familyhistory_questions
            ");
            $statement->execute();
            $questions = collect($statement->fetchAll(\PDO::FETCH_ASSOC))->groupBy('title');

            $questions['health_problems'] = $questions['Health Problems'];
            $questions['family_history'] = $questions['Family History'];
            unset($questions['Health Problems']);
            unset($questions['Family History']);
            
            // return response
            return \Response::json([
                'error'=>false,
                'questions'=>$questions
            ],200);
        }
        catch(\Exception $e)
        {
            return \Response::json([
                'error'=>true,
                'message'=>$e->getMessage()
            ],500);
        }
    }

    /**
     * @OA\Get(
     *     path="/api/v1/flex-form/answers/{patient_id}",
     *     tags={"Health Problems & Family History"},
     *     description="API to get Answers for Health problems and Family History",
     *     summary="Get Answers for Health problems and Family History By Patient ID",
     *     security={
     *       {"passport": {}},
     *     },
     *     @OA\Parameter(
     *         name="patient_id",
     *         in="path",
     *         required=true,
     *         description="Patient ID of the Patient to retrieve Answers.",
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean",
     *                 example="false"
     *             ),
     *             @OA\Property(
     *                 property="answers",
     *                 type="array",
     *                 @OA\Items(
     *                      type="object",
     *                      @OA\Property(property="id", type="string", example="B7D26C7F-9CB1-EB11-AAE9-02E7A59D591E"),
     *                      @OA\Property(property="question_id", type="string", example="E7BB7A41-24AC-EB11-AAE9-02E7A59D591E"),
     *                      @OA\Property(property="question", type="string", example="Arrhythmias"),
     *                      @OA\Property(property="answer", type="string", example="Yes"),
     *                      @OA\Property(property="option_id", type="string", example="2C2F829C-FFDD-4FF8-BEE6-21BB63C821F5"),
     *                      @OA\Property(property="form", type="string", example="Health Problems"),
     *                      @OA\Property(property="created_at", type="string", example="2021-05-25 07:33:04.290"),
     *                      @OA\Property(property="updated_at", type="string", example="2021-05-25 07:33:04.290"),
     *                 ),
     *             ),
     *         )
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": true, "message": "Error Message"}
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": true, "message": "Error Message"}
     *         )
     *     )
     * )
     */
    public function getFlexFormAnswersByPatientID(Request $request, $patient_id)
    {
        try
        {
            // set connection
            $connection = \DB::connection('sqlsrv')->getPdo();

            // execute appointment by date mobile
            $statement = $connection->prepare("
                EXEC dbo.sp_healthproblems_familyhistory_flexform_answers
                    @patient_id=:patient_id
            ");
            $statement->bindParam(':patient_id',$patient_id);
            $statement->execute();
            $answers = $statement->fetchAll(\PDO::FETCH_ASSOC);

            // return response
            return \Response::json([
                'error'=>false,
                'answers'=>$answers
            ],200);
        }
        catch(\Exception $e)
        {
            return \Response::json([
                'error'=>true,
                'message'=>$e->getMessage()
            ],500);
        }
    }

    /**
     * @OA\Post(
     *     path="/api/v1/flex-form",
     *     tags={"Health Problems & Family History"},
     *     description="Description",
     *     summary="Store Health Problems and Family History",
     *     security={
     *       {"passport": {}},
     *     },
     *     @OA\RequestBody(
     *       required=true,
     *       description="Description",
     *       @OA\JsonContent(
     *           required={"patient_id", "value", "question_id", "form_id"},
     *           @OA\Property(property="patient_id", type="uuid", example="944550AC-7EB1-EB11-AAE9-02E7A59D591E"),
     *           @OA\Property(property="value", type="string", example="Better"),
     *           @OA\Property(property="question_id", type="string", example="CBA97328-079E-EB11-AAE9-02E7A59D591E"),
     *           @OA\Property(property="form_id", type="string", example="5FEF210A-F79D-EB11-AAE9-02E7A59D591E"),
     *           @OA\Property(property="option_id", type="string", example="D17A1030-009E-EB11-AAE9-02E7A59D591E"),
     *           @OA\Property(property="option_value", type="integer", example="5"),
     *           @OA\Property(property="practice_id", type="string", example="1"),
     *           @OA\Property(property="user_id", type="string", example=""),
     *       ),
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": false, "message": "Answers successfully added!"}
     *         )
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": true, "message": "Error Message"}
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": true, "message": "Error Message"}
     *         )
     *     )
     * )
     */
    public function store(Request $request)
    {
        try
        {
            // get error
            $error = static::validateRequest(
                \Validator::make($request->all(), [
                    'patient_id' => 'required',
                    'value' => 'required',
                    'question_id' => 'required',
                    'form_id' => 'required',
                ])
            );

            // count error
            if (count($error) > 0)
            {
                return \Response::json([
                    'error' => true,
                    'message' => $error
                ], 400);
            }

            // start transaction
            \DB::connection('sqlsrv')->beginTransaction();

            # set connection
            $connection = \DB::connection('sqlsrv')->getPdo();

            // extract all
            extract($request->all());

            // execute insurance mobile
            $statement = $connection->prepare("
                EXEC dbo.spm_flex_form_answers
                    @flag=0,
                    @patient_id=:patient_id,
                    @value =:value,
                    @question_id =:question_id,
                    @form_id =:form_id,
                    @option_id =:option_id,
                    @option_value =:option_value,
                    @practice_id =:practice_id,
                    @user_id =:user_id,
                    @id=NULL
            ");
            $statement->bindParam(':patient_id',$patient_id);
            $statement->bindParam(':value',$value);
            $statement->bindParam(':question_id',$question_id);
            $statement->bindParam(':form_id',$form_id);
            $statement->bindParam(':option_id',$option_id);
            $statement->bindParam(':option_value',$option_value);
            $statement->bindParam(':practice_id',$practice_id);
            $statement->bindParam(':user_id',$user_id);
            $statement->execute();

            // commit
            \DB::connection('sqlsrv')->commit();

            // return response
            return \Response::json([
                'error'=>false,
                'message'=>'Answers successfully added!'
            ],200);
        }
        catch(\Exception $e)
        {
            return \Response::json([
                'error'=>true,
                'message'=>$e->getMessage()
            ],500);
        }
    }

    /**
     * @OA\Put(
     *     path="/api/v1/flex-form/{id}",
     *     tags={"Health Problems & Family History"},
     *     description="Description",
     *     summary="Update Health Problems and Family History",
     *     security={
     *       {"passport": {}},
     *     },
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         required=true,
     *         description="ID of the Health Problems and Family History.",
     *     ),
     *     @OA\RequestBody(
     *       required=true,
     *       description="Description",
     *       @OA\JsonContent(
     *           required={"patient_id", "value", "question_id", "form_id"},
     *           @OA\Property(property="patient_id", type="uuid", example="944550AC-7EB1-EB11-AAE9-02E7A59D591E"),
     *           @OA\Property(property="value", type="string", example="Better"),
     *           @OA\Property(property="question_id", type="string", example="CBA97328-079E-EB11-AAE9-02E7A59D591E"),
     *           @OA\Property(property="form_id", type="string", example="5FEF210A-F79D-EB11-AAE9-02E7A59D591E"),
     *           @OA\Property(property="option_id", type="string", example="D17A1030-009E-EB11-AAE9-02E7A59D591E"),
     *           @OA\Property(property="option_value", type="integer", example="5"),
     *           @OA\Property(property="practice_id", type="string", example="1"),
     *           @OA\Property(property="user_id", type="string", example=""),
     *       ),
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": false, "message": "Answers successfully updated!"}
     *         )
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": true, "message": "Error Message"}
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": true, "message": "Error Message"}
     *         )
     *     )
     * )
     */
    public function update(Request $request, $id)
    {
        try
        {
            # set connection
            $connection = \DB::connection('sqlsrv')->getPdo();

            // extract all
            extract($request->all());

            $patient_id = (isset($patient_id)&&!empty($patient_id) ? $patient_id : '');
            $value   = (isset($value)&&!empty($value) ? $value : '');
            $question_id   = (isset($question_id)&&!empty($question_id) ? $question_id : '');
            $form_id   = (isset($form_id)&&!empty($form_id) ? $form_id : '');
            $option_id   = (isset($option_id)&&!empty($option_id) ? $option_id : '');
            $option_value   = (isset($option_value)&&!empty($option_value) ? $option_value : 0);
            $practice_id   = (isset($practice_id)&&!empty($practice_id) ? $practice_id : '');
            $user_id   = (isset($user_id)&&!empty($user_id) ? $user_id : NULL);

            // start transaction
            \DB::connection('sqlsrv')->beginTransaction();

            // execute insurance mobile
            $statement = $connection->prepare("
                EXEC dbo.spm_flex_form_answers
                    @flag=1,
                    @patient_id=:patient_id,
                    @value =:value,
                    @question_id =:question_id,
                    @form_id =:form_id,
                    @option_id =:option_id,
                    @option_value =:option_value,
                    @practice_id =:practice_id,
                    @user_id  =:user_id,
                    @id=:id
            ");
            $statement->bindParam(':patient_id',$patient_id);
            $statement->bindParam(':value',$value);
            $statement->bindParam(':question_id',$question_id);
            $statement->bindParam(':form_id',$form_id);
            $statement->bindParam(':option_id',$option_id);
            $statement->bindParam(':option_value',$option_value);
            $statement->bindParam(':practice_id',$practice_id);
            $statement->bindParam(':user_id',$user_id);
            $statement->bindParam(':id',$id);
            $statement->execute();

            // commit
            \DB::connection('sqlsrv')->commit();

            // return response
            return \Response::json([
                'error'=>false,
                'message'=>'Answers successfully updated!'
            ],200);
        }
        catch(\Exception $e)
        {
            return \Response::json([
                'error'=>true,
                'message'=>$e->getMessage()
            ],500);
        }
    }

    /**
     * @OA\Delete(
     *     path="/api/v1/flex-form/{id}",
     *     tags={"Health Problems & Family History"},
     *     description="Description",
     *     summary="Delete Answer for Health Problems and Family History",
     *     security={
     *       {"passport": {}},
     *     },
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         required=true,
     *         description="ID of the Health Problems and Family History Record.",
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": false, "message": "Answer successfully deleted!"}
     *         )
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": true, "message": "Error Message"}
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": true, "message": "Error Message"}
     *         )
     *     )
     * )
     */
    public function destroy($id)
    {
        try
        {
            # set connection
            $connection = \DB::connection('sqlsrv')->getPdo();

            // start transaction
            \DB::connection('sqlsrv')->beginTransaction();

            // execute insurance mobile
            $statement = $connection->prepare("
                EXEC dbo.spm_flex_form_answers
                    @flag=2,
                    @patient_id=null,
                    @value =null,
                    @question_id =null,
                    @form_id =null,
                    @option_id =null,
                    @option_value =null,
                    @practice_id =null,
                    @user_id =null,
                    @id=:id
            ");
            $statement->bindParam(':id',$id);
            $statement->execute();

            // commit
            \DB::connection('sqlsrv')->commit();

            // return response
            return \Response::json([
                'error'=>false,
                'message'=>'Answer successfully deleted!'
            ],200);
        }
        catch(\Exception $e)
        {
            return \Response::json([
                'error'=>true,
                'message'=>$e->getMessage()
            ],500);
        }
    }
}
