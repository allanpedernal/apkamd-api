<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use PDO;

/**
 * @group  Insurance Management
 *
 * APIs for managing insurances
 */
class InsuranceController extends Controller
{
    /**
     * @OA\Get(
     *     path="/api/v1/insurances",
     *     tags={"Insurance Management"},
     *     description="Description",
     *     summary="Get Insurances",
     *     security={
     *       {"passport": {}},
     *     },
     *     @OA\Parameter(
     *         name="offset",
     *         in="query",
     *         required=true,
     *         description="Offset of the record (ex. 0).",
     *     ),
     *     @OA\Parameter(
     *         name="limit",
     *         in="query",
     *         required=true,
     *         description="Limit of the record (ex. 10).",
     *     ),
     *     @OA\Parameter(
     *         name="search",
     *         in="query",
     *         required=false,
     *         description="optional Keyword search (ex. Jane).",
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean",
     *                 example="false"
     *             ),
     *             @OA\Property(
     *                 property="insurances",
     *                 type="array",
     *                 @OA\Items(
     *                      type="object",
     *                      @OA\Property(property="insurance_id", type="string", example="B7D26C7F-9CB1-EB11-AAE9-02E7A59D591E"),
     *                      @OA\Property(property="person_id", type="string", example="458C3D28-7EB1-EB11-AAE9-02E7A59D591E"),
     *                      @OA\Property(property="patient_id", type="string", example="468C3D28-7EB1-EB11-AAE9-02E7A59D591E"),
     *                      @OA\Property(property="patient_name", type="string", example="Porter Shawn"),
     *                      @OA\Property(property="carrier_id", type="string", example="724379A4-9AB1-EB11-AAE9-02E7A59D591E"),
     *                      @OA\Property(property="plan_name", type="string", example="ACS BENEFIT SERVICES, INC"),
     *                      @OA\Property(property="member_id", type="string", example="78985789542600"),
     *                      @OA\Property(property="total_row_count", type="integer", example="1"),
     *                      @OA\Property(property="priority", type="string", example="1"),
     *                      @OA\Property(property="eligibility", type="[string, null]", example="Eligible"),
     *                      @OA\Property(property="date_verified", type="string", format="date-time")
     *                 ),
     *             ),
     *         )
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": true, "message": "Error Message"}
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": true, "message": "Error Message"}
     *         )
     *     )
     * )
     */
    public function getInsurances(Request $request)
    {
        try
        {
            // get error
            $error = static::validateRequest(
                \Validator::make($request->all(), [
                    'offset' => 'required',
                    'limit' => 'required',
                ])
            );

            // count error
            if (count($error) > 0)
            {
                // return response
                return \Response::json([
                    'error' => true,
                    'message' => $error
                ], 400);
            }

            // set connection
            $connection = \DB::connection('sqlsrv')->getPdo();

            // extract
            extract($request->all());

            // optional
            $search = (isset($search)&&!empty($search) ? $search : '');

            // execute patient list mobile
            $statement = $connection->prepare("
                EXEC dbo.sp_insurance_list_mobile
                    @offset=:offset,
                    @limit=:limit,
                    @search=:search
            ");
            $statement->bindParam(':offset',$offset);
            $statement->bindParam(':limit',$limit);
            $statement->bindParam(':search',$search);
            $statement->execute();
            $insurances = $statement->fetchAll(PDO::FETCH_ASSOC);

            // return response
            return \Response::json([
                'error'=>false,
                'insurances'=>$insurances
            ],200);
        }
        catch(Exception $e)
        {
            return \Response::json([
                'error'=>true,
                'message'=>$e->getMessage()
            ],500);
        }
    }

    /**
     * @OA\Get(
     *     path="/api/v1/insurance/patient/{patient_id}",
     *     tags={"Insurance Management"},
     *     description="API to get insurances by patient id",
     *     summary="Get Insurances By Patient ID",
     *     security={
     *       {"passport": {}},
     *     },
     *     @OA\Parameter(
     *         name="patient_id",
     *         in="path",
     *         required=true,
     *         description="Patient ID of the insurance.",
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean",
     *                 example="false"
     *             ),
     *             @OA\Property(
     *                 property="insurances",
     *                 type="array",
     *                 @OA\Items(
     *                      type="object",
     *                      @OA\Property(property="ID", type="string", example="CB65660C-0CD0-EB11-AAE9-02E7A59D591E"),
     *                      @OA\Property(property="PayerName", type="string", example="AARP MEDICARE ADVANTAGE"),
     *
     *
     *
     *                      @OA\Property(property="PlanName", type="string", example=""),
     *                      @OA\Property(property="InsuranceIDNumber", type="string", example="543736346536"),
     *                      @OA\Property(property="Active", type="string", example="1"),
     *                      @OA\Property(property="Priority", type="string", example="1"),
     *                      @OA\Property(property="InsuranceType", type="string", example=""),
     *                      @OA\Property(property="Medicare", type="string", example=""),
     *                      @OA\Property(property="Dob", type="string", example="1980-09-09 00:00:00.000"),
     *                      @OA\Property(property="Firstname", type="integer", example="Mark"),
     *                      @OA\Property(property="Lastname", type="integer", example="Sloan"),
     *                      @OA\Property(property="amd_insurance_id", type="integer", example=""),
     *                      @OA\Property(property="eligibility", type="integer", example="Eligible"),
     *                      @OA\Property(property="date_verified", type="integer", example="2021-07-08 09:57:07.000"),
     *                      @OA\Property(property="CreatedAt", type="integer", example="2021-06-18 04:06:14.113"),
     *                 ),
     *             ),
     *         )
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": true, "message": "Error Message"}
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": true, "message": "Error Message"}
     *         )
     *     )
     * )
     */
    public function getInsurancesByPatientID(Request $request, $patient_id)
    {
        try
        {
            // set connection
            $connection = app('db_connection', ['sqlsrv'])->getPdo();

            // execute appointment by date mobile
            // $statement = $connection->prepare("
            //     EXEC dbo.sp_insurance_by_patient_id_mobile
            //         @patient_id=:patient_id
            // ");
            // $statement->bindParam(':patient_id',$patient_id);
            // $statement->execute();
            // $insurances = $statement->fetchAll(\PDO::FETCH_ASSOC);

            $statement = $connection->prepare("EXEC glutality.sp_Intake_Get_Insurance @pPatientID = :pPatientID");
            $statement->bindParam(':pPatientID', $patient_id);
            $statement->execute();
            $insurances = $statement->fetchAll(PDO::FETCH_ASSOC);

            // return response
            return response()->json([
                'error'=> false,
                'insurances'=> $insurances ?: []
            ]);
        }
        catch(Exception $e)
        {
            return response()->json([
                'error'=>true,
                'message'=>$e->getMessage()
            ],500);
        }
    }

    /**
     * @OA\Post(
     *     path="/api/v1/insurance",
     *     tags={"Insurance Management"},
     *     description="Description",
     *     summary="Store Insurance",
     *     security={
     *       {"passport": {}},
     *     },
     *     @OA\RequestBody(
     *       required=true,
     *       description="Description",
     *       @OA\JsonContent(
     *           required={"patient_id","carrier_id","carrier_name","priority","member_id","amd_patient_id","amd_resp_party"},
     *           @OA\Property(property="patient_id", type="uuid", example="944550AC-7EB1-EB11-AAE9-02E7A59D591E"),
     *           @OA\Property(property="amd_payer_code", type="string", example="ARPP"),
     *           @OA\Property(property="carrier_name", type="string", example="ACS BENEFIT SERVICES, INC"),
     *           @OA\Property(property="priority", type="string", example="1"),
     *           @OA\Property(property="member_id", type="string", example="78985789542600"),
     *           @OA\Property(property="amd_patient_id", type="string", example="pat1234"),
     *           @OA\Property(property="amd_resp_party", type="string", example="18"),
     *           @OA\Property(property="last_name", type="string", example="John"),
     *           @OA\Property(property="first_name", type="string", example="Doe"),
     *           @OA\Property(property="dob", type="string", example="07/15/2021"),
     *           @OA\Property(property="user_id", type="string", example=""),
     *       ),
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": false, "message": "Insurance is successfully added!"}
     *         )
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": true, "message": "Error Message"}
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": true, "message": "Error Message"}
     *         )
     *     )
     * )
     */
    public function store(Request $request): JsonResponse
    {
        try
        {
            # set connection
            $connection = app('db_connection', ['sqlsrv'])->getPdo();

            $patient_id = $request->get('patient_id');
            $amd_payer_code = $request->get('amd_payer_code');
            $carrier_name = $request->get('carrier_name');
            $priority = $request->get('priority');
            $member_id = $request->get('member_id');
            $amd_patient_id = $request->get('amd_patient_id');
            $amd_resp_party = $request->get('amd_resp_party');

            $lastname  = $request->get('last_name');
            $firstname = $request->get('first_name');
            $dob = $request->get('dob');
            $userid = $request->get('user_id');

            if($userid == ''){
                $userid = null;
            }

            // get error
            $error = static::validateRequest(
                Validator::make($request->all(), [
                    'patient_id' => [
                        'required',
                        'uuid',
                        function($attribute, $value, $fail) use (& $connection) {
                            $sql = "SELECT * FROM [person].[Patient] WHERE ID = :id";
                            $statement = $connection->prepare($sql);
                            $statement->execute([':id' => $value]);
                            $result = $statement->fetch(PDO::FETCH_ASSOC);
                            $attribute = 'Entity';
                            if (!$result) {
                                $fail($attribute . ' not exist in database!.');
                            }
                        }
                    ],
                    'amd_payer_code' => 'required',
                    'carrier_name' => 'required',
                    'priority' => 'required',
                    'member_id' => 'required',
                    'amd_patient_id' => [
                        'required',
                        function($attribute, $value, $fail) use (& $connection, & $request) {

                            $patient_id = $request->get('patient_id');

                            $sql = "SELECT * FROM [person].[Patient] WHERE advancedmd_patient_id = :id";
                            $statement = $connection->prepare($sql);
                            $statement->execute([':id' => $value]);
                            $result = $statement->fetch(PDO::FETCH_ASSOC);
                            $attribute = 'Entity';
                            if (!$result) {
                                $fail($attribute . ' not exist in database!.');
                            } else {
                                $sql = "SELECT * FROM [person].[Patient] WHERE advancedmd_patient_id = :id AND ID = :patient_id";
                                $statement = $connection->prepare($sql);
                                $statement->execute([':id' => $value, ':patient_id' => $patient_id]);
                                $result = $statement->fetch(PDO::FETCH_ASSOC);
                                if (!$result) {
                                    $fail($attribute . ' not matched with patient_id!.');
                                }
                            }
                        }
                    ],
                    'amd_resp_party' => [
                        'required',
                        function($attribute, $value, $fail) use (& $connection, & $request) {
                            $patient_id = $request->get('patient_id');

                            $sql = "SELECT * FROM [person].[Patient] WHERE amd_responsible_party = :id";
                            $statement = $connection->prepare($sql);
                            $statement->execute([':id' => $value]);
                            $result = $statement->fetch(PDO::FETCH_ASSOC);
                            $attribute = 'Entity';
                            if (!$result) {
                                $fail($attribute . ' not exist in database!.');
                            } else {
                                $sql = "SELECT * FROM [person].[Patient] WHERE amd_responsible_party = :id AND ID = :patient_id";
                                $statement = $connection->prepare($sql);
                                $statement->execute([':id' => $value, ':patient_id' => $patient_id]);
                                $result = $statement->fetch(PDO::FETCH_ASSOC);
                                if (!$result) {
                                    $fail($attribute . ' not matched with patient_id!.');
                                }
                            }
                        }
                    ]
                ])
            );

            // count error
            if (count($error) > 0)
            {
                return response()->json([
                    'error' => true,
                    'message' => $error
                ], 400);
            }

            // start transaction
            app('db_connection', ['sqlsrv'])->beginTransaction();


            //add insurance to amd.
            $add_amd_insurance = function() use (& $amd_payer_code, & $amd_patient_id, & $amd_resp_party, & $member_id) {

                #lookup api
                $carrier_data = [
                    'ppmdmsg' => [
                        '@action' => 'lookupcarrier',
                        '@class' => 'lookup',
                        '@msgtime' => Carbon::now()->format('m/d/Y h:i:s A'),
                        '@exactmatch' => '0',
                        '@code' => $amd_payer_code,
                        '@orderby' => 'code',
                        '@page' => '1',
                        'subtype' => [
                            "@filter-activeonly" => "true"
                        ]
                    ]
                ];
                $get_carrier_response = \AMD::init($carrier_data)->process();
                $result = @$get_carrier_response['PPMDResults'];

                $insurance_data = [
                    'ppmdmsg' => [
                        '@action' => 'addinsurance',
                        '@class' => 'demographics',
                        '@msgtime' => Carbon::now()->format('m/d/Y h:i:s A'),
                        'patient' => [
                            "@id" => $amd_patient_id,
                            "@changed" => "1",
                            'insplanlist' => [
                                'insplan' => [
                                    "@begindate" => Carbon::now()->format('m/d/Y'),
                                    "@enddate" => "",
                                    "@carrier" => @$result['Results']['carrierlist']['carrier']['@id'],
                                    "@subscriber" => $amd_resp_party,
                                    "@subscribernum" => $member_id,
                                    "@hipaarelationship" => "18",
                                    "@relationship" => "1",
                                    "@copay" => "0.00",
                                    "@copaytype" => "$",
                                    "@coverage" => "1",
                                    "@deductible" => "0.00",
                                    "@deductiblemet" => "0.00",
                                    "@yearendmonth" => "1",
                                    "@lifetime" => "0.00"
                                ]
                            ]
                        ]
                    ]
                ];

                $add_insurance_response = \AMD::init($insurance_data)->process();
                $insurance_result = @$add_insurance_response['PPMDResults'];
                $amd_insurance_id = @$insurance_result['Results']['patient']['@id'];
                $insurance_info = $insurance_result;

                return compact('amd_insurance_id', 'insurance_info');
            };

            $amd_insurance = $add_amd_insurance();

            // execute insurance mobile
            $statement = $connection->prepare("
                EXEC dbo.spm_insurance_mobile
                    @flag=0,
                    @patient_id=:patient_id,
                    @amd_payer_code=:amd_payer_code,
                    @carrier_name=:carrier_name,
                    @member_id=:member_id,
                    @priority=:priority,
                    @id=NULL,
                    @amd_insurance_id=:amd_insurance_id,
                    @lastname=:lastname,
                    @firstname=:firstname,
                    @dob=:dob,
                    @userid=:userid
            ");

            $statement->execute([
                ':patient_id' => $patient_id,
                ':amd_payer_code' => $amd_payer_code,
                ':carrier_name' => $carrier_name,
                ':priority' => $priority,
                ':member_id' => $member_id,
                ':amd_insurance_id' => $amd_insurance['amd_insurance_id'],
                ':lastname' => $lastname,
                ':firstname' => $firstname,
                ':dob' => $dob,
                ':userid' => $userid
            ]);

            // commit
            app('db_connection', ['sqlsrv'])->commit();

            // return response
            return response()->json([
                'error' => false,
                'message' => 'Insurance is successfully added!'
            ]);
        }
        catch(Exception $e)
        {
            app('db_connection', ['sqlsrv'])->rollback();
            return response()->json([
                'error'=>true,
                'message'=>$e->getMessage()
            ],500);
        }
    }

    /**
     * @OA\Get(
     *     path="/api/v1/insurance/{id}",
     *     tags={"Insurance Management"},
     *     description="Description",
     *     summary="Show Insurance",
     *     security={
     *       {"passport": {}},
     *     },
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         required=true,
     *         description="ID of the insurance.",
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean",
     *                 example="false"
     *             ),
     *             @OA\Property(
     *                 property="insurance",
     *                      @OA\Property(property="insurance_id", type="string", example="B7D26C7F-9CB1-EB11-AAE9-02E7A59D591E"),
     *                      @OA\Property(property="person_id", type="string", example="458C3D28-7EB1-EB11-AAE9-02E7A59D591E"),
     *                      @OA\Property(property="patient_id", type="string", example="468C3D28-7EB1-EB11-AAE9-02E7A59D591E"),
     *                      @OA\Property(property="patient_name", type="string", example="Porter Shawn"),
     *                      @OA\Property(property="carrier_id", type="string", example="724379A4-9AB1-EB11-AAE9-02E7A59D591E"),
     *                      @OA\Property(property="plan_name", type="string", example="ACS BENEFIT SERVICES, INC"),
     *                      @OA\Property(property="member_id", type="string", example="78985789542600"),
     *             ),
     *         )
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": true, "message": "Error Message"}
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": true, "message": "Error Message"}
     *         )
     *     )
     * )
     */
    public function show($id): JsonResponse
    {
        try
        {
            // set connection
            $connection = app('db_connection', ['sqlsrv'])->getPdo();

            $sql = "SELECT * FROM [patient].[Insurance] WHERE ID = :id";
            $statement = $connection->prepare($sql);
            $statement->execute([':id' => $id]);
            $result = $statement->fetch(PDO::FETCH_ASSOC);
            if (!$result) {
                return response()->json([
                    'error' => false,
                    'message' => 'No record found!'
                ]);
            }


            // execute insurance by id mobile
            $statement = $connection->prepare("
                EXEC dbo.sp_insurance_by_id_mobile
                    @insurance_id=:insurance_id
            ");
            $statement->bindParam(':insurance_id', $id);
            $statement->execute();
            $insurance = $statement->fetch(PDO::FETCH_ASSOC);

            // return response
            return response()->json([
                'error' => false,
                'insurance' => $insurance ?: []
            ]);
        }
        catch(Exception $e)
        {
            return response()->json([
                'error'=>true,
                'message'=>$e->getMessage()
            ],500);
        }
    }

    /**
     * @OA\Put(
     *     path="/api/v1/insurance/{id}",
     *     tags={"Insurance Management"},
     *     description="Description",
     *     summary="Update Insurance",
     *     security={
     *       {"passport": {}},
     *     },
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         required=true,
     *         description="ID of the insurance.",
     *     ),
     *     @OA\RequestBody(
     *       required=true,
     *       description="Description",
     *       @OA\JsonContent(
     *           required={"id", "patient_id","carrier_id","carrier_name","priority","member_id","amd_insurance_id"},
     *           @OA\Property(property="patient_id", format="uuid", type="string"),
     *           @OA\Property(property="amd_payer_code", type="string", example="ARPP"),
     *           @OA\Property(property="carrier_name", type="string", example="ACS BENEFIT SERVICES, INC"),
     *           @OA\Property(property="priority", type="string", example="1"),
     *           @OA\Property(property="member_id", type="string", example="78985789542600"),
     *           @OA\Property(property="amd_insurance_id", type="string", example="ins1234"),
     *           @OA\Property(property="last_name", type="string", example="John"),
     *           @OA\Property(property="first_name", type="string", example="Doe"),
     *           @OA\Property(property="dob", type="string", example="07/15/2021"),
     *           @OA\Property(property="user_id", type="string", example=""),
     *       ),
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": false, "message": "Insurance is successfully updated!"}
     *         )
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": true, "message": "Error Message"}
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": true, "message": "Error Message"}
     *         )
     *     )
     * )
     */
    public function update(Request $request, $id): JsonResponse
    {
        try
        {
            #set connection
            $connection = app('db_connection', ['sqlsrv'])->getPdo();


            $patient_id = $request->get('patient_id');
            $amd_payer_code = $request->get('amd_payer_code');
            $carrier_name = $request->get('carrier_name');
            $priority = $request->get('priority');
            $member_id = $request->get('member_id');

            $lastname  = $request->get('last_name');
            $firstname = $request->get('first_name');
            $dob = $request->get('dob');
            $userid = $request->get('user_id');

            if($userid == ''){
                $userid = null;
            }

            #$amd_patient_id = $request->get('amd_patient_id');
            #$amd_resp_party = $request->get('amd_resp_party');
            $amd_insurance_id = $request->get('amd_insurance_id');

            // get error
            $arr_to_validate = $request->all();
            $arr_to_validate['id'] = $id;

            $error = static::validateRequest(
                Validator::make($arr_to_validate, [
                    'id' => [
                        'required',
                        'uuid',
                        function($attribute, $value, $fail) use (& $connection) {
                            $sql = "SELECT * FROM [patient].[Insurance] WHERE ID = :id";
                            $statement = $connection->prepare($sql);
                            $statement->execute([':id' => $value]);
                            $result = $statement->fetch(PDO::FETCH_ASSOC);
                            $attribute = 'Entity';
                            if (!$result) {
                                $fail($attribute . ' not exist in database!.');
                            }
                        }
                    ],
                    'patient_id' => [
                        'required',
                        'uuid',
                        function($attribute, $value, $fail) use (& $connection, & $id) {
                            $sql = "SELECT * FROM [person].[Patient] WHERE ID = :id";
                            $statement = $connection->prepare($sql);
                            $statement->execute([':id' => $value]);
                            $result = $statement->fetch(PDO::FETCH_ASSOC);
                            $attribute = 'Entity';
                            if (!$result) {
                                $fail($attribute . ' not exist in database!.');
                            } else {
                                $sql = "SELECT * FROM [patient].[Insurance] WHERE PatientID = :patient_id AND ID = :id";
                                $statement = $connection->prepare($sql);
                                $statement->execute([
                                    ':patient_id' => $value,
                                    ':id' => $id
                                ]);
                                $result = $statement->fetch(PDO::FETCH_ASSOC);
                                if (!$result) {
                                    $fail($attribute . ' not matched with insurance id!.');
                                }
                            }
                        }
                    ],
                    'amd_payer_code' => 'required',
                    'carrier_name' => 'required',
                    'priority' => 'required',
                    'member_id' => 'required',
                    'amd_insurance_id' => [
                        'required',
                        function($attribute, $value, $fail) use (& $connection, & $id) {
                            $sql = "SELECT * FROM [patient].[Insurance] WHERE amd_insurance_id = :id";
                            $statement = $connection->prepare($sql);
                            $statement->execute([':id' => $value]);
                            $result = $statement->fetch(PDO::FETCH_ASSOC);
                            $attribute = 'Entity';
                            if (!$result) {
                                $fail($attribute . ' not exist in database!.');
                            } else {
                                $sql = "SELECT * FROM [patient].[Insurance] WHERE amd_insurance_id = :amd_insurance_id AND ID = :id";
                                $statement = $connection->prepare($sql);
                                $statement->execute([
                                    ':amd_insurance_id' => $value,
                                    ':id' => $id
                                ]);
                                $result = $statement->fetch(PDO::FETCH_ASSOC);
                                if (!$result) {
                                    $fail($attribute . ' not matched with insurance id!.');
                                }
                            }
                        }
                    ]
                ])
            );

            // count error
            if (count($error)) {
                return response()->json([
                    'error' => true,
                    'message' => $error
                ], 400);
            }



            // start transaction
            app('db_connection', ['sqlsrv'])->beginTransaction();

            // execute insurance mobile
            $statement = $connection->prepare("
                EXEC dbo.spm_insurance_mobile
                    @flag=1,
                    @patient_id=:patient_id,
                    @amd_payer_code=:amd_payer_code,
                    @carrier_name=:carrier_name,
                    @member_id=:member_id,
                    @priority=:priority,
                    @id=:id,
                    @amd_insurance_id=:amd_insurance_id,
                    @lastname=:lastname,
                    @firstname=:firstname,
                    @dob=:dob,
                    @userid=:userid
            ");

            $statement->execute([
                ':patient_id' => $patient_id,
                ':amd_payer_code' => $amd_payer_code,
                ':carrier_name' => $carrier_name,
                ':priority' => $priority,
                ':member_id' => $member_id,
                ':id' => $id,
                ':amd_insurance_id' => $amd_insurance_id,
                ':lastname' => $lastname,
                ':firstname' => $firstname,
                ':dob' => $dob,
                ':userid' => $userid
            ]);

            // commit
            app('db_connection', ['sqlsrv'])->commit();

            // return response
            return response()->json([
                'error'=>false,
                'message'=>'Insurance is successfully updated!'
            ]);
        }
        catch(Exception $e)
        {
            app('db_connection', ['sqlsrv'])->rollback();
            return response()->json([
                'error' => true,
                'message' => $e->getMessage()
            ],500);
        }
    }

    /**
     * @OA\Delete(
     *     path="/api/v1/insurance/{id}",
     *     tags={"Insurance Management"},
     *     description="Description",
     *     summary="Delete Insurance",
     *     security={
     *       {"passport": {}},
     *     },
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         required=true,
     *         description="ID of the insurance.",
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": false, "message": "Insurance is successfully deleted!"}
     *         )
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": true, "message": "Error Message"}
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": true, "message": "Error Message"}
     *         )
     *     )
     * )
     */
    public function destroy($id): JsonResponse
    {
        try
        {
            # set connection
            $connection = app('db_connection', ['sqlsrv'])->getPdo();

            // start transaction
            app('db_connection', ['sqlsrv'])->beginTransaction();

            // execute insurance mobile
            $statement = $connection->prepare("
                EXEC dbo.spm_insurance_mobile
                    @flag=2,
                    @patient_id=:patient_id,
                    @amd_payer_code=:amd_payer_code,
                    @carrier_name=:carrier_name,
                    @member_id=:member_id,
                    @priority=:priority,
                    @id=:id,
                    @amd_insurance_id=:amd_insurance_id
            ");

            $statement->execute([
                ':patient_id' => NULL,
                ':amd_payer_code' => NULL,
                ':carrier_name' => NULL,
                ':priority' => NULL,
                ':member_id' => NULL,
                ':id' => $id,
                ':amd_insurance_id' => NULL
            ]);

            // commit
            app('db_connection', ['sqlsrv'])->commit();

            // return response
            return response()->json([
                'error' => false,
                'message' => 'Insurance is successfully deleted!'
            ]);
        }
        catch(Exception $e)
        {
            return response()->json([
                'error' => true,
                'message' => $e->getMessage()
            ],500);
        }
    }
}
