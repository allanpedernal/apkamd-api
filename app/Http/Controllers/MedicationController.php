<?php

namespace App\Http\Controllers;

use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use PDO;

/**
 * @group  Medication
 *
 * APIs for Patient Medication
 */
class MedicationController extends Controller
{
    /**
     * @OA\Get(
     *     path="/api/v1/medication-adherence/{id}",
     *     tags={"Medication Adherence"},
     *     description="get medications and med adherence by patient id",
     *     security={
     *       {"passport": {}},
     *     },
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         required=true,
     *         description="Patient ID"
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean",
     *                 example="false"
     *             ),
     *             @OA\Property(
     *                 property="medications",
     *                 type="array",
     *                 @OA\Items(
     *                      type="object",
     *                      @OA\Property(property="medication", type="string", example="ALBUTEROL SULF HFA 90 MCG INH"),
     *                      @OA\Property(property="direction", type="string", example="No directions available"),
     *                      @OA\Property(property="therapeutic_class", type="string", example=" "),
     *                      @OA\Property(property="qty", type="string", example="1"),
     *                      @OA\Property(property="refill", type="string", example="0"),
     *                      @OA\Property(property="fill_date", type="string", example="2021-04-11 00:00:00.000"),
     *                      @OA\Property(property="pharmacy", type="string", example="CVS PHARMACY # 2236"),
     *                      @OA\Property(property="provider", type="string", example="REED  KRISTY"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="array",
     *                          @OA\Items(
     *                              type="object",
     *                              @OA\Property(property="seq_num", type="string", example="1"),
     *                              @OA\Property(property="patient_id", type="string", example="7A4479C6-1A9A-EB11-AAE9-02E7A59D591E"),
     *                              @OA\Property(property="patient_name", type="string", example="Doe, John"),
     *                              @OA\Property(property="medication", type="string", example="ALBUTEROL SULF HFA 90 MCG INH"),
     *                              @OA\Property(property="quantity", type="string", example="1"),
     *                              @OA\Property(property="days_supply", type="string", example="30"),
     *                              @OA\Property(property="fill_date", type="string", example="2021-04-11 00:00:00.000"),
     *                              @OA\Property(property="next_refill_date", type="string", example="2021-05-11 00:00:00.000"),
     *                              @OA\Property(property="actual_refill_date", type="string", example="null"),
     *                              @OA\Property(property="refill_gap", type="string", example="0"),
     *                              @OA\Property(property="remaining_days_supply", type="string", example="0"),
     *                              @OA\Property(property="instructions", type="string", example="No directions availabl"),
     *                              @OA\Property(property="provider", type="string", example="REED  KRISTY"),
     *                              @OA\Property(property="pharmacy", type="string", example="CVS PHARMACY # 2236"),
     *                              @OA\Property(property="therapeutic_class", type="string", example=" "),
     *                              @OA\Property(property="refill_gap_previous", type="string", example=" "),
     *                              @OA\Property(property="fill_actual_previous", type="string", example=" "),
     *                              @OA\Property(property="fill_end_previous", type="string", example=" "),
     *                              @OA\Property(property="remainingdaysupply_previous", type="string", example="0"),
     *                          ),
     *                      ),
     *                 ),
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Error"
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Error"
     *     ),
     * )
     */
    function getMedicationAdherence($id)
    {
        try
        {
            $error = false;

            if (!$id)
                return response()->json([
                    'error' => !$error,
                    'message' => 'patient id is required'
                ], 400);

            // set connection
            $connection = \DB::connection('sqlsrv')->getPdo();

            // execute carrier list
            $statement = $connection->prepare("
                EXEC dbo.sp_medication_adherence_per_patient_mobile @patient_id=:patient_id
            ");
            $statement->bindParam(':patient_id',$id);
            $statement->execute();
            $data = $statement->fetchAll(PDO::FETCH_ASSOC);

            $med_raw = [];
            $medications = [];

            $medication_index = [];
            $fill_end_dt_last_record = '';
            foreach($data as $d){
                $d = (array) $d;

                $index = $d['medication'];
                if(!in_array($index, $medication_index)) {
                    $medication_index[] = $index;
                    $temp = [
                        //'ndcid' => $d['ndcid'],
                        'medication' => $d['medication'],
                        'direction' =>  $d['instructions'],
                        'therapeutic_class' => '',
                        'qty' => $d['quantity'],
                        'refill' => $d['refill_gap'],
                        'fill_date' => '', //fill_end_dt last record
                        'pharmacy' => $d['pharmacy'],
                        'provider' => $d['provider'],
                        'show' => FALSE,
                        'show_adherance' => FALSE,
                        'data' => [],
                    ];
                }
                else {

                    continue;
                };


                $meds = [];
                foreach($data as $x) {
                    $x = (array) $x;
                    // if($x['remarks'] === 'DEL') continue;
                    if($x['medication'] === $index) {
                        $meds[] = $x;
                        $fill_end_dt_last_record = $x['fill_date'];
                    }
                }
                // manipulate gap data
                for($x = count($meds) -1; $x >= 0;$x--) {
                    $meds[$x]['refill_gap_previous'] = ($x==0) ? '' : $meds[$x-1]['refill_gap'];
                    $meds[$x]['fill_actual_previous'] = ($x==0) ? '' : $meds[$x-1]['actual_refill_date'];
                    $meds[$x]['fill_end_previous'] = ($x==0) ? '' : $meds[$x-1]['fill_date'];

                    if($x == 0) {
                        $meds[$x]['remainingdaysupply_previous'] = 0;
                    } else {
                        if($meds[$x-1]['days_supply']) {
                            $meds[$x]['remainingdaysupply_previous'] = abs($meds[$x-1]['remaining_days_supply']);
                        } else {
                            $meds[$x]['remainingdaysupply_previous'] = 0;
                        }
                    }
                }

                $temp['fill_date'] = $fill_end_dt_last_record;
                $temp['data'] = $meds;
                $medications[] = $temp;
            }

            // return response
            return \Response::json([
                'error' => false,
                'medications' => $medications,
            ], 200);
        } catch (Exception $e) {
            return \Response::json([
                'error' => true,
                'message' => $e->getMessage(),
            ], 500);
        }
    }

    /**
     * @OA\Get(
     *     path="/api/v1/medication/patient/{patient_id}",
     *     tags={"Medication"},
     *     description="API to get medications by patient id",
     *     summary="Get Medications By Patient ID",
     *     security={
     *       {"passport": {}},
     *     },
     *     @OA\Parameter(
     *         name="patient_id",
     *         in="path",
     *         required=true,
     *         description="Patient ID of the Patient to retrieve medications.",
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean",
     *                 example="false"
     *             ),
     *             @OA\Property(
     *                 property="medications",
     *                 type="array",
     *                 @OA\Items(
     *                      type="object",
     *                      @OA\Property(property="id", type="string", example="B7D26C7F-9CB1-EB11-AAE9-02E7A59D591E"),
     *                      @OA\Property(property="patient_id", type="string", example="468C3D28-7EB1-EB11-AAE9-02E7A59D591E"),
     *                      @OA\Property(property="medication_name", type="string", example="LOSARTAN"),
     *                      @OA\Property(property="dosage", type="string", example="500 mg"),
     *                      @OA\Property(property="directions", type="string", example="Take once a day"),
     *                      @OA\Property(property="status", type="string", example="active"),
     *                      @OA\Property(property="other_direction", type="string", example=""),
     *                      @OA\Property(property="created_at", type="string", example="2021-05-25 07:33:04.290"),
     *                      @OA\Property(property="updated_at", type="string", example="2021-05-25 07:33:04.290"),
     *                 ),
     *             ),
     *         )
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": true, "message": "Error Message"}
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": true, "message": "Error Message"}
     *         )
     *     )
     * )
     */
    public function getMedicationsByPatientID(Request $request, $patient_id)
    {
        try
        {
            // set connection
            $connection = \DB::connection('sqlsrv')->getPdo();

            // execute appointment by date mobile
            $statement = $connection->prepare("
                EXEC dbo.sp_medication_list
                    @patient_id=:patient_id
            ");
            $statement->bindParam(':patient_id',$patient_id);
            $statement->execute();
            $medications = $statement->fetchAll(\PDO::FETCH_ASSOC);

            // return response
            return \Response::json([
                'error'=>false,
                'medications'=>$medications
            ],200);
        }
        catch(\Exception $e)
        {
            return \Response::json([
                'error'=>true,
                'message'=>$e->getMessage()
            ],500);
        }
    }

    /**
     * @OA\Post(
     *     path="/api/v1/medication",
     *     tags={"Medication"},
     *     description="Description",
     *     summary="Store Medication",
     *     security={
     *       {"passport": {}},
     *     },
     *     @OA\RequestBody(
     *       required=true,
     *       description="Description",
     *       @OA\JsonContent(
     *           required={"patient_id","medication_name"},
     *           @OA\Property(property="patient_id", type="uuid", example="944550AC-7EB1-EB11-AAE9-02E7A59D591E"),
     *           @OA\Property(property="medication_name", type="string", example="LOSARTAN"),
     *           @OA\Property(property="dosage", type="string", example="500 mg"),
     *           @OA\Property(property="directions", type="string", example="Take once a day"),
     *           @OA\Property(property="status", type="string", example=" active"),
     *           @OA\Property(property="other_direction", type="string", example=" "),
     *       ),
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": false, "message": "Medication successfully added!"}
     *         )
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": true, "message": "Error Message"}
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": true, "message": "Error Message"}
     *         )
     *     )
     * )
     */
    public function store(Request $request)
    {
        try
        {
            // get error
            $error = static::validateRequest(
                \Validator::make($request->all(), [
                    'patient_id' => 'required',
                    'medication_name' => 'required',
                ])
            );

            // count error
            if (count($error) > 0)
            {
                return \Response::json([
                    'error' => true,
                    'message' => $error
                ], 400);
            }

            // start transaction
            \DB::connection('sqlsrv')->beginTransaction();

            # set connection
            $connection = \DB::connection('sqlsrv')->getPdo();

            // extract all
            extract($request->all());

            // execute insurance mobile
            $statement = $connection->prepare("
                EXEC dbo.spm_medication
                    @flag=0,
                    @patient_id=:patient_id,
                    @medication_name=:medication_name,
                    @dosage=:dosage,
                    @directions=:directions,
                    @status=:status,
                    @other_direction=:other_direction,
                    @id=NULL
            ");
            $statement->bindParam(':patient_id',$patient_id);
            $statement->bindParam(':medication_name',$medication_name);
            $statement->bindParam(':dosage',$dosage);
            $statement->bindParam(':directions',$directions);
            $statement->bindParam(':status',$status);
            $statement->bindParam(':other_direction',$other_direction);
            $statement->execute();

            // commit
            \DB::connection('sqlsrv')->commit();

            // return response
            return \Response::json([
                'error'=>false,
                'message'=>'Medication successfully added!'
            ],200);
        }
        catch(\Exception $e)
        {
            return \Response::json([
                'error'=>true,
                'message'=>$e->getMessage()
            ],500);
        }
    }

    /**
     * @OA\Put(
     *     path="/api/v1/medication/{id}",
     *     tags={"Medication"},
     *     description="Description",
     *     summary="Update Medication",
     *     security={
     *       {"passport": {}},
     *     },
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         required=true,
     *         description="ID of the Medication.",
     *     ),
     *     @OA\RequestBody(
     *       required=true,
     *       description="Description",
     *       @OA\JsonContent(
     *           required={"patient_id","medication_name"},
     *           @OA\Property(property="patient_id", type="uuid", example="944550AC-7EB1-EB11-AAE9-02E7A59D591E"),
     *           @OA\Property(property="medication_name", type="string", example="LOSARTAN"),
     *           @OA\Property(property="dosage", type="string", example="500 mg"),
     *           @OA\Property(property="directions", type="string", example="take once a day"),
     *           @OA\Property(property="status", type="string", example="active"),
     *           @OA\Property(property="other_direction", type="string", example=" "),
     *       ),
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": false, "message": "Medication successfully updated!"}
     *         )
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": true, "message": "Error Message"}
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": true, "message": "Error Message"}
     *         )
     *     )
     * )
     */
    public function update(Request $request, $id)
    {
        try
        {
            # set connection
            $connection = \DB::connection('sqlsrv')->getPdo();

            // extract all
            extract($request->all());

            $patient_id = (isset($patient_id)&&!empty($patient_id) ? $patient_id : '');
            $medication_name = (isset($medication_name)&&!empty($medication_name) ? $medication_name : '');
            $dosage = (isset($dosage)&&!empty($dosage) ? $dosage : '');
            $directions = (isset($directions)&&!empty($directions) ? $directions : '');
            $status = (isset($status)&&!empty($status) ? $status : '');
            $other_direction = (isset($other_direction)&&!empty($other_direction) ? $other_direction : '');

            // start transaction
            \DB::connection('sqlsrv')->beginTransaction();

            // execute insurance mobile
            $statement = $connection->prepare("
                EXEC dbo.spm_medication
                    @flag=1,
                    @patient_id=:patient_id,
                    @medication_name=:medication_name,
                    @dosage=:dosage,
                    @directions=:directions,
                    @status=:status,
                    @other_direction=:other_direction,
                    @id=:id
            ");
            $statement->bindParam(':patient_id',$patient_id);
            $statement->bindParam(':medication_name',$medication_name);
            $statement->bindParam(':dosage',$dosage);
            $statement->bindParam(':directions',$directions);
            $statement->bindParam(':status',$status);
            $statement->bindParam(':other_direction',$other_direction);
            $statement->bindParam(':id',$id);
            $statement->execute();

            // commit
            \DB::connection('sqlsrv')->commit();

            // return response
            return \Response::json([
                'error'=>false,
                'message'=>'Medication successfully updated!'
            ],200);
        }
        catch(\Exception $e)
        {
            return \Response::json([
                'error'=>true,
                'message'=>$e->getMessage()
            ],500);
        }
    }

    /**
     * @OA\Delete(
     *     path="/api/v1/medication/{id}",
     *     tags={"Medication"},
     *     description="Description",
     *     summary="Delete Medication",
     *     security={
     *       {"passport": {}},
     *     },
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         required=true,
     *         description="ID of the medication.",
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": false, "message": "Medication successfully deleted!"}
     *         )
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": true, "message": "Error Message"}
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": true, "message": "Error Message"}
     *         )
     *     )
     * )
     */
    public function destroy($id)
    {
        try
        {
            # set connection
            $connection = \DB::connection('sqlsrv')->getPdo();

            // start transaction
            \DB::connection('sqlsrv')->beginTransaction();

            // execute insurance mobile
            $statement = $connection->prepare("
                EXEC dbo.spm_medication
                    @flag=2,
                    @patient_id=null,
                    @medication_name='',
                    @dosage='',
                    @directions='',
                    @status='',
                    @other_direction='',
                    @id=:id
            ");
            $statement->bindParam(':id',$id);
            $statement->execute();

            // commit
            \DB::connection('sqlsrv')->commit();

            // return response
            return \Response::json([
                'error'=>false,
                'message'=>'Medication successfully deleted!'
            ],200);
        }
        catch(\Exception $e)
        {
            return \Response::json([
                'error'=>true,
                'message'=>$e->getMessage()
            ],500);
        }
    }
}
