<?php

namespace App\Http\Controllers;

use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use PDO;

/**
 * @group  Patient Management
 *
 * APIs for managing poatients
 */
class PatientController extends Controller
{
    /**
     * @OA\Get(
     *     path="/api/v1/patients",
     *     tags={"Patient Management"},
     *     description="Description",
     *     summary="Get Patients",
     *     security={
     *       {"passport": {}},
     *     },
     *     @OA\Parameter(
     *         name="offset",
     *         in="query",
     *         required=true,
     *         description="Offset of the record (ex. 0).",
     *     ),
     *     @OA\Parameter(
     *         name="limit",
     *         in="query",
     *         required=true,
     *         description="Limit of the record (ex. 10).",
     *     ),
     *     @OA\Parameter(
     *         name="search",
     *         in="query",
     *         required=false,
     *         description="optional Keyword search (ex. Jane).",
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean",
     *                 example="false"
     *             ),
     *             @OA\Property(
     *                 property="patients",
     *                 type="array",
     *                 @OA\Items(
     *                      type="object",
     *                      @OA\Property(property="person_id", type="string", example="944550AC-7EB1-EB11-AAE9-02E7A59D591E"),
     *                      @OA\Property(property="patient_id", type="string", example="954550AC-7EB1-EB11-AAE9-02E7A59D591E"),
     *                      @OA\Property(property="patient_name", type="string", example="Garcia Danny"),
     *                      @OA\Property(property="dob", type="string", example="1960-06-01 00:00:00.000"),
     *                      @OA\Property(property="gender", type="string", example="M"),
     *                      @OA\Property(property="mobile_phone", type="string", example="0357778995"),
     *                      @OA\Property(property="email", type="string", example="dg@mailinator.com"),
     *                      @OA\Property(property="patient_address", type="string", example="#25 Hallmark St. Cavite CA US 2374"),
     *                      @OA\Property(property="total_row_count", type="integer", example="1"),
     *                 ),
     *             ),
     *         )
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": true, "message": "Error Message"}
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": true, "message": "Error Message"}
     *         )
     *     )
     * )
     */
    public function getPatients(Request $request): JsonResponse
    {
        try
        {
            // get error
            $error = static::validateRequest(
                Validator::make($request->all(), [
                    'offset' => 'required',
                    'limit' => 'required',
                ])
            );

            // count error
            if (count($error) > 0)
            {
                // return response
                return response()->json([
                    'error' => true,
                    'message' => $error
                ], 400);
            }

            // set connection
            $connection = app('sql_srv_connection')->getPdo();

            // extract
            $offset = $request->get('offset');
            $limit = $request->get('limit');
            $search = $request->get('search') ?? '';

            // execute patient list mobile
            $statement = $connection->prepare("
                EXEC dbo.sp_patient_list_mobile
                    @offset = :offset,
                    @limit = :limit,
                    @search = :search
            ");
            $statement->execute([
                ':offset' => $offset,
                ':limit' => $limit,
                ':search' => $search
            ]);
            $patients = $statement->fetchAll(PDO::FETCH_ASSOC);

            // return response
            return response()->json([
                'error'=>false,
                'patients'=>$patients
            ]);
        }
        catch(Exception $e)
        {
            return response()->json([
                'error'=>true,
                'message'=>$e->getMessage()
            ],500);
        }
    }

    /**
     * @OA\Get(
     *     path="/api/v1/patient/statement/{patient_id}",
     *     tags={"Patient Management"},
     *     description="Description",
     *     summary="Get Patient Statement",
     *     security={
     *       {"passport": {}},
     *     },
     *     @OA\Parameter(
     *         name="patient_id",
     *         in="path",
     *         required=true,
     *         description="Patient ID",
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean",
     *                 example="false"
     *             ),
     *             @OA\Property(
     *                 property="statement",
     *                      @OA\Property(property="charge_id", type="uuid", example="12CEA5DC-C5CD-EB11-AAE9-02E7A59D591E"),
     *                      @OA\Property(property="encounter_id", type="uuid", example="4C14504F-2CCD-EB11-AAE9-02E7A59D591E"),
     *                      @OA\Property(property="encounter_date", type="datetime", example="2021-06-14 12:19:36.000"),
     *                      @OA\Property(property="provider_id", type="uuid", example="8DB582F0-ABBF-EB11-AAE9-02E7A59D591E"),
     *                      @OA\Property(property="provider_name", type="string", example="STEPHANIE LOUDERMILK"),
     *                      @OA\Property(property="total_charge_amount", type="double", example="650.00"),
     *                      @OA\Property(property="remaining_balance", type="double", example="150.00")
     *             ),
     *         )
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": true, "message": "Error Message"}
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": true, "message": "Error Message"}
     *         )
     *     )
     * )
     */
    public function getPatientStatement(Request $request, $patient_id)
    {
        try
        {
            // set connection
            $connection = \DB::connection('sqlsrv')->getPdo();

            // extract
            extract($request->all());

            // execute provider list mobile
            $statement = $connection->prepare("
                EXEC sp_patient_statements
                    @patient_id=:patient_id
            ");
            $statement->bindParam(':patient_id',$patient_id);
            $statement->execute();
            $statement = $statement->fetchAll(\PDO::FETCH_ASSOC);

            // return response
            return \Response::json([
                'error'=>false,
                'statement'=>$statement
            ],200);
        }
        catch(\Exception $e)
        {
            return \Response::json([
                'error'=>true,
                'message'=>$e->getMessage()
            ],500);
        }
    }

    /**
     * @OA\Get(
     *     path="/api/v1/patient/{id}",
     *     tags={"Patient Management"},
     *     description="Description",
     *     summary="Show Patient",
     *     security={
     *       {"passport": {}},
     *     },
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         required=true,
     *         description="ID of the patient. (patient_id)",
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean",
     *                 example="false"
     *             ),
     *             @OA\Property(
     *                 property="patient",
     *                      @OA\Property(property="person_id", type="string", example="944550AC-7EB1-EB11-AAE9-02E7A59D591E"),
     *                      @OA\Property(property="patient_id", type="string", example="954550AC-7EB1-EB11-AAE9-02E7A59D591E"),
     *                      @OA\Property(property="patient_name", type="string", example="Garcia Danny"),
     *                      @OA\Property(property="dob", type="string", example="1960-06-01 00:00:00.000"),
     *                      @OA\Property(property="gender", type="string", example="M"),
     *                      @OA\Property(property="mobile_phone", type="string", example="0357778995"),
     *                      @OA\Property(property="email", type="string", example="dg@mailinator.com"),
     *                      @OA\Property(property="patient_address", type="string", example="#25 Hallmark St. Cavite CA US 2374"),
     *             ),
     *         )
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": true, "message": "Error Message"}
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": true, "message": "Error Message"}
     *         )
     *     )
     * )
     */
    public function show($id)
    {
        try
        {
            // set connection
            $connection = \DB::connection('sqlsrv')->getPdo();

            // execute patient by id mobile
            $statement = $connection->prepare("
                EXEC dbo.sp_patient_by_id_mobile
                    @patient_id=:patient_id
            ");
            $statement->bindParam(':patient_id',$id);
            $statement->execute();
            $patient = $statement->fetch(PDO::FETCH_ASSOC);

            if($patient == false) {
                return \Response::json([
                    'error'=>true,
                    'message'=>'Error Message'
                ],400);
            }

            // return response
            return \Response::json([
                'error'=>false,
                'patient'=>$patient
            ],200);
        }
        catch(Exception $e)
        {
            return \Response::json([
                'error'=>true,
                'message'=>$e->getMessage()
            ],500);
        }
    }

    /**
     * @OA\Put(
     *     path="/api/v1/patient/{id}",
     *     tags={"Patient Management"},
     *     description="Description",
     *     summary="Update Patient",
     *     security={
     *       {"passport": {}},
     *     },
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         required=true,
     *         description="ID of the patient. (person_id)",
     *     ),
     *     @OA\RequestBody(
     *       required=true,
     *       description="Description",
     *       @OA\JsonContent(
     *           required={},
     *           @OA\Property(property="lastname", type="string", example="Doe"),
     *           @OA\Property(property="firstname", type="string", example="John"),
     *           @OA\Property(property="gender", type="string", example="M"),
     *           @OA\Property(property="dob", type="string", example="12/25/2020"),
     *           @OA\Property(property="street_address", type="string", example="#25 Hallmark St."),
     *           @OA\Property(property="city", type="string", example="Brooklyn"),
     *           @OA\Property(property="country", type="string", example="US"),
     *           @OA\Property(property="state", type="string", example="CA"),
     *           @OA\Property(property="zip_code", type="string", example="2374"),
     *           @OA\Property(property="mobile_number", type="string", example="0357778995"),
     *           @OA\Property(property="email", type="email", example="john.doe@gmail.com"),
     *           @OA\Property(property="user_id", type="uuid", example=""),
     *       ),
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": false, "message": "Patient is successfully updated!"}
     *         )
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": true, "message": "Error Message"}
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": true, "message": "Error Message"}
     *         )
     *     )
     * )
     */
    public function update(Request $request, $id)
    {
        try
        {
            // set connection
            $connection = \DB::connection('sqlsrv')->getPdo();

            // extract all
            extract($request->all());

            // optional
            $lastname = (isset($lastname)&&!empty($lastname) ? $lastname : '');
            $firstname = (isset($firstname)&&!empty($firstname) ? $firstname : '');
            $gender = (isset($gender)&&!empty($gender) ? $gender : '');
            $dob = (isset($dob)&&!empty($dob) ? $dob : '');
            $street_address = (isset($street_address)&&!empty($street_address) ? $street_address : '');
            $city = (isset($city)&&!empty($city) ? $city : '');
            $country = (isset($country)&&!empty($country) ? $country : '');
            $state = (isset($state)&&!empty($state) ? $state : '');
            $zip_code = (isset($zip_code)&&!empty($zip_code) ? $zip_code : '');
            $mobile_number = (isset($mobile_number)&&!empty($mobile_number) ? $mobile_number : '');
            $email = (isset($email)&&!empty($email) ? $email : '');

            if(isset($user_id) && !empty($user_id)) {
                if($user_id == '') {
                    $user_id = null;
                }
            } else {
                $user_id = null;
            }

            // start transaction
            \DB::connection('sqlsrv')->beginTransaction();

            // execute patient mobile
            $statement = $connection->prepare("
                EXEC dbo.spm_patient_mobile
                    @flag=1,
                    @lastname=:lastname,
                    @firstname=:firstname,
                    @gender=:gender,
                    @dob=:dob,
                    @street_address=:street_address,
                    @city=:city,
                    @country=:country,
                    @state=:state,
                    @zip_code=:zip_code,
                    @mobile_number=:mobile_number,
                    @email=:email,
                    @user_id=:user_id,
                    @person_id=:person_id
            ");
            $statement->bindParam(':lastname',$lastname);
            $statement->bindParam(':firstname',$firstname);
            $statement->bindParam(':gender',$gender);
            $statement->bindParam(':dob',$dob);
            $statement->bindParam(':street_address',$street_address);
            $statement->bindParam(':city',$city);
            $statement->bindParam(':country',$country);
            $statement->bindParam(':state',$state);
            $statement->bindParam(':zip_code',$zip_code);
            $statement->bindParam(':mobile_number',$mobile_number);
            $statement->bindParam(':email',$email);
            $statement->bindParam(':user_id',$user_id);
            $statement->bindParam(':person_id',$id);
            $statement->execute();

            // commit
            \DB::connection('sqlsrv')->commit();

            // return response
            return \Response::json([
                'error'=>false,
                'message'=>'Patient is successfully updated!'
            ],200);
        }
        catch(\Exception $e)
        {
            return \Response::json([
                'error'=>true,
                'message'=>$e->getMessage()
            ],500);
        }
    }
}
