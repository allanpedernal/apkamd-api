<?php

namespace App\Http\Controllers;

use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use PDO;

class PatientMedication extends Controller
{
    //
    protected $pdo_connection;

    protected $request;

    function __construct(Request $request) {
        $this->pdo_connection = app('sql_srv_connection')->getPdo();
        $this->request = $request;
    }


    function medicationByPatientID($patient_id): JsonResponse
    {
        try {
            $request = ['patient_id' => $patient_id]; //hack for route param validation via Validator
            $validation_error = self::validateRequest(Validator::make($request, [
                'patient_id' => 'required|uuid'
            ]));

            if (count($validation_error) > 0)
                return response()->json([
                    'error' => true,
                    'message' => $validation_error
                ], 400);

            $sql = "EXEC sp_medication_list @patient_id = :patient_id";
            $statement = $this->pdo_connection->prepare($sql);
            $statement->execute([':patient_id' => $patient_id]);
            $results = $statement->fetchAll(PDO::FETCH_ASSOC);

            return response()->json([
                'error' => false,
                'data' => $results
            ]);

        } catch (Exception $e) {
            return response()->json([
                'error' => true,
                'message' => $e->getMessage()
            ], 500);
        }
    }

    function store(Request $request): JsonResponse
    {
        try {
            $validation_error = self::validateRequest(Validator::make($request->all(), [
                'patient_id' => 'required|uuid',
                'medication_name' => 'required',
                'dosage' => 'required',
                'directions' => 'required'
            ]));

            if (count($validation_error) > 0)
                return response()->json([
                    'error' => true,
                    'message' => $validation_error
                ], 400);

            $patient_id = $request->get('patient_id');
            $medication_name = $request->get('medication_name');
            $dosage = $request->get('dosage');
            $direction = $request->get('direction');
            $status = $request->get('status');
            $other_direction = $request->get('other_direction');

            $sql = "EXEC spm_medication";
            $sql .= " @flag = :flag";
            $sql .= " ,@patient_id = :patient_id";
            $sql .= " ,@medication_name = :medication_name";
            $sql .= " ,@dosage = :dosage";
            $sql .= " ,@directions = :directions";
            $sql .= " ,@status = :status";
            $sql .= " ,@other_direction = :other_direction";
            $sql .= " ,@id = :id";

            $statement = $this->pdo_connection->prepare($sql);
            $statement->execute([
                ':flag' => 0,
                ':patient_id' => $patient_id,
                ':medication_name' => $medication_name,
                ':dosage' => $dosage,
                ':directions' => $direction,
                ':status' => $status,
                ':other_direction' => $other_direction,
                ':id' => NULL
            ]);

            return response()->json([
                'error' => false,
                'data' => 'Medication successfully created!'
            ]);

        } catch (Exception $e) {
            return response()->json([
                'error' => true,
                'message' => $e->getMessage()
            ], 500);
        }
    }


    function update(Request $request, $medication): JsonResponse
    {
        try {
            $params = array_merge($request->all(), ['medication' => $medication]);

            $validation_error = self::validateRequest(Validator::make($params, [
                'patient_id' => 'required|uuid',
                'medication' => 'required|uuid',
                'medication_name' => 'required',
                'dosage' => 'required',
                'directions' => 'required'
            ]));

            if (count($validation_error) > 0)
                return response()->json([
                    'error' => true,
                    'message' => $validation_error
                ], 400);

            $patient_id = $request->get('patient_id');
            $medication_name = $request->get('medication_name');
            $dosage = $request->get('dosage');
            $direction = $request->get('direction');
            $status = $request->get('status');
            $other_direction = $request->get('other_direction');

            $sql = "EXEC spm_medication";
            $sql .= " @flag = :flag";
            $sql .= " ,@patient_id = :patient_id";
            $sql .= " ,@medication_name = :medication_name";
            $sql .= " ,@dosage = :dosage";
            $sql .= " ,@directions = :directions";
            $sql .= " ,@status = :status";
            $sql .= " ,@other_direction = :other_direction";
            $sql .= " ,@id = :id";

            $statement = $this->pdo_connection->prepare($sql);
            $statement->execute([
                ':flag' => 1,
                ':patient_id' => $patient_id,
                ':medication_name' => $medication_name,
                ':dosage' => $dosage,
                ':directions' => $direction,
                ':status' => $status,
                ':other_direction' => $other_direction,
                ':id' => $medication
            ]);

            return response()->json([
                'error' => false,
                'data' => 'Medication successfully updated!'
            ]);

        } catch (Exception $e) {
            return response()->json([
                'error' => true,
                'message' => $e->getMessage()
            ], 500);
        }
    }


    function destroy($medication): JsonResponse
    {
        try {
            $request = ['medication' => $medication];
            $validation_error = self::validateRequest(Validator::make($request, [
                'medication' => 'required|uuid',
            ]));

            if (count($validation_error) > 0)
                return response()->json([
                    'error' => true,
                    'message' => $validation_error
                ], 400);

            $patient_id = NULL;
            $medication_name = NULL;
            $dosage = NULL;
            $direction = NULL;
            $status = NULL;
            $other_direction = NULL;

            $sql = "EXEC spm_medication";
            $sql .= " @flag = :flag";
            $sql .= " ,@patient_id = :patient_id";
            $sql .= " ,@medication_name = :medication_name";
            $sql .= " ,@dosage = :dosage";
            $sql .= " ,@directions = :directions";
            $sql .= " ,@status = :status";
            $sql .= " ,@other_direction = :other_direction";
            $sql .= " ,@id = :id";

            $statement = $this->pdo_connection->prepare($sql);
            $statement->execute([
                ':flag' => 2,
                ':patient_id' => $patient_id,
                ':medication_name' => $medication_name,
                ':dosage' => $dosage,
                ':directions' => $direction,
                ':status' => $status,
                ':other_direction' => $other_direction,
                ':id' => $medication
            ]);

            return response()->json([
                'error' => false,
                'data' => 'Medication successfully deleted!'
            ]);

        } catch (Exception $e) {
            return response()->json([
                'error' => true,
                'message' => $e->getMessage()
            ], 500);
        }
    }
}
