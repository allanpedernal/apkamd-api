<?php

namespace App\Http\Controllers;

use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use PDO;

class PayerController extends Controller
{
    //
    /**
     * @OA\Get(
     *     path="/api/v1/payer-list",
     *     tags={"Payer Management"},
     *     description="Amd Payer List",
     *     summary="Get Payers",
     *     security={
     *       {"passport": {}},
     *     },
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean",
     *                 example="false"
     *             ),
     *             @OA\Property(
     *                 property="payer",
     *                 type="array",
     *                 @OA\Items(
     *                      type="object",
     *                      @OA\Property(property="id", type="string", format="uuid"),
     *                      @OA\Property(property="code", type="string", example="AAR01"),
     *                      @OA\Property(property="carrier_name", type="string", example="AARP"),
     *                      @OA\Property(property="address", type="string", example="PO BOX 740819"),
     *                      @OA\Property(property="cpid", type="string", example="5271"),
     *                      @OA\Property(property="apt_suite", type="[string, null]", example=""),
     *                      @OA\Property(property="pmt_source", type="string", example="Commercial Insurance"),
     *                      @OA\Property(property="ins_type", type="integer", example="Commercial"),
     *                      @OA\Property(property="city", type="string", example="ATLANTA"),
     *                      @OA\Property(property="state", type="[string, null]", example="GA"),
     *                      @OA\Property(property="zip", type="string", example="30374")
     *                 ),
     *             ),
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": true, "message": "Error Message"}
     *         )
     *     )
     * )
     */
    public function list(): JsonResponse
    {
        try {
            $pdo_connection = app('db_connection', ['sqlsrv'])->getPdo();
            $statement = $pdo_connection->prepare("EXEC glutality.sp_AMD_Payer_List");
            $statement->execute();
            $result = $statement->fetchAll(PDO::FETCH_ASSOC);
            return response()->json([
                'error' => false,
                'data' => $result ?: []
            ]);
        } catch (Exception $e) {
            return response()->json([
                'error' => true,
                'data' => [
                    'message' => $e->getMessage(),
                    'line' => $e->getLine(),
                    'file' => $e->getFile()
                ]
            ], 500);
        }
    }
}
