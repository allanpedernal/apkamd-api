<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Str;
use Illuminate\Support\Arr;

use Stripe\Stripe;
use Stripe\StripeClient;
use Stripe\Customer;
use Stripe\SetupIntent;
use Stripe\Event;
use Stripe\Checkout\Session;

/**
 * @group  Payment
 *
 * APIs for managing payments
 */
class PaymentController extends Controller
{
    /**
     * @OA\Get(
     *     path="/api/v1/payment/get-stripe-customer-id/{patient_id}",
     *     tags={"Payment"},
     *     description="Description",
     *     summary="Get Stripe Customer ID",
     *     security={
     *       {"passport": {}},
     *     },
     *     @OA\Parameter(
     *         name="patient_id",
     *         in="path",
     *         required=true,
     *         description="Patient ID.",
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean",
     *                 example="false"
     *             ),
     *             @OA\Property(
     *                 property="patient",
     *                 type="array",
     *                 @OA\Items(
     *                      type="object",
     *                      @OA\Property(property="stripe_customer_id", type="string", example="468C3D28-7EB1-EB11-AAE9-02E7A59D591E"),
     *                 ),
     *             ),
     *         )
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": true, "message": "Error Message"}
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": true, "message": "Error Message"}
     *         )
     *     )
     * )
     */
    public function getStripeCustomerID(Request $request, $patient_id)
    {
        try
        {
            // set connection
            $connection = \DB::connection('sqlsrv')->getPdo();

            // execute appointment by date mobile
            $statement = $connection->prepare("
                EXEC dbo.sp_patient_stripe_customer
                    @patient_id=:patient_id
            ");
            $statement->bindParam(':patient_id',$patient_id);
            $statement->execute();
            $patient = $statement->fetch(\PDO::FETCH_ASSOC);

            // return response
            return \Response::json([
                'error'=>false,
                'patient'=>$patient
            ],200);
        }
        catch(\Exception $e)
        {
            return \Response::json([
                'error'=>true,
                'message'=>$e->getMessage()
            ],500);
        }
    }

    /**
     * @OA\Post(
     *     path="/api/v1/payment/save-stripe-customer-id",
     *     tags={"Payment"},
     *     description="Description",
     *     summary="Save Stripe Customer",
     *     security={
     *       {"passport": {}},
     *     },
     *     @OA\RequestBody(
     *       required=true,
     *       description="Description",
     *       @OA\JsonContent(
     *           required={"patient_id"},
     *           @OA\Property(property="patient_id", type="uuid", example="944550AC-7EB1-EB11-AAE9-02E7A59D591E"),
     *       ),
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": false, "message": "Stripe Customer is successfully saved!"}
     *         )
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": true, "message": "Error Message"}
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": true, "message": "Error Message"}
     *         )
     *     )
     * )
     */
    public function saveStripeCustomer(Request $request)
    {
        try
        {
            Stripe::setApiKey(config('stripe.secret_key'));

            // get error
            $error = static::validateRequest(
                \Validator::make($request->all(), [
                    'patient_id' => 'required',
                ])
            );

            // count error
            if (count($error) > 0)
            {
                return \Response::json([
                    'error' => true,
                    'message' => $error
                ], 400);
            }

            // start transaction
            \DB::connection('sqlsrv')->beginTransaction();

            # set connection
            $connection = \DB::connection('sqlsrv')->getPdo();

            // extract all
            extract($request->all());

            //check first if customer id already exists
            $statement = $connection->prepare(
                "sp_patient_stripe_customer
                    @patient_id = :patient_id"
            );
    
            $statement->bindParam(':patient_id', $patient_id);
            $statement->execute();
            $patient = $statement->fetch(\PDO::FETCH_ASSOC);
            $customer_id = $patient['stripe_customer_id'];

            /**
             * Create Stripe Customer if patient does not have a stripe customer stored in database
             */
            if (! $customer_id) {
                //get patient details
                $practice_id = '1';
                $statement = $connection->prepare(
                    "glutality.sp_PatientDetail
                        @PatientID = :patient_id,
                        @PracticeID = :practice_id"
                );
        
                $statement->bindParam(':patient_id', $patient_id);
                $statement->bindParam(':practice_id', $practice_id);
                $statement->execute();
                $patient_details = $statement->fetch(\PDO::FETCH_ASSOC);
                $p_person_id = $patient_details['PersonID'];
                $p_firstname = $patient_details['Firstname'];
                $p_lastname = $patient_details['Lastname'];
                $p_mobile = $patient_details['mobile_phone'];
                $p_email = $patient_details['Email'];

                //create customer ID from stripe
                $customer = Customer::create([
                    'name' => $p_firstname . ' ' . $p_lastname,
                    'phone' => $p_mobile,
                    'email' => $p_email,
                    'metadata' => [
                        'Patient ID' => $patient_id,
                        'Person ID' => $p_person_id,
                    ],
                ]);

                $customer_id = $customer->id;

                // execute insurance mobile
                $statement = $connection->prepare("
                    EXEC dbo.spm_patient_stripe_customer
                        @patient_id=:patient_id,
                        @stripe_customer_id=:stripe_customer_id
                ");
                $statement->bindParam(':patient_id',$patient_id);
                $statement->bindParam(':stripe_customer_id',$customer_id);
                $statement->execute();

                // commit
                \DB::connection('sqlsrv')->commit();
            }

            // return response
            return \Response::json([
                'error'=>false,
                'message'=>'Stripe Customer is successfully saved!'
            ],200);
        }
        catch(\Exception $e)
        {
            return \Response::json([
                'error'=>true,
                'message'=>$e->getMessage()
            ],500);
        }
    }

    /**
     * @OA\Post(
     *     path="/api/v1/payment/save-card-details",
     *     tags={"Payment"},
     *     description="Description",
     *     summary="Save Card Details",
     *     security={
     *       {"passport": {}},
     *     },
     *     @OA\RequestBody(
     *       required=true,
     *       description="Description",
     *       @OA\JsonContent(
     *           required={"patient_id","customer_id","payment_method_id"},
     *           @OA\Property(property="patient_id", type="uuid", example="944550AC-7EB1-EB11-AAE9-02E7A59D591E"),
     *           @OA\Property(property="customer_id", type="uuid", example="724379A4-9AB1-EB11-AAE9-02E7A59D591E"),
     *           @OA\Property(property="payment_method_id", type="uuid", example="724379A4-9AB1-EB11-AAE9-02E7A59D591E"),
     *       ),
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": false, "message": "Card Details is successfully saved!"}
     *         )
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": true, "message": "Error Message"}
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": true, "message": "Error Message"}
     *         )
     *     )
     * )
     */
    public function saveCardDetails(Request $request)
    {
        try
        {
            // get error
            $error = static::validateRequest(
                \Validator::make($request->all(), [
                    'patient_id' => 'required',
                    'customer_id' => 'required',
                    'payment_method_id' => 'required',
                ])
            );

            // count error
            if (count($error) > 0)
            {
                return \Response::json([
                    'error' => true,
                    'message' => $error
                ], 400);
            }

            // start transaction
            \DB::connection('sqlsrv')->beginTransaction();

            # set connection
            $connection = \DB::connection('sqlsrv')->getPdo();

            // extract all
            extract($request->all());

            // execute insurance mobile
            $statement = $connection->prepare("
                EXEC dbo.spm_stripe_payment_method
                    @flag=0,
                    @patient_id=:patient_id,
                    @customer_id=:customer_id,
                    @payment_method_id=:payment_method_id,
                    @id=NULL
            ");
            $statement->bindParam(':patient_id',$patient_id);
            $statement->bindParam(':customer_id',$customer_id);
            $statement->bindParam(':payment_method_id',$payment_method_id);
            $statement->execute();

            // commit
            \DB::connection('sqlsrv')->commit();

            // return response
            return \Response::json([
                'error'=>false,
                'message'=>'Card Details is successfully saved!'
            ],200);
        }
        catch(\Exception $e)
        {
            return \Response::json([
                'error'=>true,
                'message'=>$e->getMessage()
            ],500);
        }
    }

    /**
     * @OA\Post(
     *     path="/api/v1/payment/store-payment-invoice-details",
     *     tags={"Payment"},
     *     description="Description",
     *     summary="Store Payment/Invoice Details",
     *     security={
     *       {"passport": {}},
     *     },
     *     @OA\RequestBody(
     *       required=true,
     *       description="Description",
     *       @OA\JsonContent(
     *           required={"patient_id"},
     *           @OA\Property(property="patient_id", type="uuid", example="944550AC-7EB1-EB11-AAE9-02E7A59D591E"),
     *           @OA\Property(property="appointment_id", type="uuid", example="724379A4-9AB1-EB11-AAE9-02E7A59D591E"),
     *           @OA\Property(property="payment_method", type="string", example="stripe"),
     *           @OA\Property(property="status", type="string", example="pending"),
     *           @OA\Property(property="amount", type="string", example="95.00"),
     *       ),
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": false, "message": "Appointment transaction is successfully saved!"}
     *         )
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": true, "message": "Error Message"}
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": true, "message": "Error Message"}
     *         )
     *     )
     * )
     */
    public function storePaymentInvoiceDetails(Request $request)
    {
        try
        {
            Stripe::setApiKey(config('stripe.secret_key'));

            // get error
            $error = static::validateRequest(
                \Validator::make($request->all(), [
                    'patient_id' => 'required',
                ])
            );

            // count error
            if (count($error) > 0)
            {
                return \Response::json([
                    'error' => true,
                    'message' => $error
                ], 400);
            }

            // start transaction
            \DB::connection('sqlsrv')->beginTransaction();

            # set connection
            $connection = \DB::connection('sqlsrv')->getPdo();

            // extract all
            extract($request->all());


            //get customer ID
            $statement = $connection->prepare(
                "EXEC sp_patient_stripe_customer
                    @patient_id = :patient_id"
            );
    
            $statement->bindParam(':patient_id', $patient_id);
            $statement->execute();
            $patient = $statement->fetch(\PDO::FETCH_ASSOC);
            $customer_id = $patient['stripe_customer_id'];
    
            if ($customer_id) {
                //create invoice
                $stripe = new StripeClient(config('stripe.secret_key'));

                $invoice_item = $stripe->invoiceItems->create([
                    'customer' => $customer_id,
                    'amount' => ($amount * 100),//9500,
                    'currency' => 'USD',
                    'description' => 'Appointment Fee'
                ]);

                $invoice = $stripe->invoices->create([
                    'customer' => $customer_id,
                    'collection_method' => 'charge_automatically',
                    'description' => 'Appointment Fee'
                ]);

                $reference_code = $invoice->id;

                /**
                 * Fetch Invoice
                 */
                $statement = $connection->prepare(
                    "EXEC sp_billing_payment_registration_by_appointment
                        @appointment_id  = :appointment_id"
                );
                $statement->bindParam(':appointment_id', $appointment_id);
                $statement->execute();
                $billing = $statement->fetch(\PDO::FETCH_ASSOC);

                $flag = 0;
                $id = NULL;
                if ($billing && isset($billing['id'])) {
                    $flag = 1;
                    $id = $billing['id'];
                    $status = $billing['status'];
                }

                // execute insurance mobile
                $statement = $connection->prepare("
                    EXEC dbo.spm_billing_payment_registration
                        @flag=:flag,
                        @patient_id=:patient_id,
                        @appointment_id=:appointment_id,
                        @payment_method=:payment_method,
                        @reference_code=:reference_code,
                        @status=:status,
                        @amount=:amount,
                        @id = :id
                ");
                
                $statement->bindParam(':flag',$flag);
                $statement->bindParam(':patient_id',$patient_id);
                $statement->bindParam(':appointment_id',$appointment_id);
                $statement->bindParam(':payment_method',$payment_method);
                $statement->bindParam(':reference_code',$reference_code);
                $statement->bindParam(':status',$status);
                $statement->bindParam(':amount',$amount);
                $statement->bindParam(':id',$id);
                $statement->execute();

                // commit
                \DB::connection('sqlsrv')->commit();

                // return response
                return \Response::json([
                    'error'=>false,
                    'message'=>'Appointment transaction is successfully saved!'
                ],200);
            }else{
                return \Response::json([
                    'error'=>true,
                    'message'=>'Patient does not have a customer ID. Please create a customer ID by using the API Save Stripe Customer'
                ],500);
            }
        }
        catch(\Exception $e)
        {
            return \Response::json([
                'error'=>true,
                'message'=>$e->getMessage()
            ],500);
        }
    }


}
