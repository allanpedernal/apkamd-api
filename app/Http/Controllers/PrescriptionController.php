<?php

namespace App\Http\Controllers;

use App\Services\Facades\Telnyx;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Validator;
use PDF;
use Illuminate\Http\Request;
use PDO;

class PrescriptionController extends Controller
{
    //
    protected $pdo_connection;

    function __construct() {
        $this->pdo_connection = app('sql_srv_connection')->getPdo();
    }

    /**
     * @OA\Post(
     *     path="/api/v1/refill",
     *     tags={"Prescription Management"},
     *     description="Description",
     *     summary="Request Refill",
     *     security={
     *       {"passport": {}},
     *     },
     *     @OA\RequestBody(
     *       required=true,
     *       description="Description",
     *       @OA\JsonContent(
     *           required={"patient_id","provider_id","medication_id"},
     *           @OA\Property(property="patient_id", type="string", format="uuid"),
     *           @OA\Property(property="provider_id", type="string", format="uuid"),
     *           @OA\Property(property="medication_id", type="string", format="uuid"),
     *       ),
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": false, "message": "Request refill sent!"}
     *         )
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": true, "message": "patient id is required."}
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": true, "message": "Error Message"}
     *         )
     *     )
     * )
     */
    function refill(Request $request): JsonResponse
    {
        try {
            $data = [];
            $patient_id = $request->get('patient_id');
            $provider_id = $request->get('provider_id');
            $medication_id = $request->get('medication_id');
            $comment = $request->get('comment');

            $validation_error = self::validateRequest(Validator::make($request->all(), [
                'patient_id' => 'required|uuid',
                'provider_id' => 'required|uuid',
                'medication_id' => 'required|uuid'
            ]));

            if (count($validation_error) > 0)
                return response()->json([
                    'error' => true,
                    'message' => $validation_error
                ], 400);

            $medication_info = function() use (& $medication_id) {
                $sql = "EXEC dbo.sp_get_medication_info_mobile @id = :id";
                $statement = $this->pdo_connection->prepare($sql);
                $statement->execute([':id' => $medication_id]);
                return $statement->fetch(PDO::FETCH_ASSOC);
            };

            $patient_info = function() use (& $patient_id) {
                $sql = "EXEC dbo.sp_patient_by_id_mobile @patient_id = :patient_id";
                $statement = $this->pdo_connection->prepare($sql);
                $statement->execute([':patient_id' => $patient_id]);
                return $statement->fetch(PDO::FETCH_ASSOC);
            };

            $provider_info = function() use (& $provider_id) {
                $sql = "EXEC dbo.sp_provider_mobile @person_id = :person_id";
                $statement = $this->pdo_connection->prepare($sql);
                $statement->execute([':person_id' => $provider_id]);
                return $statement->fetch(PDO::FETCH_ASSOC);
            };

            $fax_log = function($param) use (& $patient_id, $medication_id) {
                $data = @$param['data'];

                $sql = "EXEC dbo.spm_prescription_fax_log_mobile";
                $sql .= " @flag = :flag";
                $sql .= " ,@patient_id = :patient_id";
                $sql .= " ,@fax_id = :fax_id";
                $sql .= " ,@connection_id = :connection_id";
                $sql .= " ,@medication_id = :medication_id";
                $sql .= " ,@to_number = :to_number";
                $sql .= " ,@media_url = :media_url";
                $sql .= " ,@status = :status";
                $sql .= " ,@id = :id";
                $statement = $this->pdo_connection->prepare($sql);
                $statement->execute([
                    ':flag' => 0,
                    ':patient_id' => $patient_id,
                    ':fax_id' => $data['id'],
                    ':connection_id' => $data['connection_id'],
                    ':medication_id' => $medication_id,
                    ':to_number' => $data['to'],
                    ':media_url' => $data['media_url'],
                    ':status' => $data['status'],
                    ':id' => NULL,
                ]);
            };

            #get default pharmacy data.
            $data['pharmacy_data']['name'] = config('services.default-pharmacy.name');
            $data['pharmacy_data']['address'] = config('services.default-pharmacy.address');
            $data['pharmacy_data']['fax'] = config('services.default-pharmacy.fax');
            $data['pharmacy_data']['phone'] = config('services.default-pharmacy.phone');

            #patient info
            $patient = $patient_info();
            $data['patient']['name'] = $patient ? $patient['patient_name'] ?? 'N/A' : 'N/A';
            $data['patient']['full_address'] = $patient ? $patient['patient_address'] ?? 'N/A' : 'N/A';
            $data['patient']['contact'] = $patient ? $patient['mobile_phone'] ?? 'N/A' : 'N/A';
            $data['patient']['dob'] = $patient ? ($patient['dob'] ? Carbon::parse( $patient['dob'] )->format('m/d/Y') : 'N/A' ) : 'N/A';
            $data['patient']['email'] = $patient ? $patient['email'] ?? 'N/A' : 'N/A';
            $data['patient']['gender'] = $patient ? $patient['gender'] ?? 'N/A' : 'N/A';

            #provider info
            $provider = $provider_info();
            $data['provider']['name'] = $provider ? ( ($provider['lastname'] AND $provider['lastname'])  ? $provider['firstname'] . ' ' . $provider['lastname'] : 'N/A' ) : 'N/A';
            $data['provider']['address'] = $provider ? $provider['address'] ?? 'N/A' : 'N/A';
            $data['provider']['email'] = $provider ? $provider['email'] ?? 'N/A' : 'N/A';
            $data['provider']['npi'] = $provider ? $provider['npi'] ?? 'N/A' : 'N/A';
            $data['provider']['phone'] = $provider ? $provider['phone'] ?? 'N/A' : 'N/A';


            #medication
            $medication = $medication_info();
            $data['medication']['name'] = $medication ? $medication['medication_name'] ?? 'N/A' : 'N/A';
            $data['medication']['unit_quantity'] = $medication ? $medication['quantity'] ?? 'N/A' : 'N/A';
            $data['medication']['unit_dosage'] = $medication ? $medication['unit'] ?? 'N/A' : 'N/A';
            $data['medication']['instruction'] = $medication ? $medication['instructions'] ?? 'N/A' : 'N/A';

            $data['other']['comment'] = $comment ?: 'No Data Provided';

            $unique_identifier = time();

            $file_path = public_path('/') . 'refill-form-' . $unique_identifier . '.' . 'pdf';

            $file_public_url = url('/') . '/refill-form-' . $unique_identifier . '.' . 'pdf';


            PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])
                ->loadView('refill-form', $data)->save($file_path);

            $response = Telnyx::send($file_public_url);

            if (isset($response['data'])) {
                #save
                $fax_log($response);
            }

            return response()->json([
                'error' => false,
                'message' => 'Request refill sent'
            ]);

        } catch (Exception $e) {
            return response()->json([
                'error' => true,
                'message' => $e->getMessage()
            ], 500);
        }
    }
}
