<?php

namespace App\Http\Controllers;

use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class ProviderController extends Controller
{
    /**
     * @OA\Get(
     *     path="/api/v1/providers",
     *     tags={"Provider Management"},
     *     description="Description",
     *     summary="Get Providers",
     *     security={
     *       {"passport": {}},
     *     },
     *     @OA\Parameter(
     *         name="offset",
     *         in="query",
     *         required=true,
     *         description="Offset of the record (ex. 0).",
     *     ),
     *     @OA\Parameter(
     *         name="limit",
     *         in="query",
     *         required=true,
     *         description="Limit of the record (ex. 10).",
     *     ),
     *     @OA\Parameter(
     *         name="search",
     *         in="query",
     *         required=false,
     *         description="optional Keyword search (ex. Jane).",
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean",
     *                 example="false"
     *             ),
     *             @OA\Property(
     *                 property="providers",
     *                 type="array",
     *                 @OA\Items(
     *                      type="object",
     *                      @OA\Property(property="provider_id", type="uuid", example="ADFA83AE-71A3-EB11-AAE9-02E7A59D591E"),
     *                      @OA\Property(property="provider_name", type="string", example="SHANNON ACCARDO"),
     *                      @OA\Property(property="lastname", type="string", example="ACCARDO"),
     *                      @OA\Property(property="firstname", type="string", example="SHANNON"),
     *                      @OA\Property(property="state", type="string", example="CA"),
     *                      @OA\Property(property="npi", type="string", example="1700235454"),
     *                      @OA\Property(property="home_phone", type="string", example="1234567890"),
     *                      @OA\Property(property="mobile_phone", type="string", example="1234567890"),
     *                      @OA\Property(property="work_phone", type="string", example="1234567890"),
     *                      @OA\Property(property="cnt", type="integer", example="2"),
     *                 ),
     *             ),
     *         )
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": true, "message": "Error Message"}
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": true, "message": "Error Message"}
     *         )
     *     )
     * )
     */
    public function getProviders(Request $request)
    {
        try
        {
            // get error
            $error = static::validateRequest(
                \Validator::make($request->all(), [
                    'offset' => 'required',
                    'limit' => 'required',
                ])
            );

            // count error
            if (count($error) > 0)
            {
                // return response
                return \Response::json([
                    'error' => true,
                    'message' => $error
                ], 400);
            }

            // set connection
            $connection = \DB::connection('sqlsrv')->getPdo();

            // extract
            extract($request->all());

            // optional
            $search = (isset($search)&&!empty($search) ? $search : '');

            // execute provider list mobile
            $statement = $connection->prepare("
                EXEC dbo.sp_provider_list_mobile
                    @offset=:offset,
                    @limit=:limit,
                    @search=:search
            ");
            $statement->bindParam(':offset',$offset);
            $statement->bindParam(':limit',$limit);
            $statement->bindParam(':search',$search);
            $statement->execute();
            $providers = $statement->fetchAll(\PDO::FETCH_ASSOC);

            // return response
            return \Response::json([
                'error'=>false,
                'providers'=>$providers
            ],200);
        }
        catch(\Exception $e)
        {
            return \Response::json([
                'error'=>true,
                'message'=>$e->getMessage()
            ],500);
        }
    }

    /**
     * @OA\Get(
     *     path="/api/v1/provider/city/{city}",
     *     tags={"Provider Management"},
     *     description="Description",
     *     summary="Get Providers By City",
     *     security={
     *       {"passport": {}},
     *     },
     *     @OA\Parameter(
     *         name="city",
     *         in="path",
     *         required=true,
     *         description="City of the provider (ex. Los Gatos).",
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean",
     *                 example="false"
     *             ),
     *             @OA\Property(
     *                 property="providers",
     *                 type="array",
     *                 @OA\Items(
     *                      type="object",
     *                      @OA\Property(property="id", type="uuid", example="7320BFEF-3599-EB11-AAE9-02E7A59D591E"),
     *                      @OA\Property(property="person_id", type="string", example="7020BFEF-3599-EB11-AAE9-02E7A59D591E"),
     *                      @OA\Property(property="lastname", type="string", example="ALEXANDER"),
     *                      @OA\Property(property="firstname", type="string", example="SANDRA"),
     *                      @OA\Property(property="practice_id", type="integer", example="1"),
     *                      @OA\Property(property="specialty_list_id", type="uuid", example="449F07BC-0DBA-EB11-AAE9-02E7A59D591E"),
     *                      @OA\Property(property="specialty_name", type="string", example="Endocrinology"),
     *                      @OA\Property(property="npi", type="string", example="1023181401"),
     *                      @OA\Property(property="bio", type="string", example="null"),
     *                      @OA\Property(property="address", type="string", example="405 ALBERTO WAY"),
     *                      @OA\Property(property="city", type="string", example="LOS GATOS"),
     *                      @OA\Property(property="state", type="string", example="CA"),
     *                      @OA\Property(property="zip", type="integer", example="95032"),
     *                      @OA\Property(property="country", type="string", example="null"),
     *                      @OA\Property(property="phone", type="phone", example="408-888-0009"),
     *                      @OA\Property(property="email", type="string", example="jane.dow@example.com"),
     *                      @OA\Property(property="photo_url", type="string", example="null"),
     *                      @OA\Property(property="rating", type="integer", example="2"),
     *                      @OA\Property(property="number_of_visit", type="integer", example="1"),
     *                 ),
     *             ),
     *         )
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": true, "message": "Error Message"}
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": true, "message": "Error Message"}
     *         )
     *     )
     * )
     */
    public function getProviderByCity(Request $request, $city)
    {
        try
        {
            // set connection
            $connection = \DB::connection('sqlsrv')->getPdo();

            // extract
            extract($request->all());

            // optional
            $search = (isset($search)&&!empty($search) ? $search : '');

            // execute provider list mobile
            $statement = $connection->prepare("
                EXEC dbo.sp_provider_by_city_mobile
                    @city=:city
            ");
            $statement->bindParam(':city',$city);
            $statement->execute();
            $providers = $statement->fetchAll(\PDO::FETCH_ASSOC);

            // return response
            return \Response::json([
                'error'=>false,
                'providers'=>$providers
            ],200);
        }
        catch(\Exception $e)
        {
            return \Response::json([
                'error'=>true,
                'message'=>$e->getMessage()
            ],500);
        }
    }

    /**
     * @OA\Get(
     *     path="/api/v1/provider/specialty/{specialty}",
     *     tags={"Provider Management"},
     *     description="Description",
     *     summary="Get Providers By Specialty",
     *     security={
     *       {"passport": {}},
     *     },
     *     @OA\Parameter(
     *         name="specialty",
     *         in="path",
     *         required=true,
     *         description="Specialty of the provider (ex. Cardiology).",
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean",
     *                 example="false"
     *             ),
     *             @OA\Property(
     *                 property="providers",
     *                 type="array",
     *                 @OA\Items(
     *                      type="object",
     *                      @OA\Property(property="id", type="uuid", example="7320BFEF-3599-EB11-AAE9-02E7A59D591E"),
     *                      @OA\Property(property="person_id", type="string", example="7020BFEF-3599-EB11-AAE9-02E7A59D591E"),
     *                      @OA\Property(property="lastname", type="string", example="ALEXANDER"),
     *                      @OA\Property(property="firstname", type="string", example="SANDRA"),
     *                      @OA\Property(property="practice_id", type="integer", example="1"),
     *                      @OA\Property(property="specialty_list_id", type="uuid", example="449F07BC-0DBA-EB11-AAE9-02E7A59D591E"),
     *                      @OA\Property(property="specialty_name", type="string", example="Endocrinology"),
     *                      @OA\Property(property="npi", type="string", example="1023181401"),
     *                      @OA\Property(property="bio", type="string", example="null"),
     *                      @OA\Property(property="address", type="string", example="405 ALBERTO WAY"),
     *                      @OA\Property(property="city", type="string", example="LOS GATOS"),
     *                      @OA\Property(property="state", type="string", example="CA"),
     *                      @OA\Property(property="zip", type="integer", example="95032"),
     *                      @OA\Property(property="country", type="string", example="null"),
     *                      @OA\Property(property="phone", type="phone", example="408-888-0009"),
     *                      @OA\Property(property="email", type="string", example="jane.dow@example.com"),
     *                      @OA\Property(property="photo_url", type="string", example="null"),
     *                      @OA\Property(property="rating", type="integer", example="2"),
     *                      @OA\Property(property="number_of_visit", type="integer", example="1"),
     *                 ),
     *             ),
     *         )
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": true, "message": "Error Message"}
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": true, "message": "Error Message"}
     *         )
     *     )
     * )
     */
    public function getProviderBySpecialty(Request $request, $specialty)
    {
        try
        {
            // set connection
            $connection = \DB::connection('sqlsrv')->getPdo();

            // extract
            extract($request->all());

            // optional
            $search = (isset($search)&&!empty($search) ? $search : '');

            // execute provider list mobile
            $statement = $connection->prepare("
                EXEC dbo.sp_provider_by_specialty_mobile
                    @specialty=:specialty
            ");
            $statement->bindParam(':specialty',$specialty);
            $statement->execute();
            $providers = $statement->fetchAll(\PDO::FETCH_ASSOC);

            // return response
            return \Response::json([
                'error'=>false,
                'providers'=>$providers
            ],200);
        }
        catch(\Exception $e)
        {
            return \Response::json([
                'error'=>true,
                'message'=>$e->getMessage()
            ],500);
        }
    }

    /**
     * @OA\Get(
     *     path="/api/v1/provider/name/{name}",
     *     tags={"Provider Management"},
     *     description="Description",
     *     summary="Get Providers By Name",
     *     security={
     *       {"passport": {}},
     *     },
     *     @OA\Parameter(
     *         name="name",
     *         in="path",
     *         required=true,
     *         description="Name of the provider (ex. alex).",
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean",
     *                 example="false"
     *             ),
     *             @OA\Property(
     *                 property="providers",
     *                 type="array",
     *                 @OA\Items(
     *                      type="object",
     *                      @OA\Property(property="id", type="uuid", example="7320BFEF-3599-EB11-AAE9-02E7A59D591E"),
     *                      @OA\Property(property="person_id", type="string", example="7020BFEF-3599-EB11-AAE9-02E7A59D591E"),
     *                      @OA\Property(property="lastname", type="string", example="ALEXANDER"),
     *                      @OA\Property(property="firstname", type="string", example="SANDRA"),
     *                      @OA\Property(property="practice_id", type="integer", example="1"),
     *                      @OA\Property(property="specialty_list_id", type="uuid", example="449F07BC-0DBA-EB11-AAE9-02E7A59D591E"),
     *                      @OA\Property(property="specialty_name", type="string", example="Endocrinology"),
     *                      @OA\Property(property="npi", type="string", example="1023181401"),
     *                      @OA\Property(property="bio", type="string", example="null"),
     *                      @OA\Property(property="address", type="string", example="405 ALBERTO WAY"),
     *                      @OA\Property(property="city", type="string", example="LOS GATOS"),
     *                      @OA\Property(property="state", type="string", example="CA"),
     *                      @OA\Property(property="zip", type="integer", example="95032"),
     *                      @OA\Property(property="country", type="string", example="null"),
     *                      @OA\Property(property="phone", type="phone", example="408-888-0009"),
     *                      @OA\Property(property="email", type="string", example="jane.dow@example.com"),
     *                      @OA\Property(property="photo_url", type="string", example="null"),
     *                      @OA\Property(property="rating", type="integer", example="2"),
     *                      @OA\Property(property="number_of_visit", type="integer", example="1"),
     *                 ),
     *             ),
     *         )
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": true, "message": "Error Message"}
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": true, "message": "Error Message"}
     *         )
     *     )
     * )
     */
    public function getProviderByName(Request $request, $name)
    {
        try
        {
            // set connection
            $connection = \DB::connection('sqlsrv')->getPdo();

            // extract
            extract($request->all());

            // optional
            $search = (isset($search)&&!empty($search) ? $search : '');

            // execute provider list mobile
            $statement = $connection->prepare("
                EXEC dbo.sp_provider_by_name_mobile
                    @name=:name
            ");
            $statement->bindParam(':name',$name);
            $statement->execute();
            $providers = $statement->fetchAll(\PDO::FETCH_ASSOC);

            // return response
            return \Response::json([
                'error'=>false,
                'providers'=>$providers
            ],200);
        }
        catch(\Exception $e)
        {
            return \Response::json([
                'error'=>true,
                'message'=>$e->getMessage()
            ],500);
        }
    }

    /**
     * @OA\Get(
     *     path="/api/v1/provider/online",
     *     tags={"Provider Management"},
     *     description="Description",
     *     summary="Get Providers Online",
     *     security={
     *       {"passport": {}},
     *     },
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean",
     *                 example="false"
     *             ),
     *             @OA\Property(
     *                 property="providers",
     *                 type="array",
     *                 @OA\Items(
     *                      type="object",
     *                      @OA\Property(property="id", type="uuid", example="0E9514B7-3DC5-EB11-AAE9-02E7A59D591E"),
     *                      @OA\Property(property="lastname", type="string", example="ABARCA"),
     *                      @OA\Property(property="firstname", type="string", example="DAVID"),
     *                      @OA\Property(property="specialty", type="string", example="Case Manager/Care Coordinator"),
     *                      @OA\Property(property="address", type="string", example="2160 W ADAMS BLVD"),
     *                      @OA\Property(property="city", type="string", example="LOS ANGELES"),
     *                      @OA\Property(property="state", type="string", example="LA"),
     *                      @OA\Property(property="zip", type="integer", example="90018"),
     *                      @OA\Property(property="contact_number", type="phone", example="323-432-5185")
     *                 ),
     *             ),
     *         )
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": true, "message": "Error Message"}
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": true, "message": "Error Message"}
     *         )
     *     )
     * )
     */
    public function getProviderOnline(Request $request)
    {
        try
        {
            // set connection
            $connection = \DB::connection('sqlsrv')->getPdo();

            // extract
            extract($request->all());

            // execute provider list mobile
            $statement = $connection->prepare("
                EXEC sp_providers_detail_mobile
                    @person_id=null
            ");
            $statement->execute();
            $providers = $statement->fetchAll(\PDO::FETCH_ASSOC);

            // return response
            return \Response::json([
                'error'=>false,
                'providers'=>$providers
            ],200);
        }
        catch(\Exception $e)
        {
            return \Response::json([
                'error'=>true,
                'message'=>$e->getMessage()
            ],500);
        }
    }

    /**
     * @OA\Get(
     *     path="/api/v1/provider/{id}",
     *     tags={"Provider Management"},
     *     description="Description",
     *     summary="Show Provider",
     *     security={
     *       {"passport": {}},
     *     },
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         required=true,
     *         description="ID of the provider.",
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean",
     *                 example="false"
     *             ),
     *             @OA\Property(
     *                 property="provider",
     *                      @OA\Property(property="provider_id", type="uuid", example="ADFA83AE-71A3-EB11-AAE9-02E7A59D591E"),
     *                      @OA\Property(property="provider_name", type="string", example="SHANNON ACCARDO"),
     *                      @OA\Property(property="lastname", type="string", example="ACCARDO"),
     *                      @OA\Property(property="firstname", type="string", example="SHANNON"),
     *                      @OA\Property(property="state", type="string", example="CA"),
     *                      @OA\Property(property="npi", type="string", example="1700235454"),
     *                      @OA\Property(property="home_phone", type="string", example="1234567890"),
     *                      @OA\Property(property="mobile_phone", type="string", example="1234567890"),
     *                      @OA\Property(property="work_phone", type="string", example="1234567890"),
     *                      @OA\Property(property="cnt", type="integer", example="2"),
     *             ),
     *         )
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": true, "message": "Error Message"}
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": true, "message": "Error Message"}
     *         )
     *     )
     * )
     */
    public function show($id)
    {
        try
        {
            // set connection
            $connection = \DB::connection('sqlsrv')->getPdo();

            // execute provider mobile
            $statement = $connection->prepare("
                EXEC dbo.sp_provider_mobile
                    @person_id=:person_id
            ");
            $statement->bindParam(':person_id',$id);
            $statement->execute();
            $provider = $statement->fetch(\PDO::FETCH_ASSOC);

            // return response
            return \Response::json([
                'error'=>false,
                'provider'=>$provider
            ],200);
        }
        catch(\Exception $e)
        {
            return \Response::json([
                'error'=>true,
                'message'=>$e->getMessage()
            ],500);
        }
    }
}
