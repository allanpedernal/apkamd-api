<?php

namespace App\Http\Controllers;

use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use PDO;

class RPMController extends Controller
{
    //
    protected $pdo_connection;

    function __construct() {
        $this->pdo_connection = app('sql_srv_connection')->getPdo();
    }

    /**
     * @OA\Get(
     *     path="/api/v1/rpm",
     *     tags={"RPM Management"},
     *     description="Description",
     *     summary="Get RPM Data",
     *     security={
     *       {"passport": {}},
     *     },
     *     @OA\Parameter(
     *         name="patient_id",
     *         in="query",
     *         required=true,
     *         @OA\Schema(
     *              type="string",
     *              format="uuid"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="type",
     *         in="query",
     *         required=true,
     *         @OA\Schema(
     *              type="string",
     *              example="heart_rate"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="value",
     *         in="query",
     *         required=true,
     *         @OA\Schema(
     *              type="string",
     *              example="heart_rate"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="aggregate",
     *         in="query",
     *         required=true,
     *         @OA\Schema(
     *              type="string",
     *              example="average"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="period",
     *         in="query",
     *         required=true,
     *         @OA\Schema(
     *              type="string",
     *              example="day"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="start_date",
     *         in="query",
     *         required=true,
     *         @OA\Schema(
     *              type="string",
     *              format="date"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="end_date",
     *         in="query",
     *         required=true,
     *         @OA\Schema(
     *              type="string",
     *              format="date"
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean",
     *                 example="false"
     *             ),
     *             @OA\Property(
     *                 property="data",
     *                 type="array",
     *                 @OA\Items(
     *                      type="object",
     *                      @OA\Property(property="id", type="string", format="uuid"),
     *                      @OA\Property(property="patient_id", type="string", format="uuid"),
     *                      @OA\Property(property="avg_value", type="string", example="330.82999999999998"),
     *                      @OA\Property(property="data_type", type="string", example="blood_glucose"),
     *                      @OA\Property(property="unit", type="string", example="mg/dL"),
     *                      @OA\Property(property="attribute", type="string", example="blood_glucose"),
     *                      @OA\Property(property="log_date", type="string", example="04/12/2021"),
     *                 )
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="data",
     *                 type="object",
     *                 @OA\Property(
     *                     property="patient_id",
     *                     type="string",
     *                     example="The patient id field is required."
     *                 ),
     *                 @OA\Property(
     *                     property="value",
     *                     type="string",
     *                     example="The value field is required."
     *                 ),
     *                 @OA\Property(
     *                     property="type",
     *                     type="string",
     *                     example="The type field is required."
     *                 ),
     *                 @OA\Property(
     *                     property="aggregate",
     *                     type="string",
     *                     example="The aggregate field is required."
     *                 ),
     *                 @OA\Property(
     *                     property="period",
     *                     type="string",
     *                     example="The period field is required."
     *                 ),
     *                 @OA\Property(
     *                     property="start_date",
     *                     type="string",
     *                     example="The start date field is required."
     *                 ),
     *                 @OA\Property(
     *                     property="end_date",
     *                     type="string",
     *                     example="The end date does not match the format Y-m-d."
     *                 )
     *             )
     *          )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": true, "message": "Error Message"}
     *         )
     *     )
     * )
     */
    function getRPMData(Request $request): JsonResponse
    {
        try {
            $patient_id = $request->get('patient_id');
            $type = $request->get('type');
            $value = $request->get('value');
            $aggregate = $request->get('aggregate');
            $period = $request->get('period');
            $start_date = $request->get('start_date');
            $end_date = $request->get('end_date');

            $validation_error = self::validateRequest(Validator::make($request->all(), [
                'patient_id' => 'required|uuid',
                'value' => 'required',
                'type' => 'required',
                'aggregate' => 'required',
                'period' => 'required',
                'start_date' => 'required|date|date_format:Y-m-d',
                'end_date' => 'required|date|date_format:Y-m-d'
            ]));

            if (count($validation_error) > 0)
                return response()->json([
                    'error' => true,
                    'data' => $validation_error
                ], 400);

            $sql = "EXEC sp_Get_PatientHealthRecords_ByReadingTypeDateFunction";
            $sql .= " @PatientID = :PatientID";
            $sql .= " ,@ReadingType = :ReadingType";
            $sql .= " ,@Attribute = :Attribute";
            $sql .= " ,@AggrFunction = :AggrFunction";
            $sql .= " ,@Period = :Period";
            $sql .= " ,@StartDate = :StartDate";
            $sql .= " ,@EndDate = :EndDate";

            $statement = $this->pdo_connection->prepare($sql);
            $statement->execute([
                ':PatientID' => $patient_id,
                ':ReadingType' => $type,
                ':Attribute' => $value,
                ':AggrFunction' => $aggregate,
                ':Period' => $period,
                ':StartDate' => $start_date,
                ':EndDate' => $end_date
            ]);
            $result = $statement->fetch(PDO::FETCH_ASSOC);
            return response()->json([
               'error' => false,
               'data' => $result ?: []
            ]);
        } catch (Exception $e) {
            return response()->json([
                'error' => true,
                'data' => [
                    'message' => $e->getMessage(),
                    'line' => $e->getLine(),
                    'file' => $e->getFile()
                ]
            ], 500);
        }
    }

    /**
     * @OA\Get(
     *     path="/api/v1/rpm/education/last-sync",
     *     tags={"RPM Management"},
     *     description="API to get education date and last sync date",
     *     summary="Get RPM Education and Last Sync",
     *     security={
     *       {"passport": {}},
     *     },
     *     @OA\Parameter(
     *         name="patient_id",
     *         in="query",
     *         required=true,
     *         description="Patient id of the RPM record (ex. 05893B9A-599C-EB11-AAE9-02E7A59D591E).",
     *     ),
     *     @OA\Parameter(
     *         name="source",
     *         in="query",
     *         required=true,
     *         description="Source of the RM record (ex. L12_smartwatch).",
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean",
     *                 example="false"
     *             ),
     *             @OA\Property(
     *                 property="rpm",
     *                 type="array",
     *                 @OA\Items(
     *                      type="object",
     *                      @OA\Property(property="patient_id", type="uuid", example="05893B9A-599C-EB11-AAE9-02E7A59D591E"),
     *                      @OA\Property(property="rpm_enrollment_date", type="datetime", example="2021-04-13 09:17:48.007"),
     *                      @OA\Property(property="last_sync_date", type="datetime", example="2021-05-30 12:10:57.667"),
     *                 ),
     *             ),
     *         )
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": true, "message": "Error Message"}
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": true, "message": "Error Message"}
     *         )
     *     )
     * )
     */
    public function getRPMEnrollmentAndLastSync(Request $request)
    {
        try
        {
            // get error
            $error = static::validateRequest(
                \Validator::make($request->all(), [
                    'patient_id' => 'required',
                    'source' => 'required'
                ])
            );

            // count error
            if (count($error) > 0)
            {
                // return response
                return \Response::json([
                    'error' => true,
                    'message' => $error
                ], 400);
            }

            // set connection
            $connection = \DB::connection('sqlsrv')->getPdo();

            // extract all
            extract($request->all());

            // execute patient list mobile
            $statement = $connection->prepare("
                EXEC sp_rpm_enrollment_dt_and_last_sync_dt
                    @patient_id=:patient_id,
                    @source=:source
            ");
            $statement->bindParam(':patient_id',$patient_id);
            $statement->bindParam(':source',$source);
            $statement->execute();
            $rpm = $statement->fetchAll(\PDO::FETCH_ASSOC);

            // return response
            return \Response::json([
                'error'=>false,
                'rpm'=>$rpm
            ],200);
        }
        catch(\Exception $e)
        {
            return \Response::json([
                'error'=>true,
                'message'=>$e->getMessage()
            ],500);
        }
    }


    /**
     * @OA\Get(
     *     path="/api/v1/rpm/latest-blood-sugar/{patient_id}",
     *     tags={"RPM Management"},
     *     description="API to get latest blood sugar level via patient id",
     *     summary="RPM Latest Blood Sugar",
     *     security={
     *       {"passport": {}},
     *     },
     *     @OA\Parameter(
     *         name="patient_id",
     *         in="path",
     *         required=true,
     *         description="ID of the patient.",
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean",
     *                 example="false"
     *             ),
     *             @OA\Property(
     *                 property="data",
     *                      @OA\Property(property="log_date", type="string", format="date-time"),
     *                      @OA\Property(property="blood_glucose", type="string", example="340"),
     *                      @OA\Property(property="range", type="string", example="above")
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": true, "message": "Error Message"}
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": true, "message": "Error Message"}
     *         )
     *     )
     * )
     */
    public function getLatetBloodSugar($patient_id): JsonResponse
    {
        try {

            $validation_error = self::validateRequest(Validator::make(['patient_id' => $patient_id], [
                'patient_id' => 'required|uuid'
            ]));

            if (count($validation_error) > 0)
                return response()->json([
                    'error' => true,
                    'data' => $validation_error
                ], 400);

            $sql = "EXEC glutality.sp_RPM_Current_Health_Overview @pPatientID = :pPatientID";
            $statement = $this->pdo_connection->prepare($sql);
            $statement->execute([':pPatientID' => $patient_id]);
            $data = $statement->fetch(PDO::FETCH_ASSOC);
            unset($data['before_meal']);
            return response()->json([
                'error' => false,
                'data' => $data ?: []
            ]);

        } catch (Exception $e) {
            return response()->json([
                'error' => true,
                'data' => [
                    'message' => $e->getMessage(),
                    'line' => $e->getLine(),
                    'file' => $e->getFile()
                ]
            ], 500);
        }
    }


    /**
     * @OA\Get(
     *     path="/api/v1/rpm/average-blood-glucose",
     *     tags={"RPM Management"},
     *     description="API to get average blood glucose",
     *     summary="RPM Blood Glucose",
     *     security={
     *       {"passport": {}},
     *     },
     *     @OA\Parameter(
     *         name="patient_id",
     *         in="query",
     *         required=true,
     *         @OA\Schema(
     *              type="string",
     *              example="05893B9A-599C-EB11-AAE9-02E7A59D591E"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="start_date",
     *         in="query",
     *         required=true,
     *         @OA\Schema(
     *              type="string",
     *              example="2021-06-19"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="end_date",
     *         in="query",
     *         required=true,
     *         @OA\Schema(
     *              type="string",
     *              example="2021-06-19"
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean",
     *                 example="false"
     *             ),
     *             @OA\Property(
     *                 property="data",
     *                 type="array",
     *                 @OA\Items(
     *                      type="object",
     *                      @OA\Property(property="id", type="string", format="uuid"),
     *                      @OA\Property(property="patient_id", type="string", format="uuid"),
     *                      @OA\Property(property="avg_value", type="string", example="330.82999999999998"),
     *                      @OA\Property(property="data_type", type="string", example="blood_glucose"),
     *                      @OA\Property(property="unit", type="string", example="mg/dL"),
     *                      @OA\Property(property="attribute", type="string", example="blood_glucose"),
     *                      @OA\Property(property="log_date", type="string", example="04/12/2021"),
     *                 ),
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": true, "message": "Error Message"}
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": true, "message": "Error Message"}
     *         )
     *     )
     * )
     */
    public function getBloodGlucose(Request $request) {
        try {

            $validation_error = self::validateRequest(Validator::make($request->all(), [
                'patient_id' => 'required|uuid',
                'start_date' => 'required|date:m/d/Y',
                'end_date' => 'required|date:m/dY'
            ]));

            $patient_id = $request->get('patient_id');
            $start_date = $request->get('start_date');
            $end_date = $request->get('end_date');

            if (count($validation_error) > 0)
                return response()->json([
                    'error' => true,
                    'data' => $validation_error
                ], 400);

            $sql = "EXEC sp_Get_PatientHealthRecords_ByReadingTypeDateFunction";
            $sql .= " @PatientID = :PatientID";
            $sql .= " ,@ReadingType = :ReadingType";
            $sql .= " ,@Attribute = :Attribute";
            $sql .= " ,@AggrFunction = :AggrFunction";
            $sql .= " ,@Period = :Period";
            $sql .= " ,@StartDate = :StartDate";
            $sql .= " ,@EndDate = :EndDate";
            $statement = $this->pdo_connection->prepare($sql);
            $statement->execute([
                ':PatientID' => $patient_id,
                ':ReadingType' => 'blood_glucose',
                ':Attribute' => 'blood_glucose',
                ':AggrFunction' => 'average',
                ':Period' => 'day',
                ':StartDate' => $start_date,
                ':EndDate' => $end_date
            ]);
            $data = $statement->fetchAll(PDO::FETCH_ASSOC);

            unset($data['before_meal']);
            return response()->json([
                'error' => false,
                'data' => $data ?: []
            ]);

        } catch (Exception $e) {
            return response()->json([
                'error' => true,
                'data' => [
                    'message' => $e->getMessage(),
                    'line' => $e->getLine(),
                    'file' => $e->getFile()
                ]
            ], 500);
        }
    }
}
