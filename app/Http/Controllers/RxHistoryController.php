<?php

namespace App\Http\Controllers;

use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use PDO;

class RxHistoryController extends Controller
{
    //
    protected $pdo_connection;

    function __construct() {
        $this->pdo_connection = app('sql_srv_connection')->getPdo();
    }

    /**
     * @OA\Get(
     *     path="/api/v1/rx-history/{id}",
     *     tags={"Rx-History Management"},
     *     description="Description",
     *     summary="Show Rx history by Patient ID",
     *     security={
     *       {"passport": {}},
     *     },
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         required=true,
     *         description="Patient ID"
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean",
     *                 example="false"
         *             ),
         *             @OA\Property(
     *                 property="data",
     *                 type="array",
     *                 @OA\Items(
     *                     @OA\Property(property="id", type="string", example="29ECF4DD-5CB2-EB11-AAE9-02E7A59D591E"),
     *                     @OA\Property(property="drug", type="string", example="ALBUTEROL SULF HFA 90 MCG INH"),
     *                     @OA\Property(property="direction", type="string", example="No directions available"),
     *                     @OA\Property(property="quantity", type="string", example="100"),
     *                     @OA\Property(property="days_supply", type="string", example="30"),
     *                     @OA\Property(property="next_fill_date", type="string", example="2021-04-11"),
     *                     @OA\Property(property="pharmacy_name", type="string", example="Testing PHARMACY # 2236"),
     *                     @OA\Property(property="provider_name", type="string", example="Dr. John Doe"),
     *                 )
     *             ),
     *         )
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": true, "message": "patient id is required."}
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": true, "message": "Error Message"}
     *         )
     *     )
     * )
     */
    function rxHistoryByPatientID($id): JsonResponse
    {
        try {
            $error = false;

            if (!$id)
                return response()->json([
                    'error' => !$error,
                    'message' => 'patient id is required'
                ], 400);

            $sql = "EXEC sp_medication_history_by_patient_mobile @patient_id = :patient_id";
            $statement = $this->pdo_connection->prepare($sql);
            $statement->execute([
               ':patient_id' => $id
            ]);
            $result = $statement->fetchAll(PDO::FETCH_ASSOC);
            return response()->json([
               'error' => $error,
               'data' => $result ?: []
            ]);
        } catch (Exception $e) {
            return response()->json([
                'error' => !$error,
                'data' => [
                    'message' => $e->getMessage(),
                    'line' => $e->getLine(),
                    'file' => $e->getFile()
                ]
            ], 500);
        }
    }


    /**
     * @OA\Get(
     *     path="/api/v1/rx-history/{id}/{fill_date}",
     *     tags={"Rx-History Management"},
     *     description="Description",
     *     summary="Show Rx history by Patient ID and Fill Date",
     *     security={
     *       {"passport": {}},
     *     },
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         required=true,
     *         description="Patient ID"
     *     ),
     *     @OA\Parameter(
     *         name="fill_date",
     *         in="path",
     *         required=true,
     *         description="Fill Date (YYYY-MM-DD)",
     *         @OA\Schema(
     *             type="string",
     *             format="date"
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean",
     *                 example="false"
     *             ),
     *             @OA\Property(
     *                 property="data",
     *                 type="array",
     *                 @OA\Items(
     *                     @OA\Property(property="id", type="string", example="29ECF4DD-5CB2-EB11-AAE9-02E7A59D591E"),
     *                     @OA\Property(property="drug", type="string", example="ALBUTEROL SULF HFA 90 MCG INH"),
     *                     @OA\Property(property="direction", type="string", example="No directions available"),
     *                     @OA\Property(property="quantity", type="string", example="100"),
     *                     @OA\Property(property="days_supply", type="string", example="30"),
     *                     @OA\Property(property="next_fill_date", type="string", example="2021-04-11"),
     *                     @OA\Property(property="pharmacy_name", type="string", example="Testing PHARMACY # 2236"),
     *                     @OA\Property(property="provider_name", type="string", example="Dr. John Doe"),
     *                 )
     *             ),
     *         )
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": true, "message": "patient id is required."}
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": true, "message": "Error Message"}
     *         )
     *     )
     * )
     */
    function rxHistoryByPatientIDAndDate($id, $fill_date): JsonResponse
    {
        try {
            $error = false;

            if (!$id OR !$fill_date)
                return response()->json([
                    'error' => !$error,
                    'message' => 'patient id and date are required'
                ], 400);

            $sql = "EXEC sp_medication_history_by_date_mobile @patient_id = :patient_id, @fill_date = :fill_date";
            $statement = $this->pdo_connection->prepare($sql);
            $statement->execute([
                ':patient_id' => $id,
                ':fill_date' => $fill_date
            ]);
            $result = $statement->fetchAll(PDO::FETCH_ASSOC);
            return response()->json([
                'error' => $error,
                'data' => $result ?: []
            ]);
        } catch (Exception $e) {
            return response()->json([
                'error' => !$error,
                'data' => [
                    'message' => $e->getMessage(),
                    'line' => $e->getLine(),
                    'file' => $e->getFile()
                ]
            ], 500);
        }
    }
}
