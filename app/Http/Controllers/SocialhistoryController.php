<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

/**
 * @group  Social History
 *
 * APIs for Social History
 */
class SocialhistoryController extends Controller
{
    /**
     * @OA\Get(
     *     path="/api/v1/social-history/patient/{patient_id}",
     *     tags={"Social History"},
     *     description="API to get social history by patient id",
     *     summary="Get Social History By Patient ID",
     *     security={
     *       {"passport": {}},
     *     },
     *     @OA\Parameter(
     *         name="patient_id",
     *         in="path",
     *         required=true,
     *         description="Patient ID of the Patient to retrieve Social History.",
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean",
     *                 example="false"
     *             ),
     *             @OA\Property(
     *                 property="socialhistory",
     *                 type="array",
     *                 @OA\Items(
     *                      type="object",
     *                      @OA\Property(property="id", type="string", example="B7D26C7F-9CB1-EB11-AAE9-02E7A59D591E"),
     *                      @OA\Property(property="traveled_overseas_for_past_2_months", type="string", example="468C3D28-7EB1-EB11-AAE9-02E7A59D591E"),
     *                      @OA\Property(property="drink_alcohol", type="string", example="No"),
     *                      @OA\Property(property="smoked_or_use_tobacco", type="string", example="Yes"),
     *                      @OA\Property(property="tobacco_frequency", type="string", example="1"),
     *                      @OA\Property(property="years_using_tobacco", type="string", example="1"),
     *                      @OA\Property(property="quit_smoking", type="string", example="Yes"),
     *                      @OA\Property(property="alcohol_frequency", type="string", example="1"),
     *                      @OA\Property(property="created_at", type="string", example="2021-05-25 07:33:04.290"),
     *                      @OA\Property(property="updated_at", type="string", example="2021-05-25 07:33:04.290"),
     *                 ),
     *             ),
     *         )
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": true, "message": "Error Message"}
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": true, "message": "Error Message"}
     *         )
     *     )
     * )
     */
    public function getSocialhistoryByPatientID(Request $request, $patient_id)
    {
        try
        {
            // set connection
            $connection = \DB::connection('sqlsrv')->getPdo();

            // execute appointment by date mobile
            $statement = $connection->prepare("
                EXEC dbo.sp_social_history
                    @patient_id=:patient_id
            ");
            $statement->bindParam(':patient_id',$patient_id);
            $statement->execute();
            $socialhistory = $statement->fetchAll(\PDO::FETCH_ASSOC);

            // return response
            return \Response::json([
                'error'=>false,
                'socialhistory'=>$socialhistory
            ],200);
        }
        catch(\Exception $e)
        {
            return \Response::json([
                'error'=>true,
                'message'=>$e->getMessage()
            ],500);
        }
    }

    /**
     * @OA\Post(
     *     path="/api/v1/social-history",
     *     tags={"Social History"},
     *     description="Description",
     *     summary="Store Social History",
     *     security={
     *       {"passport": {}},
     *     },
     *     @OA\RequestBody(
     *       required=true,
     *       description="Description",
     *       @OA\JsonContent(
     *           required={"patient_id"},
     *           @OA\Property(property="patient_id", type="uuid", example="944550AC-7EB1-EB11-AAE9-02E7A59D591E"),
     *           @OA\Property(property="traveled_overseas_for_past_2_months", type="string", example="Yes"),
     *           @OA\Property(property="drink_alcohol", type="string", example="No"),
     *           @OA\Property(property="smoked_or_use_tobacco", type="string", example="Yes"),
     *           @OA\Property(property="tobacco_frequency", type="string", example="1"),
     *           @OA\Property(property="years_using_tobacco", type="string", example="1"),
     *           @OA\Property(property="quit_smoking", type="string", example="Yes"),
     *           @OA\Property(property="alcohol_freqency", type="string", example="1"),
     *       ),
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": false, "message": "Social History successfully added!"}
     *         )
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": true, "message": "Error Message"}
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": true, "message": "Error Message"}
     *         )
     *     )
     * )
     */
    public function store(Request $request)
    {
        try
        {
            // get error
            $error = static::validateRequest(
                \Validator::make($request->all(), [
                    'patient_id' => 'required',
                ])
            );

            // count error
            if (count($error) > 0)
            {
                return \Response::json([
                    'error' => true,
                    'message' => $error
                ], 400);
            }

            // start transaction
            \DB::connection('sqlsrv')->beginTransaction();

            # set connection
            $connection = \DB::connection('sqlsrv')->getPdo();

            // extract all
            extract($request->all());

            // execute insurance mobile
            $statement = $connection->prepare("
                EXEC dbo.spm_social_history
                    @flag=0,
                    @patient_id=:patient_id,
                    @traveled_overseas_for_past_2_months =:traveled_overseas_for_past_2_months,
                    @drink_alcohol =:drink_alcohol,
                    @smoked_or_use_tobacco =:smoked_or_use_tobacco,
                    @tobacco_frequency =:tobacco_frequency,
                    @years_using_tobacco =:years_using_tobacco,
                    @quit_smoking =:quit_smoking,
                    @alcohol_freqency =:alcohol_freqency,
                    @id=NULL
            ");
            $statement->bindParam(':patient_id',$patient_id);
            $statement->bindParam(':traveled_overseas_for_past_2_months',$traveled_overseas_for_past_2_months);
            $statement->bindParam(':drink_alcohol',$drink_alcohol);
            $statement->bindParam(':smoked_or_use_tobacco',$smoked_or_use_tobacco);
            $statement->bindParam(':tobacco_frequency',$tobacco_frequency);
            $statement->bindParam(':years_using_tobacco',$years_using_tobacco);
            $statement->bindParam(':quit_smoking',$quit_smoking);
            $statement->bindParam(':alcohol_freqency',$alcohol_freqency);
            $statement->execute();

            // commit
            \DB::connection('sqlsrv')->commit();

            // return response
            return \Response::json([
                'error'=>false,
                'message'=>'Social History successfully added!'
            ],200);
        }
        catch(\Exception $e)
        {
            return \Response::json([
                'error'=>true,
                'message'=>$e->getMessage()
            ],500);
        }
    }

    /**
     * @OA\Put(
     *     path="/api/v1/social-history/{id}",
     *     tags={"Social History"},
     *     description="Description",
     *     summary="Update Social History",
     *     security={
     *       {"passport": {}},
     *     },
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         required=true,
     *         description="ID of the Social History.",
     *     ),
     *     @OA\RequestBody(
     *       required=true,
     *       description="Description",
     *       @OA\JsonContent(
     *           required={"patient_id"},
     *           @OA\Property(property="patient_id", type="uuid", example="944550AC-7EB1-EB11-AAE9-02E7A59D591E"),
     *           @OA\Property(property="traveled_overseas_for_past_2_months", type="string", example="No"),
     *           @OA\Property(property="drink_alcohol", type="string", example="Yes"),
     *           @OA\Property(property="smoked_or_use_tobacco", type="string", example="Yes"),
     *           @OA\Property(property="tobacco_frequency", type="string", example="1"),
     *           @OA\Property(property="years_using_tobacco", type="string", example="1"),
     *           @OA\Property(property="quit_smoking", type="string", example="Yes"),
     *           @OA\Property(property="alcohol_freqency", type="string", example="1"),
     *       ),
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": false, "message": "Social History successfully updated!"}
     *         )
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": true, "message": "Error Message"}
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": true, "message": "Error Message"}
     *         )
     *     )
     * )
     */
    public function update(Request $request, $id)
    {
        try
        {
            # set connection
            $connection = \DB::connection('sqlsrv')->getPdo();

            // extract all
            extract($request->all());

            $patient_id = (isset($patient_id)&&!empty($patient_id) ? $patient_id : '');
            $traveled_overseas_for_past_2_months = (isset($traveled_overseas_for_past_2_months)&&!empty($traveled_overseas_for_past_2_months) ? $traveled_overseas_for_past_2_months : '');
            $drink_alcohol = (isset($drink_alcohol)&&!empty($drink_alcohol) ? $drink_alcohol : '');
            $smoked_or_use_tobacco = (isset($smoked_or_use_tobacco)&&!empty($smoked_or_use_tobacco) ? $smoked_or_use_tobacco : '');
            $tobacco_frequency = (isset($tobacco_frequency)&&!empty($tobacco_frequency) ? $tobacco_frequency : '');
            $years_using_tobacco = (isset($years_using_tobacco)&&!empty($years_using_tobacco) ? $years_using_tobacco : '');
            $quit_smoking = (isset($quit_smoking)&&!empty($quit_smoking) ? $quit_smoking : '');
            $alcohol_freqency = (isset($alcohol_freqency)&&!empty($alcohol_freqency) ? $alcohol_freqency : '');

            // start transaction
            \DB::connection('sqlsrv')->beginTransaction();

            // execute insurance mobile
            $statement = $connection->prepare("
                EXEC dbo.spm_social_history
                    @flag=1,
                    @patient_id=:patient_id,
                    @traveled_overseas_for_past_2_months =:traveled_overseas_for_past_2_months,
                    @drink_alcohol =:drink_alcohol,
                    @smoked_or_use_tobacco =:smoked_or_use_tobacco,
                    @tobacco_frequency =:tobacco_frequency,
                    @years_using_tobacco =:years_using_tobacco,
                    @quit_smoking =:quit_smoking,
                    @alcohol_freqency  =:alcohol_freqency,
                    @id=:id
            ");
            $statement->bindParam(':patient_id',$patient_id);
            $statement->bindParam(':traveled_overseas_for_past_2_months',$traveled_overseas_for_past_2_months);
            $statement->bindParam(':drink_alcohol',$drink_alcohol);
            $statement->bindParam(':smoked_or_use_tobacco',$smoked_or_use_tobacco);
            $statement->bindParam(':tobacco_frequency',$tobacco_frequency);
            $statement->bindParam(':years_using_tobacco',$years_using_tobacco);
            $statement->bindParam(':quit_smoking',$quit_smoking);
            $statement->bindParam(':alcohol_freqency',$alcohol_freqency);
            $statement->bindParam(':id',$id);
            $statement->execute();

            // commit
            \DB::connection('sqlsrv')->commit();

            // return response
            return \Response::json([
                'error'=>false,
                'message'=>'Social History successfully updated!'
            ],200);
        }
        catch(\Exception $e)
        {
            return \Response::json([
                'error'=>true,
                'message'=>$e->getMessage()
            ],500);
        }
    }

    /**
     * @OA\Delete(
     *     path="/api/v1/social-history/{id}",
     *     tags={"Social History"},
     *     description="Description",
     *     summary="Delete Social History",
     *     security={
     *       {"passport": {}},
     *     },
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         required=true,
     *         description="ID of the Social History Record.",
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": false, "message": "Social History successfully deleted!"}
     *         )
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": true, "message": "Error Message"}
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": true, "message": "Error Message"}
     *         )
     *     )
     * )
     */
    public function destroy($id)
    {
        try
        {
            # set connection
            $connection = \DB::connection('sqlsrv')->getPdo();

            // start transaction
            \DB::connection('sqlsrv')->beginTransaction();

            // execute insurance mobile
            $statement = $connection->prepare("
                EXEC dbo.spm_social_history
                    @flag=2,
                    @patient_id=null,
                    @traveled_overseas_for_past_2_months ='',
                    @drink_alcohol ='',
                    @smoked_or_use_tobacco ='',
                    @tobacco_frequency ='',
                    @years_using_tobacco ='',
                    @quit_smoking ='',
                    @alcohol_freqency ='',
                    @id=:id
            ");
            $statement->bindParam(':id',$id);
            $statement->execute();

            // commit
            \DB::connection('sqlsrv')->commit();

            // return response
            return \Response::json([
                'error'=>false,
                'message'=>'Social History successfully deleted!'
            ],200);
        }
        catch(\Exception $e)
        {
            return \Response::json([
                'error'=>true,
                'message'=>$e->getMessage()
            ],500);
        }
    }
}
