<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

/**
 * @group  Statement
 *
 * APIs for managing insurances
 */
class StatementController extends Controller
{
    /**
     * @OA\Get(
     *     path="/api/v1/view-statement",
     *     tags={"Doctor"},
     *     description="Description",
     *     security={
     *       {"passport": {}},
     *     },
     *     @OA\Parameter(
     *         name="patient_id",
     *         in="query",
     *         required=true,
     *         description="Patient ID (ex. 468C1D9E-1ECD-EB11-AAE9-02E7A59D591E).",
     *     ),
     *     @OA\Parameter(
     *         name="charge_id",
     *         in="query",
     *         required=true,
     *         description="Charge ID (ex. 12CEA5DC-C5CD-EB11-AAE9-02E7A59D591E).",
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean",
     *                 example="false"
     *             ),
     *             @OA\Property(
     *                 property="statement",
     *                 type="array",
     *                 @OA\Items(
     *                      type="object",
     *                      @OA\Property(property="encounter_id", type="string", example="4C14504F-2CCD-EB11-AAE9-02E7A59D591E"),
     *                      @OA\Property(property="encounter_date", type="string", example="2021-06-14 12:19:36.000"),
     *                      @OA\Property(property="due_date", type="string", example="2021-07-15"),
     *                      @OA\Property(property="total_charge_amount", type="string", example="650.00"),
     *                      @OA\Property(property="total_insurance_payment", type="string", example="-500.00"),
     *                      @OA\Property(property="total_outo_pocket", type="string", example=".00"),
     *                      @OA\Property(property="remaining_balance", type="string", example="150.00"),
     *                      @OA\Property(property="payment_transaction_date", type="string", example="2021-06-15 12:48:48.697"),
     *                      @OA\Property(property="payment_description", type="string", example="Telemedicine 25 - 39 minutes"),
     *                      @OA\Property(property="payment_transaction_amount", type="string", example="-250.00"),
     *                      @OA\Property(property="transaction_type", type="string", example="insurance"),
     *                      @OA\Property(property="notes", type="string", example=""),
     *                      @OA\Property(property="payment_type", type="string", example=""),
     *                 ),
     *             ),
     *         )
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": true, "message": "Error Message"}
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": true, "message": "Error Message"}
     *         )
     *     )
     * )
     */
    public function viewStatement(Request $request)
    {
        try
        {
            // set connection
            $connection = \DB::connection('sqlsrv')->getPdo();

            // extract
            extract($request->all());

            // execute carrier list
            $statement = $connection->prepare("
                EXEC dbo.sp_patient_view_statement
                    @patient_id=:patient_id,
                    @charge_id=:charge_id
            ");
            $statement->bindParam(':patient_id',$patient_id);
            $statement->bindParam(':charge_id',$charge_id);
            $statement->execute();
            $statements = $statement->fetchAll(\PDO::FETCH_ASSOC);

            // return response
            return \Response::json([
                'error' => false,
                'statement' => $statements,
            ], 200);
        } catch (\Exception $e) {
            return \Response::json([
                'error' => true,
                'message' => $e->getMessage(),
            ], 500);
        }
    }
}
