<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use PDO;

/**
 * @group  User Management
 *
 * APIs for managing users
 */
class UserController extends Controller
{
    /**
     * @OA\Get(
     *     path="/api/v1/users",
     *     tags={"User Management"},
     *     description="Description",
     *     summary="Get Users",
     *     security={
     *       {"passport": {}},
     *     },
     *     @OA\Parameter(
     *         name="offset",
     *         in="query",
     *         required=true,
     *         description="Offset of the record (ex. 0).",
     *     ),
     *     @OA\Parameter(
     *         name="limit",
     *         in="query",
     *         required=true,
     *         description="Limit of the record (ex. 10).",
     *     ),
     *     @OA\Parameter(
     *         name="search",
     *         in="query",
     *         required=false,
     *         description="optional Keyword search (ex. Jane).",
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean",
     *                 example="false"
     *             ),
     *             @OA\Property(
     *                 property="users",
     *                 type="array",
     *                 @OA\Items(
     *                      type="object",
     *                      @OA\Property(property="users_id", type="string", example="E6851A86-7FB1-EB11-AAE9-02E7A59D591E"),
     *                      @OA\Property(property="person_id", type="string", example="944550AC-7EB1-EB11-AAE9-02E7A59D591E"),
     *                      @OA\Property(property="username", type="string", example="dannyg"),
     *                      @OA\Property(property="email", type="string", example="dg@mailinator.com"),
     *                      @OA\Property(property="patient_name", type="string", example="Garcia Danny"),
     *                      @OA\Property(property="gender", type="string", example="M"),
     *                      @OA\Property(property="dob", type="string", example="1960-06-01 00:00:00.000"),
     *                      @OA\Property(property="total_row_count", type="integer", example="1"),
     *                 ),
     *             ),
     *         )
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": true, "message": "Error Message"}
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": true, "message": "Error Message"}
     *         )
     *     )
     * )
     */
    public function getUsers(Request $request)
    {
        try
        {
            // get error
            $error = static::validateRequest(
                Validator::make($request->all(), [
                    'offset' => 'required',
                    'limit' => 'required',
                ])
            );

            // count error
            if (count($error) > 0)
            {
                // return response
                return response()->json([
                    'error' => true,
                    'message' => $error
                ], 400);
            }

            // set connection
            $connection = app('sql_srv_connection')->getPdo();

            // extract
            #extract($request->all());
            $offset = $request->get('offset');
            $limit = $request->get('limit');
            $search = $request->get('search') ?? '';

            // execute patient list mobile
            $statement = $connection->prepare("
                EXEC dbo.sp_user_list_mobile
                    @offset = :offset,
                    @limit = :limit,
                    @search = :search
            ");
            $statement->execute([
                ':offset' => $offset,
                ':limit' => $limit,
                ':search' => $search
            ]);
            $users = $statement->fetchAll(PDO::FETCH_ASSOC);

            // return response
            return response()->json([
                'error'=>false,
                'users'=>$users
            ]);
        }
        catch(\Exception $e)
        {
            return response()->json([
                'error'=>true,
                'message'=>$e->getMessage()
            ],500);
        }
    }

    /**
     * @OA\Get(
     *     path="/api/v1/user/{id}",
     *     tags={"User Management"},
     *     description="Description",
     *     summary="Show User",
     *     security={
     *       {"passport": {}},
     *     },
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         required=true,
     *         description="ID of the user.",
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean",
     *                 example="false"
     *             ),
     *             @OA\Property(
     *                 property="user",
     *                 @OA\Property(property="users_id", type="string", example="E6851A86-7FB1-EB11-AAE9-02E7A59D591E"),
     *                 @OA\Property(property="person_id", type="string", example="944550AC-7EB1-EB11-AAE9-02E7A59D591E"),
     *                 @OA\Property(property="username", type="string", example="dannyg"),
     *                 @OA\Property(property="email", type="string", example="dg@mailinator.com"),
     *                 @OA\Property(property="patient_name", type="string", example="Garcia Danny"),
     *                 @OA\Property(property="gender", type="string", example="M"),
     *                 @OA\Property(property="dob", type="string", example="1960-06-01 00:00:00.000"),
     *             ),
     *         )
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": true, "message": "Error Message"}
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": true, "message": "Error Message"}
     *         )
     *     )
     * )
     */
    public function show($id)
    {
        try
        {
            // set connection
            $connection = \DB::connection('sqlsrv')->getPdo();

            // execute patient by id
            $statement = $connection->prepare("
                EXEC dbo.sp_user_by_id_mobile
                    @user_id=:user_id
            ");
            $statement->bindParam(':user_id',$id);
            $statement->execute();
            $user = $statement->fetch(PDO::FETCH_ASSOC);

            if($user == false) {
                return \Response::json([
                    'error'=>true,
                    'message'=>'Error Message'
                ],400);
            }

            // return response
            return \Response::json([
                'error'=>false,
                'user'=>$user
            ],200);
        }
        catch(\Exception $e)
        {
            return \Response::json([
                'error'=>true,
                'message'=>$e->getMessage()
            ],500);
        }
    }
}
