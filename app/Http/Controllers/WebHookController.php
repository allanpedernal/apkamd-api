<?php

namespace App\Http\Controllers;

use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class WebHookController extends Controller
{
    //
    protected $request;

    protected $pdo_connection;

    function __construct(Request $request) {
        $this->request = $request;
        $this->pdo_connection = app('sql_srv_connection')->getPdo();
    }
    function telnyxWebHook(): JsonResponse
    {
        try {

            $update_fax_log = function($param) {
                $data = $param['payload'];
                $sql = "EXEC dbo.spm_prescription_fax_log_mobile";
                $sql .= " @flag = :flag";
                $sql .= " ,@patient_id = :patient_id";
                $sql .= " ,@fax_id = :fax_id";
                $sql .= " ,@status = :status";
                $statement = $this->pdo_connection->prepare($sql);
                $statement->execute([
                    ':flag' => 1,
                    ':patient_id' => NULL,
                    ':fax_id' => $data['fax_id'],
                    ':status' => $data['status']
                ]);
            };

            #log all request for this specific endpoint.
            Log::channel('TELNYX_WEBHOOK_LOGGER')->info('[TELNYX_WEBHOOK_EVENT]: ', [
                'log' => $this->request->all()
            ]);

            $data = $this->request->all();
            if (isset($data['data']['payload']['connection_id']) AND $data['data']['payload']['connection_id'] === env('TELNYX_CONNECTION_ID')) {
                #update record.
                $update_fax_log($data['data']);

                return response()->json([
                    'status' => 'success',
                ]);
            } else {
                abort(403);
            }
        } catch (Exception $e) {
            abort(500);
        }
    }
}
