<?php

namespace App\Http\Requests\Register;

use Illuminate\Foundation\Http\FormRequest;

use App\Rules\EmailUniqueRule;

class RegisterEmailVerificationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'email' => 'required|email'
        ];

        if ($this->input('flag') == 0) {
            $rules = array_merge($rules, [
                'email' => ['required', 'email', new EmailUniqueRule],
            ]);
        }

        return $rules;
    }

    public function attributes()
    {
        return [
            //
        ];
    }

    public function messages()
    {
        return [
            //
        ];
    }
}
