<?php

namespace App\Http\Requests\Register;

use Illuminate\Foundation\Http\FormRequest;

use App\Rules\MobileNumber;
use App\Rules\MobileUniqueRule;

class RegisterMobileVerificationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'mobile' => ['required', new MobileNumber],
        ];

        if ($this->input('flag') == 0) {
            $rules = array_merge($rules, [
                'mobile' => ['required', new MobileNumber, new MobileUniqueRule],
            ]);
        }

        return $rules;
    }

    public function attributes()
    {
        return [
            //
        ];
    }

    public function messages()
    {
        return [
            //
        ];
    }
}
