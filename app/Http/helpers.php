<?php

if (!function_exists('checkEmptyParam')) {
    function checkEmptyParam($params = []) {
        $error_param = [];
        foreach ($params as $param) {
            $error_param[] = $param;
        }

        return $error_param;
    }
}

function sendPushNotification($device_token, $title, $message, $id = null) {
    $server_key = env('FIREBASE_SERVER_KEY');
    $url = "https://fcm.googleapis.com/fcm/send";
    $header = [
    'authorization: key=' . $server_key,
        'content-type: application/json'
    ];

    $postdata = '{
        "to" : "' . $device_token . '",
            "notification" : {
                "title":"' . $title . '",
                "text" : "' . $message . '"
            },
        "data" : {
            "id" : "'.$id.'",
            "title":"' . $title . '",
            "description" : "' . $message . '",
            "text" : "' . $message . '",
            "is_read": 0
          }
    }';

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 60);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
    curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

    $result = curl_exec($ch);
    curl_close($ch);

    return $result;
}
