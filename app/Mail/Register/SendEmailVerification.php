<?php

namespace App\Mail\Register;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Notifications\Messages\MailMessage;

class SendEmailVerification extends Mailable
{
    use Queueable, SerializesModels;

    protected $code;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($code)
    {
        $this->code = $code;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject(config('app.name') . ':' . ' Verify Email')
            ->html((new MailMessage)
                    ->greeting('Hello,')
                    ->line('To verify your email address please enter the verification code on the registration form.')
                    ->line('Verification code: ' . $this->code)
                    ->line('Thank you for using our application!')
                    ->render()
                );
    }
}
