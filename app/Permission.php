<?php

namespace App;

use App\Traits\UuidForKey;
use Spatie\Permission\Models\Permission as BasePermission;

class Permission extends BasePermission
{
    use UuidForKey;

    protected $table = 'person.Permission';
    protected $primaryKey = 'id';

    protected $keyType = 'string';

    public $fillable = [
        'name',
        'guard_name',
        'practice_id',
    ];
}
