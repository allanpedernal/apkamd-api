<?php

namespace App;

use App\Traits\UuidForKey;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Person extends Model
{
    use UuidForKey;

    protected $table = 'person.Person';
    protected $primaryKey = 'ID';
    protected $keyType = 'string';
    public $incrementing = false;
    public $timestamps = false;

    public $fillable = [
        'Lastname',
        'Firstname',
        'Middlename',
        'Gender',
        'DOB',
        'SSN',
    ];

    const CREATED_AT = 'CreatedAt';
    const UPDATED_AT = 'UpdatedAt';
}
