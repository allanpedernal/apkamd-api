<?php

namespace App\Providers;

use App\Services\AdvancedMDService;
use App\Services\TelnyxService;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\DB;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
        $this->app->bind('sql_srv_connection', function () {
            return DB::connection('sqlsrv');
        });

        $this->app->singleton(
            'telnyx',
            TelnyxService::class
        );

        $this->app->bind('db_connection', function($app, $args) {
            $args = $args[0] ?? 'sqlsrv';
            return DB::connection($args);
        });

        $this->app->bind(
            'advanced-md',
            AdvancedMDService::class
        );
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
