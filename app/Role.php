<?php

namespace App;

use App\Traits\UuidForKey;
use Spatie\Permission\Models\Role as BaseRole;

class Role extends BaseRole
{
    use UuidForKey;

    protected $table = 'person.Role';
    protected $primaryKey = 'id';

    protected $keyType = 'string';

    public $fillable = [
        'name',
        'practice_id',
    ];
}
