<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

use PDO;
use DB;

class EmailUniqueRule implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $connection = \DB::connection('sqlsrv')->getPdo();
        $statement = $connection->prepare(
            "EXEC sp_verify_email_or_mobile
                @email = :email,
                @mobile = ''"
        );

        $email = $value;
        $statement->bindParam(':email', $email);
        $statement->execute();
        $result = $statement->fetch(PDO::FETCH_ASSOC);

        if (isset($result['response'])) {
            $result = $result['response'];
        } else {
            $result = $result[''];
        }

        return $result == 0;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The :attribute is already taken.';
    }
}
