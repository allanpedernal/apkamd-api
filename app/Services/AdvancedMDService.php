<?php


namespace App\Services;

use Carbon\Carbon;
use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\GuzzleException;
use PDO;

class AdvancedMDService
{

    protected static $app_name;

    protected static $username;

    protected static $password;

    protected static $office_code;

    protected $base_url;

    protected $auth_appended_uri;

    protected $client;

    protected $transaction;

    protected $data;

    protected $user_context;

    protected $request;

    protected $error_count = 0;


    function __construct() {
        $this->client = new Client;
        $this->base_url = 'https://partnerlogin.advancedmd.com/practicemanager/xmlrpc/processrequest.aspx';
        $this->auth_appended_uri = '/xmlrpc/processrequest.aspx';
    }

    function init($data = []): AdvancedMDService
    {
        if (count($data) === 0)
            trigger_error('Empty request payload!', E_USER_NOTICE);

        $this->authentication();
        $this->data = $data;

        return $this;
    }

    function process() {
        do {
            try {
                $response = $this->client->request('POST', $this->base_url, [
                    'headers' => [
                        'Content-Type' => 'application/json',
                        'Accept'     => 'application/json'
                    ],
                    'json' => $this->processRequest()
                ]);

                return json_decode($response->getBody(), true);
            } catch (ClientException $e) {
                $this->error_count++;
                if ($this->error_count >= 4) {
                    trigger_error($e->getResponse()->getBody()->getContents());
                }
            } catch (GuzzleException $e) {
                $this->error_count++;
                if ($this->error_count >= 4) {
                    trigger_error($e);
                }
            } catch (Exception $e) {
                $this->error_count++;
                if ($this->error_count >= 4) {
                    trigger_error($e->getMessage());
                }
            }
        } while ($this->error_count > 0 AND $this->error_count < 4);
    }

    protected function authentication()
    {
        self::resolveCredential();

        try {
            $user_context = $this->getUserContext();
            if (!$user_context) {
                $this->authenticate();
            } else {
                $tz = 'US/Eastern';
                $expiration = new Carbon( $user_context['expiration'] );
                $now = Carbon::now($tz);
                if ($now->gte($expiration)) {
                    $this->authenticate();
                } else {
                    $this->user_context = json_decode($user_context['usercontext'], true);
                }
            }


        } catch (ClientException $e) {
            $this->error_count++;
            if ($this->error_count >= 4) {
                trigger_error($e->getResponse()->getBody()->getContents());
            }
        } catch (GuzzleException $e) {
            $this->error_count++;
            if ($this->error_count >= 4) {
                trigger_error($e);
            }
        } catch (Exception $e) {
            $this->error_count++;
            if ($this->error_count >= 4) {
                trigger_error($e->getMessage());
            }
        }

    }

    /**
     * @throws GuzzleException
     */
    protected function authenticate() {
        $response = $this->client->request('POST', $this->base_url, [
            'headers' => [
                'Content-Type' => 'application/json',
                'Accept' => 'application/json'
            ],
            'json' => [
                'ppmdmsg' => [
                    '@action' => 'login',
                    '@class' => 'login',
                    '@msgtime' => Carbon::now()->format('m/d/Y h:i:s A'),
                    '@username' => self::$username,
                    '@psw' => self::$password,
                    '@officecode' => self::$office_code,
                    '@appname' => self::$app_name
                ]
            ]
        ]);

        $response = json_decode($response->getBody(), true);
        $result = @$response['PPMDResults']['Results']['@success'];
        $error = @$response['PPMDResults']['Error']['Fault']['detail']['code'];
        $web_server = @$response['PPMDResults']['Results']['usercontext']['@webserver'];

        if ($result === '0' AND $error === '-2147220476') {
            $url = $web_server . $this->auth_appended_uri;

            $response = $this->client->request('POST', $url, [
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Accept' => 'application/json'
                ],
                'json' => [
                    'ppmdmsg' => [
                        '@action' => 'login',
                        '@class' => 'login',
                        '@msgtime' => Carbon::now()->format('m/d/Y h:i:s A'),
                        '@username' => self::$username,
                        '@psw' => self::$password,
                        '@officecode' => self::$office_code,
                        '@appname' => self::$app_name
                    ]
                ]
            ]);
            $response = json_decode($response->getBody(), true);
            $result = $response['PPMDResults']['Results'];
            if (@$result['@success'] === '1') {
                $this->user_context = @$result['usercontext'];
                $this->saveUserContext($this->user_context);
            }
        }
    }

    protected static function resolveCredential() {
        self::$password = config('services.advanced-md.password');
        self::$office_code = config('services.advanced-md.office_code');
        self::$username = config('services.advanced-md.username');
        self::$app_name = config('services.advanced-md.app_name');
    }

    protected function processRequest(): array
    {
        $request = $this->data;
        $arr = function(& $arr, $key, $val){
            $arr = array_reverse($arr, true);
            $arr[$key] = $val;
            $arr = array_reverse($arr, true);
            return $arr;
        };
        $arr($request['ppmdmsg'], 'usercontext', $this->user_context);
        $request['ppmdmsg']['@nocookie'] = '1';
        return $request;
    }

    protected function getUserContext() {
        try {
            $pdo_connection = app('sql_srv_connection')->getPdo();
            $sql = "EXEC glutality.sp_Get_AMD_UserContext";
            $statement = $pdo_connection->prepare($sql);
            $statement->execute();
            return $statement->fetch(PDO::FETCH_ASSOC);
        } catch (Exception $e) {
            trigger_error($e->getMessage(), E_USER_NOTICE);
        }
    }

    protected function saveUserContext($context, $web_server = 'ApkaMD'): bool
    {
        try {
            $tz = 'US/Eastern';
            $expiration = Carbon::now($tz)->addHours(7);
            $pdo_connection = app('sql_srv_connection')->getPdo();
            $sql = "EXEC glutality.spm_AMD_UserContext @pWeb_Server = :pWeb_Server, @pUser_Context = :pUser_Context, @pExpiration = :pExpiration";
            $statement = $pdo_connection->prepare($sql);
            $statement->execute([
                ':pWeb_Server' => $web_server,
                ':pUser_Context' => json_encode( $context ),
                ':pExpiration' => $expiration
            ]);
            return true;
        } catch (Exception $e) {
            trigger_error($e->getMessage(), E_USER_NOTICE);
        }
    }
}
