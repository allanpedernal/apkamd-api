<?php


namespace App\Services\Facades;


use Illuminate\Support\Facades\Facade;

class AdvancedMD extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor(): string
    {
        return 'advanced-md';
    }
}
