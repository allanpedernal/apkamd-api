<?php


namespace App\Services\Facades;


use Illuminate\Support\Facades\Facade;

class Telnyx extends Facade
{
    /**
     * @return string
     */
    protected static function getFacadeAccessor(): string
    {
        return 'telnyx';
    }
}
