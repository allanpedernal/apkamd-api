<?php


namespace App\Services;


use Exception;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Log;

class TelnyxService
{
    function send($file_public_url)
    {
        try {
            $client = new Client();
            $headers = [
                'Authorization' => 'Bearer ' . env('TELNYX_TOKEN'),
                'Accept' => 'application/json',
            ];
            $form_params = [
                'media_url' => $file_public_url,
                'connection_id' => env('TELNYX_CONNECTION_ID'),
                'to' => env('OUTBOUND_FAX_NUMBER'),
                'from' => env('TELNYX_FAX_NUMBER'),
            ];

            $response = $client->request('POST', env('TELNYX_API_URL'), [
                'form_params' => $form_params,
                'headers' => $headers
            ]);

            Log::channel('TELNYX')->info('[Telnyx Service] Fax processed. [PDF URL] = ' . $file_public_url);

            return json_decode($response->getBody()->getContents(), true);

         } catch (Exception $e) {
            Log::channel('TELNYX')->error('[Telnyx Service] exception error.', [
                'exception' => $e->getMessage(),
            ]);
        }
    }
}
