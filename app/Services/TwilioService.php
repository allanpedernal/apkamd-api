<?php

namespace App\Services;

use Twilio\Rest\Client as Twilio;
use Twilio\Exceptions\TwilioException;

class TwilioService
{
	public static function sendSms($recipient, $message)
	{
		$client = new Twilio(env('TWILIO_SID'), env('TWILIO_AUTH_TOKEN'));
		try {
			$client->messages->create("+1$recipient", [
				'from' => env('TWILIO_NUMBER'),
				'body' => $message,
			]);
			return true;
		} catch (TwilioException $e) {
			abort(400, $e->getMessage());
			return false;
		}
	}
}