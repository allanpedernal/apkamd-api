<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Traits\UuidForKey;
use Spatie\Permission\Traits\HasRoles;
use Laravel\Passport\HasApiTokens;
use Auth;

class User extends Authenticatable
{
    use Notifiable, UuidForKey, SoftDeletes, HasRoles, HasApiTokens;

    protected $table = 'person.users';

    protected $primaryKey = 'id';

    protected $keyType = 'string';

    public $fillable = [
        'first_name',
        'last_name',
        'middle_name',
        'profile_picture',
        'username',
        'email',
        'created_by',
        'email_verified_at',
        'password_crack',
        'password',
        'personid',
        'token'
    ];

    protected $dates = ['deleted_at'];

    protected $hidden = [
        'password',
        'remember_token',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function person(){
        return $this->belongsTo(Person::class,'personid', 'ID');
    }

    public function getAllPermissionsAttribute() {
        $permissions = [];

        foreach (Permission::all() as $permission) {
          if (Auth::user()->can($permission->name)) {
            $permissions[] = $permission->name;
          }
        }

        return $permissions;
    }

    public function getAllRolesAttribute() {
      $roles = [];
        foreach (Role::all() as $role) {
            if (!in_array($role->name, $roles)) {
                if (Auth::user()->hasRole($role->name)) {
                    $roles[] = $role->name;
                 }
            }
        }
        return $roles;
    }
}
