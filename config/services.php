<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Mailgun, Postmark, AWS and more. This file provides the de facto
    | location for this type of information, allowing packages to have
    | a conventional file to locate the various service credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
    ],

    'postmark' => [
        'token' => env('POSTMARK_TOKEN'),
    ],

    'ses' => [
        'key' => env('AWS_ACCESS_KEY_ID'),
        'secret' => env('AWS_SECRET_ACCESS_KEY'),
        'region' => env('AWS_DEFAULT_REGION', 'us-east-1'),
    ],

    'default-pharmacy' => [
        'name' => env('PHARMACY_NAME'),
        'address' => env('PHARMACY_ADDRESS'),
        'fax' => env('PHARMACY_FAX'),
        'phone' => env('PHARMACY_PHONE')
    ],

    'advanced-md' => [
        'password' => env('ADVANCED_MD_API_PASSWORD'),
        'office_code' => env('ADVANCED_MD_API_OFFICE_CODE'),
        'username' => env('ADVANCED_MD_API_USERNAME'),
        'app_name' => env('ADVANCED_MD_API_APP_NAME')
    ]

];
