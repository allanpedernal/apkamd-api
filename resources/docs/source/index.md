---
title: API Reference

language_tabs:
- bash
- javascript
- php
- python

includes:

search: true

toc_footers:
- <a href='http://github.com/mpociot/documentarian'>Documentation Powered by Documentarian</a>
---
<!-- START_INFO -->
# Info

Welcome to the generated API reference.
[Get Postman Collection](http://localhost/docs/collection.json)

<!-- END_INFO -->

#Carrier Management


APIs for managing insurances
<!-- START_ff02557e23520586ff7d6d60adc9193f -->
## Get Carrier

> Example request:

```bash
curl -X GET \
    -G "http://localhost/api/v1/carriers" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}"
```

```javascript
const url = new URL(
    "http://localhost/api/v1/carriers"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'http://localhost/api/v1/carriers',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
            'Authorization' => 'Bearer {token}',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```

```python
import requests
import json

url = 'http://localhost/api/v1/carriers'
headers = {
  'Content-Type': 'application/json',
  'Accept': 'application/json',
  'Authorization': 'Bearer {token}'
}
response = requests.request('GET', url, headers=headers)
response.json()
```


> Example response (200):

```json
{
    "error": false
}
```
> Example response (400):

```json
{
    "error": true
}
```
> Example response (500):

```json
{
    "error": true
}
```

### HTTP Request
`GET api/v1/carriers`


<!-- END_ff02557e23520586ff7d6d60adc9193f -->

#Insurance Management


APIs for managing insurances
<!-- START_1ba5014637ddc84bbd7584776f9ef335 -->
## Get Insurances

> Example request:

```bash
curl -X GET \
    -G "http://localhost/api/v1/insurances" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}"
```

```javascript
const url = new URL(
    "http://localhost/api/v1/insurances"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'http://localhost/api/v1/insurances',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
            'Authorization' => 'Bearer {token}',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```

```python
import requests
import json

url = 'http://localhost/api/v1/insurances'
headers = {
  'Content-Type': 'application/json',
  'Accept': 'application/json',
  'Authorization': 'Bearer {token}'
}
response = requests.request('GET', url, headers=headers)
response.json()
```


> Example response (200):

```json
{
    "error": false
}
```
> Example response (400):

```json
{
    "error": true
}
```
> Example response (500):

```json
{
    "error": true
}
```

### HTTP Request
`GET api/v1/insurances`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `offset` |  required  | Offset of the record (ex. 0).
    `limit` |  required  | Limit of the record (ex. 10).
    `search` |  optional  | optional Keyword search (ex. Jane).

<!-- END_1ba5014637ddc84bbd7584776f9ef335 -->

<!-- START_25b59ce0e655114b025d1878ea5708c0 -->
## Store Insurance

> Example request:

```bash
curl -X POST \
    "http://localhost/api/v1/insurance" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}" \
    -d '{"patient_id":"totam","carrier_id":"qui","carrier_name":"omnis","member_id":"et"}'

```

```javascript
const url = new URL(
    "http://localhost/api/v1/insurance"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

let body = {
    "patient_id": "totam",
    "carrier_id": "qui",
    "carrier_name": "omnis",
    "member_id": "et"
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->post(
    'http://localhost/api/v1/insurance',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
            'Authorization' => 'Bearer {token}',
        ],
        'json' => [
            'patient_id' => 'totam',
            'carrier_id' => 'qui',
            'carrier_name' => 'omnis',
            'member_id' => 'et',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```

```python
import requests
import json

url = 'http://localhost/api/v1/insurance'
payload = {
    "patient_id": "totam",
    "carrier_id": "qui",
    "carrier_name": "omnis",
    "member_id": "et"
}
headers = {
  'Content-Type': 'application/json',
  'Accept': 'application/json',
  'Authorization': 'Bearer {token}'
}
response = requests.request('POST', url, headers=headers, json=payload)
response.json()
```


> Example response (200):

```json
{
    "error": false
}
```
> Example response (400):

```json
{
    "error": true
}
```
> Example response (500):

```json
{
    "error": true
}
```

### HTTP Request
`POST api/v1/insurance`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `patient_id` | uuid |  required  | ID of Person/Patient (ex. 944550AC-7EB1-EB11-AAE9-02E7A59D591E)
        `carrier_id` | uuid |  required  | Carrier ID (ex. 724379A4-9AB1-EB11-AAE9-02E7A59D591E)
        `carrier_name` | string |  required  | Carrier name (ex. ACS BENEFIT SERVICES, INC)
        `member_id` | string |  required  | Member ID of the user (ex. 78985789542600)
    
<!-- END_25b59ce0e655114b025d1878ea5708c0 -->

<!-- START_6e443b64a6016da61ba1e11302de78bb -->
## Show Insurance

> Example request:

```bash
curl -X GET \
    -G "http://localhost/api/v1/insurance/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}"
```

```javascript
const url = new URL(
    "http://localhost/api/v1/insurance/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'http://localhost/api/v1/insurance/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
            'Authorization' => 'Bearer {token}',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```

```python
import requests
import json

url = 'http://localhost/api/v1/insurance/1'
headers = {
  'Content-Type': 'application/json',
  'Accept': 'application/json',
  'Authorization': 'Bearer {token}'
}
response = requests.request('GET', url, headers=headers)
response.json()
```


> Example response (200):

```json
{
    "error": false
}
```
> Example response (400):

```json
{
    "error": true
}
```
> Example response (500):

```json
{
    "error": true
}
```

### HTTP Request
`GET api/v1/insurance/{insurance}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `id` |  required  | ID of the insurance.

<!-- END_6e443b64a6016da61ba1e11302de78bb -->

<!-- START_aa46288c4da7cdd21e9284e96f558e07 -->
## Update Insurance

> Example request:

```bash
curl -X PUT \
    "http://localhost/api/v1/insurance/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}" \
    -d '{"patient_id":"earum","carrier_id":"deserunt","carrier_name":"esse","member_id":"consectetur"}'

```

```javascript
const url = new URL(
    "http://localhost/api/v1/insurance/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

let body = {
    "patient_id": "earum",
    "carrier_id": "deserunt",
    "carrier_name": "esse",
    "member_id": "consectetur"
}

fetch(url, {
    method: "PUT",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->put(
    'http://localhost/api/v1/insurance/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
            'Authorization' => 'Bearer {token}',
        ],
        'json' => [
            'patient_id' => 'earum',
            'carrier_id' => 'deserunt',
            'carrier_name' => 'esse',
            'member_id' => 'consectetur',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```

```python
import requests
import json

url = 'http://localhost/api/v1/insurance/1'
payload = {
    "patient_id": "earum",
    "carrier_id": "deserunt",
    "carrier_name": "esse",
    "member_id": "consectetur"
}
headers = {
  'Content-Type': 'application/json',
  'Accept': 'application/json',
  'Authorization': 'Bearer {token}'
}
response = requests.request('PUT', url, headers=headers, json=payload)
response.json()
```


> Example response (200):

```json
{
    "error": false
}
```
> Example response (400):

```json
{
    "error": true
}
```
> Example response (500):

```json
{
    "error": true
}
```

### HTTP Request
`PUT api/v1/insurance/{insurance}`

`PATCH api/v1/insurance/{insurance}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `id` |  required  | ID of the insurance.
#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `patient_id` | uuid |  required  | ID of Person/Patient (ex. 944550AC-7EB1-EB11-AAE9-02E7A59D591E)
        `carrier_id` | uuid |  required  | Carrier ID (ex. 724379A4-9AB1-EB11-AAE9-02E7A59D591E)
        `carrier_name` | string |  required  | Carrier name (ex. ACS BENEFIT SERVICES, INC)
        `member_id` | string |  required  | Member ID of the user (ex. 78985789542600)
    
<!-- END_aa46288c4da7cdd21e9284e96f558e07 -->

<!-- START_06fee95be6935d42962bb722046ebc12 -->
## Delete Insurance

> Example request:

```bash
curl -X DELETE \
    "http://localhost/api/v1/insurance/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}"
```

```javascript
const url = new URL(
    "http://localhost/api/v1/insurance/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->delete(
    'http://localhost/api/v1/insurance/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
            'Authorization' => 'Bearer {token}',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```

```python
import requests
import json

url = 'http://localhost/api/v1/insurance/1'
headers = {
  'Content-Type': 'application/json',
  'Accept': 'application/json',
  'Authorization': 'Bearer {token}'
}
response = requests.request('DELETE', url, headers=headers)
response.json()
```


> Example response (200):

```json
{
    "error": false
}
```
> Example response (400):

```json
{
    "error": true
}
```
> Example response (500):

```json
{
    "error": true
}
```

### HTTP Request
`DELETE api/v1/insurance/{insurance}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `id` |  required  | ID of the insurance.

<!-- END_06fee95be6935d42962bb722046ebc12 -->

#Patient Management


APIs for managing poatients
<!-- START_09f19989f6dda1c5f57ed84e811c6acc -->
## Get Patients

> Example request:

```bash
curl -X GET \
    -G "http://localhost/api/v1/patients" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}"
```

```javascript
const url = new URL(
    "http://localhost/api/v1/patients"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'http://localhost/api/v1/patients',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
            'Authorization' => 'Bearer {token}',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```

```python
import requests
import json

url = 'http://localhost/api/v1/patients'
headers = {
  'Content-Type': 'application/json',
  'Accept': 'application/json',
  'Authorization': 'Bearer {token}'
}
response = requests.request('GET', url, headers=headers)
response.json()
```


> Example response (200):

```json
{
    "error": false
}
```
> Example response (400):

```json
{
    "error": true
}
```
> Example response (500):

```json
{
    "error": true
}
```

### HTTP Request
`GET api/v1/patients`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `offset` |  required  | Offset of the record (ex. 0).
    `limit` |  required  | Limit of the record (ex. 10).
    `search` |  optional  | optional Keyword search (ex. Jane).

<!-- END_09f19989f6dda1c5f57ed84e811c6acc -->

<!-- START_04fd00b429c4f355c142e32041470433 -->
## Store Patient

> Example request:

```bash
curl -X POST \
    "http://localhost/api/v1/patient" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}" \
    -d '{"lastname":"itaque","firstname":"sint","gender":"non","dob":"similique","street_address":"ex","city":"provident","country":"rem","state":"ducimus","zip_code":"veritatis","mobile_number":"deserunt","email":"quidem","user_id":"et","person_id":"qui"}'

```

```javascript
const url = new URL(
    "http://localhost/api/v1/patient"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

let body = {
    "lastname": "itaque",
    "firstname": "sint",
    "gender": "non",
    "dob": "similique",
    "street_address": "ex",
    "city": "provident",
    "country": "rem",
    "state": "ducimus",
    "zip_code": "veritatis",
    "mobile_number": "deserunt",
    "email": "quidem",
    "user_id": "et",
    "person_id": "qui"
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->post(
    'http://localhost/api/v1/patient',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
            'Authorization' => 'Bearer {token}',
        ],
        'json' => [
            'lastname' => 'itaque',
            'firstname' => 'sint',
            'gender' => 'non',
            'dob' => 'similique',
            'street_address' => 'ex',
            'city' => 'provident',
            'country' => 'rem',
            'state' => 'ducimus',
            'zip_code' => 'veritatis',
            'mobile_number' => 'deserunt',
            'email' => 'quidem',
            'user_id' => 'et',
            'person_id' => 'qui',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```

```python
import requests
import json

url = 'http://localhost/api/v1/patient'
payload = {
    "lastname": "itaque",
    "firstname": "sint",
    "gender": "non",
    "dob": "similique",
    "street_address": "ex",
    "city": "provident",
    "country": "rem",
    "state": "ducimus",
    "zip_code": "veritatis",
    "mobile_number": "deserunt",
    "email": "quidem",
    "user_id": "et",
    "person_id": "qui"
}
headers = {
  'Content-Type': 'application/json',
  'Accept': 'application/json',
  'Authorization': 'Bearer {token}'
}
response = requests.request('POST', url, headers=headers, json=payload)
response.json()
```


> Example response (200):

```json
{
    "error": false
}
```
> Example response (400):

```json
{
    "error": true
}
```
> Example response (500):

```json
{
    "error": true
}
```

### HTTP Request
`POST api/v1/patient`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `lastname` | string |  required  | Lastname of the patient (ex. Doe)
        `firstname` | string |  required  | Firstname of the patient (ex. John)
        `gender` | string |  required  | Gender of the patient (ex. M)
        `dob` | string |  required  | Birthdate of the patient (ex. 12/25/2020)
        `street_address` | string |  required  | Street address of the patient (ex. #25 Hallmark St.)
        `city` | string |  required  | City of the patient (ex. Brooklyn)
        `country` | string |  required  | Country of the patient (ex. US)
        `state` | string |  required  | State of the patient (ex. CA)
        `zip_code` | string |  required  | Zipcode of the patient (ex. 2374)
        `mobile_number` | string |  required  | Mobile of the patient (ex. 0357778995)
        `email` | email |  required  | Email address of the patient (ex. john.doe@gmail.com)
        `user_id` | uuid |  required  | User id of the patient  (ex. NULL)
        `person_id` | uuid |  required  | ID of the patient (ex. NULL)
    
<!-- END_04fd00b429c4f355c142e32041470433 -->

<!-- START_39724c8bcb79d6a41c9c1670458a308c -->
## Show Patient

> Example request:

```bash
curl -X GET \
    -G "http://localhost/api/v1/patient/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}"
```

```javascript
const url = new URL(
    "http://localhost/api/v1/patient/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'http://localhost/api/v1/patient/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
            'Authorization' => 'Bearer {token}',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```

```python
import requests
import json

url = 'http://localhost/api/v1/patient/1'
headers = {
  'Content-Type': 'application/json',
  'Accept': 'application/json',
  'Authorization': 'Bearer {token}'
}
response = requests.request('GET', url, headers=headers)
response.json()
```


> Example response (200):

```json
{
    "error": false
}
```
> Example response (400):

```json
{
    "error": true
}
```
> Example response (500):

```json
{
    "error": true
}
```

### HTTP Request
`GET api/v1/patient/{patient}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `id` |  required  | ID of the patient.

<!-- END_39724c8bcb79d6a41c9c1670458a308c -->

<!-- START_96527aa8c811944b938f4f811f83a4b4 -->
## Update Patient

> Example request:

```bash
curl -X PUT \
    "http://localhost/api/v1/patient/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}" \
    -d '{"lastname":"voluptatem","firstname":"non","gender":"aut","dob":"dolorum","street_address":"a","city":"ullam","country":"qui","state":"sunt","zip_code":"dignissimos","mobile_number":"perferendis","email":"beatae","user_id":"neque","person_id":"eos"}'

```

```javascript
const url = new URL(
    "http://localhost/api/v1/patient/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

let body = {
    "lastname": "voluptatem",
    "firstname": "non",
    "gender": "aut",
    "dob": "dolorum",
    "street_address": "a",
    "city": "ullam",
    "country": "qui",
    "state": "sunt",
    "zip_code": "dignissimos",
    "mobile_number": "perferendis",
    "email": "beatae",
    "user_id": "neque",
    "person_id": "eos"
}

fetch(url, {
    method: "PUT",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->put(
    'http://localhost/api/v1/patient/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
            'Authorization' => 'Bearer {token}',
        ],
        'json' => [
            'lastname' => 'voluptatem',
            'firstname' => 'non',
            'gender' => 'aut',
            'dob' => 'dolorum',
            'street_address' => 'a',
            'city' => 'ullam',
            'country' => 'qui',
            'state' => 'sunt',
            'zip_code' => 'dignissimos',
            'mobile_number' => 'perferendis',
            'email' => 'beatae',
            'user_id' => 'neque',
            'person_id' => 'eos',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```

```python
import requests
import json

url = 'http://localhost/api/v1/patient/1'
payload = {
    "lastname": "voluptatem",
    "firstname": "non",
    "gender": "aut",
    "dob": "dolorum",
    "street_address": "a",
    "city": "ullam",
    "country": "qui",
    "state": "sunt",
    "zip_code": "dignissimos",
    "mobile_number": "perferendis",
    "email": "beatae",
    "user_id": "neque",
    "person_id": "eos"
}
headers = {
  'Content-Type': 'application/json',
  'Accept': 'application/json',
  'Authorization': 'Bearer {token}'
}
response = requests.request('PUT', url, headers=headers, json=payload)
response.json()
```


> Example response (200):

```json
{
    "error": false
}
```
> Example response (400):

```json
{
    "error": true
}
```
> Example response (500):

```json
{
    "error": true
}
```

### HTTP Request
`PUT api/v1/patient/{patient}`

`PATCH api/v1/patient/{patient}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `id` |  required  | ID of the patient.
#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `lastname` | string |  optional  | optional Lastname of the patient. (ex. Doe)
        `firstname` | string |  optional  | optional Firstname of the patient. (ex. John)
        `gender` | string |  optional  | optional Gender of the patient. (ex. M)
        `dob` | string |  optional  | optional Birthdate of the patient. (ex. 12/25/2020)
        `street_address` | string |  optional  | optional Street address of the patient. (ex. #25 Hallmark St.)
        `city` | string |  optional  | optional City of the patient. (ex. Brooklyn)
        `country` | string |  optional  | optional Country of the patient. (ex. US)
        `state` | string |  optional  | optional State of the patient. (ex. CA)
        `zip_code` | string |  optional  | optional Zipcode of the patient. (ex. 2374)
        `mobile_number` | string |  optional  | optional Mobile of the patient. (ex. 0357778995)
        `email` | email |  optional  | optional Email address of the patient. (ex. john.doe@gmail.com)
        `user_id` | uuid |  optional  | optional User id of the patient.  (ex. NULL)
        `person_id` | uuid |  optional  | optional ID of the patient. (ex. NULL)
    
<!-- END_96527aa8c811944b938f4f811f83a4b4 -->

<!-- START_660ef4e6f2ee2f2ea04ada218dd3eb97 -->
## Delete Patient

> Example request:

```bash
curl -X DELETE \
    "http://localhost/api/v1/patient/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}"
```

```javascript
const url = new URL(
    "http://localhost/api/v1/patient/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->delete(
    'http://localhost/api/v1/patient/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
            'Authorization' => 'Bearer {token}',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```

```python
import requests
import json

url = 'http://localhost/api/v1/patient/1'
headers = {
  'Content-Type': 'application/json',
  'Accept': 'application/json',
  'Authorization': 'Bearer {token}'
}
response = requests.request('DELETE', url, headers=headers)
response.json()
```


> Example response (200):

```json
{
    "error": false
}
```
> Example response (400):

```json
{
    "error": true
}
```
> Example response (500):

```json
{
    "error": true
}
```

### HTTP Request
`DELETE api/v1/patient/{patient}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `id` |  required  | ID of the patient.

<!-- END_660ef4e6f2ee2f2ea04ada218dd3eb97 -->

#User Management


APIs for managing users
<!-- START_1aff981da377ba9a1bbc56ff8efaec0d -->
## Get Users

> Example request:

```bash
curl -X GET \
    -G "http://localhost/api/v1/users" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}"
```

```javascript
const url = new URL(
    "http://localhost/api/v1/users"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'http://localhost/api/v1/users',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
            'Authorization' => 'Bearer {token}',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```

```python
import requests
import json

url = 'http://localhost/api/v1/users'
headers = {
  'Content-Type': 'application/json',
  'Accept': 'application/json',
  'Authorization': 'Bearer {token}'
}
response = requests.request('GET', url, headers=headers)
response.json()
```


> Example response (200):

```json
{
    "error": false
}
```
> Example response (400):

```json
{
    "error": true
}
```
> Example response (500):

```json
{
    "error": true
}
```

### HTTP Request
`GET api/v1/users`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `offset` |  required  | Offset of the record (ex. 0).
    `limit` |  required  | Limit of the record (ex. 10).
    `search` |  optional  | optional Keyword search (ex. Jane).

<!-- END_1aff981da377ba9a1bbc56ff8efaec0d -->

<!-- START_96b8840d06e94c53a87e83e9edfb44eb -->
## Store User

> Example request:

```bash
curl -X POST \
    "http://localhost/api/v1/user" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}" \
    -d '{"person_id":"reiciendis","username":"nulla","password":"quae","email":"nam","pin":"non"}'

```

```javascript
const url = new URL(
    "http://localhost/api/v1/user"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

let body = {
    "person_id": "reiciendis",
    "username": "nulla",
    "password": "quae",
    "email": "nam",
    "pin": "non"
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->post(
    'http://localhost/api/v1/user',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
            'Authorization' => 'Bearer {token}',
        ],
        'json' => [
            'person_id' => 'reiciendis',
            'username' => 'nulla',
            'password' => 'quae',
            'email' => 'nam',
            'pin' => 'non',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```

```python
import requests
import json

url = 'http://localhost/api/v1/user'
payload = {
    "person_id": "reiciendis",
    "username": "nulla",
    "password": "quae",
    "email": "nam",
    "pin": "non"
}
headers = {
  'Content-Type': 'application/json',
  'Accept': 'application/json',
  'Authorization': 'Bearer {token}'
}
response = requests.request('POST', url, headers=headers, json=payload)
response.json()
```


> Example response (200):

```json
{
    "error": false
}
```
> Example response (400):

```json
{
    "error": true
}
```
> Example response (500):

```json
{
    "error": true
}
```

### HTTP Request
`POST api/v1/user`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `person_id` | uuid |  required  | ID of Person/Patient (ex. 944550AC-7EB1-EB11-AAE9-02E7A59D591E)
        `username` | string |  required  | Username of the user (ex. jonh.doe)
        `password` | string |  required  | Password of the user (ex. password)
        `email` | email |  required  | Email address of the user (ex. john.doe@example.com)
        `pin` | string |  required  | Pin of the user (ex. 12345)
    
<!-- END_96b8840d06e94c53a87e83e9edfb44eb -->

<!-- START_a8f148df1f2cd4bc2d67314d2cb9fa3d -->
## Show User

> Example request:

```bash
curl -X GET \
    -G "http://localhost/api/v1/user/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}"
```

```javascript
const url = new URL(
    "http://localhost/api/v1/user/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'http://localhost/api/v1/user/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
            'Authorization' => 'Bearer {token}',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```

```python
import requests
import json

url = 'http://localhost/api/v1/user/1'
headers = {
  'Content-Type': 'application/json',
  'Accept': 'application/json',
  'Authorization': 'Bearer {token}'
}
response = requests.request('GET', url, headers=headers)
response.json()
```


> Example response (200):

```json
{
    "error": false
}
```
> Example response (400):

```json
{
    "error": true
}
```
> Example response (500):

```json
{
    "error": true
}
```

### HTTP Request
`GET api/v1/user/{user}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `id` |  required  | ID of the user.

<!-- END_a8f148df1f2cd4bc2d67314d2cb9fa3d -->

<!-- START_1006d782d67bb58039bde349972eb2f0 -->
## Update User

> Example request:

```bash
curl -X PUT \
    "http://localhost/api/v1/user/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}" \
    -d '{"person_id":"perferendis","username":"voluptas","password":"impedit","email":"perspiciatis","pin":"dolorem"}'

```

```javascript
const url = new URL(
    "http://localhost/api/v1/user/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

let body = {
    "person_id": "perferendis",
    "username": "voluptas",
    "password": "impedit",
    "email": "perspiciatis",
    "pin": "dolorem"
}

fetch(url, {
    method: "PUT",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->put(
    'http://localhost/api/v1/user/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
            'Authorization' => 'Bearer {token}',
        ],
        'json' => [
            'person_id' => 'perferendis',
            'username' => 'voluptas',
            'password' => 'impedit',
            'email' => 'perspiciatis',
            'pin' => 'dolorem',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```

```python
import requests
import json

url = 'http://localhost/api/v1/user/1'
payload = {
    "person_id": "perferendis",
    "username": "voluptas",
    "password": "impedit",
    "email": "perspiciatis",
    "pin": "dolorem"
}
headers = {
  'Content-Type': 'application/json',
  'Accept': 'application/json',
  'Authorization': 'Bearer {token}'
}
response = requests.request('PUT', url, headers=headers, json=payload)
response.json()
```


> Example response (200):

```json
{
    "error": false
}
```
> Example response (400):

```json
{
    "error": true
}
```
> Example response (500):

```json
{
    "error": true
}
```

### HTTP Request
`PUT api/v1/user/{user}`

`PATCH api/v1/user/{user}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `id` |  required  | ID of the user.
#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `person_id` | uuid |  optional  | optional ID of Person/Patient (ex. 944550AC-7EB1-EB11-AAE9-02E7A59D591E)
        `username` | string |  optional  | optional Username of the user (ex. jonh.doe)
        `password` | string |  optional  | optional Password of the user (ex. password)
        `email` | email |  optional  | optional Email address of the user (ex. john.doe@example.com)
        `pin` | string |  optional  | optional Pin of the user (ex. 12345)
    
<!-- END_1006d782d67bb58039bde349972eb2f0 -->

<!-- START_a5d7655acadc1b6c97d48e68f1e87be9 -->
## Delete User

> Example request:

```bash
curl -X DELETE \
    "http://localhost/api/v1/user/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}"
```

```javascript
const url = new URL(
    "http://localhost/api/v1/user/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->delete(
    'http://localhost/api/v1/user/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
            'Authorization' => 'Bearer {token}',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```

```python
import requests
import json

url = 'http://localhost/api/v1/user/1'
headers = {
  'Content-Type': 'application/json',
  'Accept': 'application/json',
  'Authorization': 'Bearer {token}'
}
response = requests.request('DELETE', url, headers=headers)
response.json()
```


> Example response (200):

```json
{
    "error": false
}
```
> Example response (400):

```json
{
    "error": true
}
```
> Example response (500):

```json
{
    "error": true
}
```

### HTTP Request
`DELETE api/v1/user/{user}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `id` |  required  | ID of the user.

<!-- END_a5d7655acadc1b6c97d48e68f1e87be9 -->

#general


<!-- START_f7b7ea397f8939c8bb93e6cab64603ce -->
## Display Swagger API page.

> Example request:

```bash
curl -X GET \
    -G "http://localhost/api/documentation" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}"
```

```javascript
const url = new URL(
    "http://localhost/api/documentation"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'http://localhost/api/documentation',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
            'Authorization' => 'Bearer {token}',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```

```python
import requests
import json

url = 'http://localhost/api/documentation'
headers = {
  'Content-Type': 'application/json',
  'Accept': 'application/json',
  'Authorization': 'Bearer {token}'
}
response = requests.request('GET', url, headers=headers)
response.json()
```


> Example response (200):

```json
null
```

### HTTP Request
`GET api/documentation`


<!-- END_f7b7ea397f8939c8bb93e6cab64603ce -->

<!-- START_a2c4ea37605c6d2e3c93b7269030af0a -->
## Display Oauth2 callback pages.

> Example request:

```bash
curl -X GET \
    -G "http://localhost/api/oauth2-callback" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}"
```

```javascript
const url = new URL(
    "http://localhost/api/oauth2-callback"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'http://localhost/api/oauth2-callback',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
            'Authorization' => 'Bearer {token}',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```

```python
import requests
import json

url = 'http://localhost/api/oauth2-callback'
headers = {
  'Content-Type': 'application/json',
  'Accept': 'application/json',
  'Authorization': 'Bearer {token}'
}
response = requests.request('GET', url, headers=headers)
response.json()
```


> Example response (200):

```json
null
```

### HTTP Request
`GET api/oauth2-callback`


<!-- END_a2c4ea37605c6d2e3c93b7269030af0a -->


