<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Refill Form</title>
</head>
<body>
    <div>
        <h4>Request Refill Form</h4>
        <table style="width:100%">
            <tr>
                <td style="text-align:left"><h5>To</h5></td>
                <td style="text-align:left"><h5>From</h5></td>
            </tr>
            <tr>
                <td style="text-align:left; font-size: 12px;">
                    <p style="margin: 0">{{$pharmacy_data['name']}} {{$pharmacy_data['fax']}}</p>
                    <p style="margin: 0">{{$pharmacy_data['address']}}</p>
                    <p style="margin: 0">Phone: {{$pharmacy_data['phone']}}</p>
                    <p style="margin: 0">Fax: {{$pharmacy_data['fax']}}</p>
                </td>

                <td style="text-align:left; font-size: 12px;">
                    <p style="margin: 0;">{{$provider['name']}}</p>
                    <p style="margin: 0;">Address: {{$provider['address']}}</p>
                    <p style="margin: 0;">Contact Number: {{$provider['phone']}}</p>
                    <p style="margin: 0;">Email: {{$provider['email']}}</p>
                </td>
            </tr>
            <tr>
                <td style="text-align:left"><h5 style="margin-bottom: 5px">Patient Information</h5></td>
            </tr>
            <tr>
                <td style="text-align:left; font-size: 12px;">
                    <p style="margin: 0">Name: {{$patient['name']}}</p>
                    <p style="margin: 0">Date of Birth: {{$patient['dob']}}</p>
                    <p style="margin: 0">Sex: {{$patient['gender']}}</p>
                    <p style="margin: 0">Address: {{$patient['full_address']}}</p>
                    <p style="margin: 0">Phone: {{$patient['contact']}}</p>
                    <p style="margin: 0">Email: {{$patient['email']}}</p>
                </td>
            </tr>
        </table>
        <br>
        <h3>Medication Order</h3>
        <table class="order-table">
            <thead>
                <tr>
                    <th style="text-align:left;">Medication</th>
                    <th style="text-align:left;">Quantity</th>
                    <th style="text-align:left;">Unit</th>
                    <th style="text-align:left;">Instruction</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>{{$medication['name']}}</td>
                    <td>{{$medication['unit_quantity']}}</td>
                    <td>{{$medication['unit_dosage']}}</td>
                    <td>{{$medication['instruction']}}</td>
                </tr>
            </tbody>
        </table>
        <br>
        <p>
            <strong>Comments:</strong> {{ $other['comment'] }}
        </p>
        <p>
            <strong>Ordered By: </strong>{{$provider['name']}},
            <i>NPI:</i> {{$provider['npi']}}
        </p>
    </div>
</body>
</html>

