<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// version 1
Route::group(['prefix' => 'v1'], function () {

	// authenticate
    Route::post('auth/token', 'AuthenticateController@authToken');

	// middleware client
	Route::group(['middleware'=>['client']],function(){

        Route::post('auth/login', 'AuthenticateController@authLogin');
        Route::post('auth/register', 'AuthenticateController@authRegister');

		// verify email
		Route::get('email-verification','AuthenticateController@sendEmailVerification')->name('auth.send.email.verification');
		Route::post('email-verification','AuthenticateController@verifyEmail')->name('auth.verify.email');

        // verify sms
        Route::get('sms-verification','AuthenticateController@sendSmsVerification')->name('auth.send.sms.verification');
        Route::post('sms-verification','AuthenticateController@verifySms')->name('auth.verify.sms');

		// users
		Route::get('users','UserController@getUsers');
		Route::resource('user','UserController')->only(['show']);

		// patients
		Route::get('patients','PatientController@getPatients');
		Route::get('patient/statement/{patient_id}','PatientController@getPatientStatement');
		Route::resource('patient','PatientController')->only(['show','update']);

		// providers
		Route::get('providers','ProviderController@getProviders');
		Route::get('provider/city/{city}','ProviderController@getProviderByCity');
		Route::get('provider/specialty/{specialty}','ProviderController@getProviderBySpecialty');
		Route::get('provider/name/{name}','ProviderController@getProviderByName');
		Route::get('provider/online','ProviderController@getProviderOnline');
		Route::resource('provider','ProviderController')->only(['show']);

		// insurances
		Route::get('insurances','InsuranceController@getInsurances');
		Route::get('insurance/patient/{patient_id}','InsuranceController@getInsurancesByPatientID');
		Route::resource('insurance','InsuranceController')->only(['store','show','update','destroy']);

        // specialties
        Route::get('specialties','SpecialtyController@getSpecialties');
        Route::resource('specialty','SpecialtyController')->only(['store','show','update','destroy']);

		// carriers
		Route::get('carriers','CarrierController@getCarriers');

		// Medical History
		Route::get('medication-adherence/{id}','MedicationController@getMedicationAdherence');
		Route::get('allergy/patient/{patient_id}','AllergyController@getAllergiesByPatientID');
		Route::resource('allergy','AllergyController')->only(['store','update','destroy']);
		Route::get('heightweight/patient/{patient_id}','HeightweightController@getHeightweightByPatientID');
		Route::resource('heightweight','HeightweightController')->only(['store','update','destroy']);
		Route::get('medication/patient/{patient_id}','MedicationController@getMedicationsByPatientID');
		Route::resource('medication','MedicationController')->only(['store','update','destroy']);
		Route::get('social-history/patient/{patient_id}','SocialhistoryController@getSocialhistoryByPatientID');
		Route::resource('social-history','SocialhistoryController')->only(['store','update','destroy']);
		Route::get('care-physician/patient/{patient_id}','CarephysicianController@getCarephysicianByPatientID');
		Route::resource('care-physician','CarephysicianController')->only(['store','update','destroy']);
		Route::get('flex-form/questions','FlexformController@getFlexFormQuestions');
		Route::get('flex-form/answers/{patient_id}','FlexformController@getFlexFormAnswersByPatientID');
		Route::resource('flex-form','FlexformController')->only(['store','update','destroy']);

		// rx-history
        Route::get('rx-history/{id}', 'RxHistoryController@rxHistoryByPatientID');
        Route::get('rx-history/{id}/{fill_date}', 'RxHistoryController@rxHistoryByPatientIDAndDate');

        // prescription refill
        Route::post('refill', 'PrescriptionController@refill');

		// appointments
		Route::get('appointments','AppointmentController@getAppointments');
		Route::get('appointment/date/{datetime}','AppointmentController@getAppointmentsByDate');
		Route::get('appointment/patient/{patient_id}','AppointmentController@getAppointmentsByPatientID');
		Route::get('appointment/provider/{provider_id}','AppointmentController@getAppointmentsByProviderID');
		Route::resource('appointment','AppointmentController')->only(['store','show','update','destroy']);
		Route::post('appointment/dates/get-provider-schedule-dates', 'AppointmentController@getProviderUnavailableDates');

		// rpm overview
		Route::get('rpm/education/last-sync', 'RPMController@getRPMEnrollmentAndLastSync');
		Route::get('rpm', 'RPMController@getRPMData');
		Route::get('rpm/latest-blood-sugar/{patient_id}', 'RPMController@getLatetBloodSugar');
        Route::get('rpm/average-blood-glucose', 'RPMController@getBloodGlucose');

		// medication
        Route::resource('patient-medication','PatientMedication')->only(['store', 'update', 'destroy']);
        Route::get('patient-medication/{patient_id}', 'PatientMedication@medicationByPatientID');


        // make payment
        Route::get('payment/get-stripe-customer-id/{patient_id}', 'PaymentController@getStripeCustomerID');
        Route::post('payment/save-stripe-customer-id', 'PaymentController@saveStripeCustomer');
        Route::post('payment/save-card-details', 'PaymentController@saveCardDetails');
        Route::post('payment/store-payment-invoice-details', 'PaymentController@storePaymentInvoiceDetails');

       	//doctor
        Route::get('get-doctor-availability', 'DoctorController@getDoctorAvailability');
        Route::get('view-statement', 'StatementController@viewStatement');

        // device
        Route::group(['prefix'=>'device'],function(){
        	Route::get('registration/key/{userid}', 'DeviceController@getRegistrationKey');
        	Route::post('registration/key', 'DeviceController@storeRegistrationKey');
        });

        //eligibility
        Route::post('check-eligibility', 'EligibilityController@checkEligibility');

        //payer list
        Route::get('payer-list', 'PayerController@list');
	});

    #rnel test.
    #Route::get('rx-history/{id}', 'RxHistoryController@rxHistoryByPatientID');
    #Route::get('rx-history/{id}/{fill_date}', 'RxHistoryController@rxHistoryByPatientIDAndDate');
    #Route::post('refill', 'PrescriptionController@refill');
    #Route::resource('medication','PatientMedication')->only(['store', 'update', 'destroy']);
    #Route::get('medication/{patient_id}', 'PatientMedication@medicationByPatientID');
    #Route::resource('appointment','AppointmentController')->only(['store','show','update','destroy']);
    #Route::get('appointments','AppointmentController@getAppointments');
    #Route::resource('user','UserController')->only(['show']);
    #Route::get('users','UserController@getUsers');
    #Route::get('patients','PatientController@getPatients');
    #Route::get('rpm/latest-blood-sugar/{patient_id}', 'RPMController@getLatetBloodSugar');
    #Route::get('rpm/average-blood-glucose', 'RPMController@getBloodGlucose');
    #Route::post('rpm', 'RPMController@getRPMData');
    #Route::post('check-eligibility', 'EligibilityController@checkEligibility');
    #Route::get('payer-list', 'PayerController@list');
    #Route::post('auth/register', 'AuthenticateController@authRegister');
    #Route::post('auth/login', 'AuthenticateController@authLogin');
    #Route::resource('insurance','InsuranceController')->only(['store','show','update','destroy']);
    #Route::get('insurance/patient/{patient_id}','InsuranceController@getInsurancesByPatientID');
});
