<?php

use Illuminate\Support\Facades\Route;

Route::group(['prefix' => '/fax'], function() {
    Route::post('/send', 'WebHookController@telnyxWebHook');
});
